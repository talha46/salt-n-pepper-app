package com.saltnpepper.app;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.os.Handler;
import android.telephony.TelephonyManager;
import android.util.DisplayMetrics;
import android.util.Log;

import com.facebook.AppEventsLogger;
import com.saltnpepper.app.utils.Util;

public class SplashActivity extends Activity {

	private Handler mHandler;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		if (!isTaskRoot()) { // Last time run from installer screen's Open
								// button rather than after Done

			Log.e("SplashActivity", "App already running, brought in front..");
			finish();
			return;
		}
		setContentView(R.layout.activity_splash_layout);
		try{
			showDeviceDisplayInfo();
		}catch (Exception e) {

		}
		mHandler = new Handler();
		mHandler.postDelayed(new Runnable() {
			@Override
			public void run() {
				if (Util.isLoggedIn(SplashActivity.this)) {
					startActivity(new Intent(SplashActivity.this, HomeScreen.class));
				} else {
					startActivity(new Intent().setClass(SplashActivity.this, Home.class));
				}
				finish();
			}
		}, 2000);
		saveInitialValues();
		
	}
	private void showDeviceDisplayInfo(){
		
		int screenSize = getResources().getConfiguration().screenLayout & Configuration.SCREENLAYOUT_SIZE_MASK;

		String toastMsg;
		switch(screenSize) {
		    case Configuration.SCREENLAYOUT_SIZE_LARGE:
		        toastMsg = "Large screen";
		        break;
		    case Configuration.SCREENLAYOUT_SIZE_NORMAL:
		        toastMsg = "Normal screen";
		        break;
		    case Configuration.SCREENLAYOUT_SIZE_SMALL:
		        toastMsg = "Small screen";
		        break;
		    case Configuration.SCREENLAYOUT_SIZE_XLARGE:
		        toastMsg = "XLarge screen";
		        break;
		    default:
		        toastMsg = "Screen size is neither large, normal or small";
		}
		Log.d("DEVICE_INFO", toastMsg);
		
		String dpi = "";
		switch (getResources().getDisplayMetrics().densityDpi) {
		case DisplayMetrics.DENSITY_LOW:
		    // handle your code here for ldpi
			dpi = "drawable-ldpi";
		    break;
		case DisplayMetrics.DENSITY_MEDIUM:
		    // handle your code here for mdpi
			dpi = "drawable-mdpi";
		    break;
		case DisplayMetrics.DENSITY_HIGH:
		   // handle your code here for hdpi
			dpi = "drawable-hdpi";
		    break;
		case DisplayMetrics.DENSITY_XHIGH:
		    // handle your code here for xhdpi
			dpi = "drawable-xhdpi";
			break;
		case DisplayMetrics.DENSITY_XXHIGH:
			dpi = "drawable-xxhdpi";
			break;	
		case DisplayMetrics.DENSITY_XXXHIGH:
			dpi = "drawable-xxxhdpi";
			break;
		case DisplayMetrics.DENSITY_TV:
			dpi = "drawable-tvdpi";
		    break;
		}
		Log.d("DEVICE_INFO", dpi);
		//Toast.makeText(getActivity(), toastMsg + "\n " + dpi, Toast.LENGTH_LONG).show();	
	}
	
	public void saveInitialValues() {
		// Saving IMEI number
		TelephonyManager telephonyManager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
		String device = telephonyManager.getDeviceId();
		String carrierName = telephonyManager.getNetworkOperatorName();
		if (device != null) {
			Util.saveStringWithKey(com.saltnpepper.app.utils.Constants.IMEI_NUMBER, device, SplashActivity.this);
		}
		if (carrierName != null) {
			Util.saveStringWithKey(com.saltnpepper.app.utils.Constants.CARRIER_NAME, carrierName, SplashActivity.this);
		}
	}

	@Override
	protected void onResume() {
		super.onResume();

		// Logs 'install' and 'app activate' App Events.
		AppEventsLogger.activateApp(this);
	}

	@Override
	protected void onPause() {
		super.onPause();

		// Logs 'app deactivate' App Event.
		AppEventsLogger.deactivateApp(this);
	}
}
