package com.saltnpepper.app.utils;

import java.util.HashMap;

import org.w3c.dom.CharacterData;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.drawable.ColorDrawable;
import android.location.Location;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import android.widget.TextView;

import com.saltnpepper.app.R;
import com.saltnpepper.app.datamodel.User;
import com.google.gson.Gson;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.assist.SimpleImageLoadingListener;

public class Util {

	public static String FB_ID = "";

	public static boolean isNetworkAvailable(Context context) {

		ConnectivityManager connectivityManager = (ConnectivityManager) context
				.getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
		return activeNetworkInfo != null;
	}

	// public static Bitmap getRoundedShape(Bitmap scaleBitmapImage) {
	//
	// int targetWidth = 50;
	// int targetHeight = 50;
	// Bitmap targetBitmap = Bitmap.createBitmap(targetWidth,
	// targetHeight,Bitmap.Config.ARGB_8888);
	//
	// Canvas canvas = new Canvas(targetBitmap);
	// Path path = new Path();
	// path.addCircle(((float) targetWidth - 1) / 2,
	// ((float) targetHeight - 1) / 2,
	// (Math.min(((float) targetWidth),
	// ((float) targetHeight)) / 2),
	// Path.Direction.CCW);
	//
	// canvas.clipPath(path);
	// Bitmap sourceBitmap = scaleBitmapImage;
	// canvas.drawBitmap(sourceBitmap,
	// new Rect(0, 0, sourceBitmap.getWidth(),
	// sourceBitmap.getHeight()),
	// new Rect(0, 0, targetWidth,
	// targetHeight), null);
	// return targetBitmap;
	// }
	// shows a dialog and exits the activity.
	public static void showAlert(String message, final Activity context, final boolean dontFinishActivity) {

		if (context != null) {

			final Dialog dialog = new Dialog(context);
			dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
			WindowManager.LayoutParams wmlp = dialog.getWindow().getAttributes();

			wmlp.gravity = Gravity.CENTER;

			dialog.setContentView(R.layout.dialog_ok_alert_layout);
			dialog.getWindow().setBackgroundDrawable(new ColorDrawable(0));

			// dialog.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
			dialog.setCanceledOnTouchOutside(false);

			TextView title = (TextView) dialog.findViewById(R.id.titleText);
			title.setText(R.string.app_name);

			TextView messageText = (TextView) dialog.findViewById(R.id.messageText);
			messageText.setText(message);

			Button ok_button = (Button) dialog.findViewById(R.id.ok_button);

			ok_button.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					if(!dontFinishActivity){
						context.finish();
					}else{
						dialog.dismiss();
					}
				}

			});

			Button cancel_button = (Button) dialog.findViewById(R.id.dialog_cancel_button);
			cancel_button.setVisibility(View.GONE);
			dialog.show();
		}
	}

	// this doesn't end the Activity
	public static void showMessage(String message, final Activity context) {
		String title = context.getResources().getString(R.string.app_name);
		showMessage(title, message, context);
	}

	public static void showMessage(String title, String message, final Activity context) {

		if (context != null) {

			final Dialog dialog = new Dialog(context);
			dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
			WindowManager.LayoutParams wmlp = dialog.getWindow().getAttributes();

			wmlp.gravity = Gravity.CENTER;

			dialog.setContentView(R.layout.dialog_ok_alert_layout);
			dialog.getWindow().setBackgroundDrawable(new ColorDrawable(0));

			// dialog.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
			dialog.setCanceledOnTouchOutside(false);

			TextView titleTx = (TextView) dialog.findViewById(R.id.titleText);
			titleTx.setText(title);

			TextView messageText = (TextView) dialog.findViewById(R.id.messageText);
			messageText.setText(message);

			Button ok_button = (Button) dialog.findViewById(R.id.ok_button);

			ok_button.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					dialog.dismiss();
				}

			});

			Button cancel_button = (Button) dialog.findViewById(R.id.dialog_cancel_button);
			cancel_button.setVisibility(View.GONE);
			dialog.show();
		}
	}

	public static void urlLoadImage(ImageLoader imageLoader, final ImageView profileImage, String get_ImagePath) {
		try {
			if (get_ImagePath != null && get_ImagePath.trim().length() > 0) {
				imageLoader.loadImage(get_ImagePath, GetImageOptions(), new SimpleImageLoadingListener() {
					public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
						super.onLoadingComplete(imageUri, view, loadedImage);
						profileImage.setImageBitmap(loadedImage);
					}
				});
			} else {
				// profileImage.setImageResource(R.drawable.dumy);
			}
		} catch (Exception e) {
			// profileImage.setImageResource(R.drawable.dumy);
			e.printStackTrace();
		}
	}

	public static void urlLoadImage(ImageLoader imageLoader, String get_ImagePath, SimpleImageLoadingListener listener) {
		try {
			if (get_ImagePath != null && get_ImagePath.trim().length() > 0) {
				imageLoader.loadImage(get_ImagePath, GetImageOptions(), listener);
			} else {
				// profileImage.setImageResource(R.drawable.dumy);
			}
		} catch (Exception e) {
			// profileImage.setImageResource(R.drawable.dumy);
			e.printStackTrace();
		}
	}

	public static void startNewMainActivity(Activity currentActivity, Class<? extends Activity> newTopActivityClass) {
		Intent intent = new Intent(currentActivity, newTopActivityClass);
		intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB)
			intent.addFlags(0x8000); // equal to Intent.FLAG_ACTIVITY_CLEAR_TASK
										// which is only available from API
										// level 11
		currentActivity.startActivity(intent);
	}

	public static DisplayImageOptions GetImageOptions() {
		DisplayImageOptions options = new DisplayImageOptions.Builder().showImageOnLoading(null)
				.showImageForEmptyUri(null).showImageOnFail(null).cacheInMemory(true).cacheOnDisc(false)
				.bitmapConfig(Bitmap.Config.RGB_565).imageScaleType(ImageScaleType.EXACTLY).build();

		return options;
	}

	public static void displayAlert(final Activity context, String message, final View.OnClickListener clickListener) {

		Util.displayAlert(context, message, null, null, clickListener);
	}

	public static void displayAlert(final Activity context, String message, String positiveButton,
			String negativeButton, final View.OnClickListener clickListener) {

		final Dialog dialog = new Dialog(context);
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		WindowManager.LayoutParams wmlp = dialog.getWindow().getAttributes();

		wmlp.gravity = Gravity.CENTER;

		dialog.setContentView(R.layout.dialog_ok_alert_layout);
		dialog.getWindow().setBackgroundDrawable(new ColorDrawable(0));

		// dialog.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
		dialog.setCanceledOnTouchOutside(false);

		TextView title = (TextView) dialog.findViewById(R.id.titleText);
		title.setText(R.string.app_name);

		TextView messageText = (TextView) dialog.findViewById(R.id.messageText);
		messageText.setText(message);

		Button ok_button = (Button) dialog.findViewById(R.id.ok_button);
		if (positiveButton != null)
			ok_button.setText(positiveButton);
		ok_button.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				dialog.dismiss();
				if (clickListener != null)
					clickListener.onClick(v);
			}

		});

		Button cancel_button = (Button) dialog.findViewById(R.id.dialog_cancel_button);
		if (negativeButton != null)
			cancel_button.setText(negativeButton);
		cancel_button.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				dialog.dismiss();
			}

		});
		dialog.show();
	}

	/**
	 * Checks if the user logged in and return true if logged in
	 * 
	 * @param c
	 *            Context
	 * @return true, if logged in.
	 */
	public static boolean isLoggedIn(Context c) {
		return Util.getBoolWithKey(Constants.IS_LOGGED_IN, c);
	}

	public static void saveUser(User user, Context c) {
		Util.saveBoolWithKey(Constants.IS_LOGGED_IN, true, c);
		Gson gson = new Gson();
		String json = gson.toJson(user);
		Util.saveStringWithKey(Constants.LOGGED_IN_USER, json, c);
	}

	public static boolean removeUserAndLogout(Context c) {
		SharedPreferences prefsPrivate = c.getSharedPreferences(Constants.SHARED_PREFS_FILE_NAME, Context.MODE_PRIVATE);
		Editor editor = prefsPrivate.edit();
		editor.clear();
		return editor.commit();
	}

	public static User getLoggedInUser(Context c) {
		Gson gson = new Gson();
		String json = Util.getStringWithKey(Constants.LOGGED_IN_USER, c);
		User u = gson.fromJson(json, User.class);
		// u.setUserImageURL("https://pbs.twimg.com/profile_images/454764261512736768/izJQIAt-.jpeg");
		return u;
	}

	public static boolean saveStringWithKey(String key, String value, Context c) {
		SharedPreferences pref = c.getSharedPreferences(Constants.SHARED_PREFS_FILE_NAME, Context.MODE_PRIVATE);
		Editor edit = pref.edit();
		edit.putString(key, value);
		return edit.commit();
	}

	public static boolean saveBoolWithKey(String key, boolean value, Context c) {
		SharedPreferences pref = c.getSharedPreferences(Constants.SHARED_PREFS_FILE_NAME, Context.MODE_PRIVATE);
		Editor edit = pref.edit();
		edit.putBoolean(key, value);
		return edit.commit();
	}

	public static boolean getBoolWithKey(String key, Context c) {
		SharedPreferences pref = c.getSharedPreferences(Constants.SHARED_PREFS_FILE_NAME, Context.MODE_PRIVATE);
		return pref.getBoolean(key, false);
	}

	public static String getStringWithKey(String key, Context c) {
		SharedPreferences pref = c.getSharedPreferences(Constants.SHARED_PREFS_FILE_NAME, Context.MODE_PRIVATE);
		return pref.getString(key, null);
	}

	public static String getCharacterDataFromElement(Element e) {
		Node child = e.getFirstChild();
		if (child instanceof CharacterData) {
			CharacterData cd = (CharacterData) child;
			return cd.getData();
		}
		return "";
	}

	public static String getStringValueFromElementForKey(Element element, String key) {
		NodeList nodeList = element.getElementsByTagName(key);
		Element nodeListElement = (Element) nodeList.item(0);
		return Util.getCharacterDataFromElement(nodeListElement);
	}

	public static void makeMaskImage(Context context, ImageView mImageView, Bitmap contentBitmap) {

		int frame_mask = R.drawable.thumb_mask;
		int frame = R.drawable.thumb_border;

		Bitmap mask = BitmapFactory.decodeResource(context.getResources(), frame_mask);
		Bitmap original = getResizedBitmap(contentBitmap, mask.getWidth(), mask.getHeight());
		Bitmap result = Bitmap.createBitmap(mask.getWidth(), mask.getHeight(), Config.ARGB_8888);
		Canvas mCanvas = new Canvas(result);
		Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG);
		paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.DST_IN));
		mCanvas.drawBitmap(original, 0, 0, null);
		mCanvas.drawBitmap(mask, 0, 0, paint);
		paint.setXfermode(null);
		mImageView.setImageBitmap(result);
		mImageView.setScaleType(ScaleType.CENTER_CROP);
		mImageView.setBackgroundResource(frame);
	}

	private static Bitmap getResizedBitmap(Bitmap bm, int newWidth, int newHeight) {

		int width = bm.getWidth();
		int height = bm.getHeight();

		float scaleWidth = ((float) newWidth) / width;
		float scaleHeight = ((float) newHeight) / height;
		Matrix matrix = new Matrix();
		matrix.postScale(scaleWidth, scaleHeight);
		// recreate the new Bitmap
		Bitmap resizedBitmap = Bitmap.createBitmap(bm, 0, 0, width, height, matrix, false);
		return resizedBitmap;
	}

	public static boolean saveLocation(Location location, Context c) {
		if (location == null)
			return false;
		SharedPreferences pref = c.getSharedPreferences(Constants.SHARED_PREFS_FILE_NAME, Context.MODE_PRIVATE);
		Editor edit = pref.edit();
		edit.putLong("lat", Double.doubleToLongBits(location.getLatitude()));
		edit.putLong("lng", Double.doubleToLongBits(location.getLongitude()));
		return edit.commit();
	}

	public static Location getLocation(Context c) {
		SharedPreferences prefs = c.getSharedPreferences(Constants.SHARED_PREFS_FILE_NAME, Context.MODE_PRIVATE);
		Location location = null;
		double lat = Double.longBitsToDouble(prefs.getLong("lat", 0));
		double lng = Double.longBitsToDouble(prefs.getLong("lng", 0));
		if (lat > 0 && lng > 0) {
			location = new Location("ADIL_IS_AWESOME");
			location.setLatitude(lat);
			location.setLongitude(lng);
		}
		return location;
	}

	private static final int TWO_MINUTES = 1000 * 60 * 2;

	/**
	 * Determines whether one Location reading is better than the current
	 * Location fix
	 * 
	 * @param location
	 *            The new Location that you want to evaluate
	 * @param currentBestLocation
	 *            The current Location fix, to which you want to compare the new
	 *            one
	 */
	public static boolean isBetterLocation(Location location, Location currentBestLocation) {
		if (currentBestLocation == null) {
			// A new location is always better than no location
			return true;
		}

		// Check whether the new location fix is newer or older
		long timeDelta = location.getTime() - currentBestLocation.getTime();
		boolean isSignificantlyNewer = timeDelta > TWO_MINUTES;
		boolean isSignificantlyOlder = timeDelta < -TWO_MINUTES;
		boolean isNewer = timeDelta > 0;

		// If it's been more than two minutes since the current location, use
		// the new location
		// because the user has likely moved
		if (isSignificantlyNewer) {
			return true;
			// If the new location is more than two minutes older, it must be
			// worse
		} else if (isSignificantlyOlder) {
			return false;
		}

		// Check whether the new location fix is more or less accurate
		int accuracyDelta = (int) (location.getAccuracy() - currentBestLocation.getAccuracy());
		boolean isLessAccurate = accuracyDelta > 0;
		boolean isMoreAccurate = accuracyDelta < 0;
		boolean isSignificantlyLessAccurate = accuracyDelta > 200;

		// Check if the old and new location are from the same provider
		boolean isFromSameProvider = isSameProvider(location.getProvider(), currentBestLocation.getProvider());

		// Determine location quality using a combination of timeliness and
		// accuracy
		if (isMoreAccurate) {
			return true;
		} else if (isNewer && !isLessAccurate) {
			return true;
		} else if (isNewer && !isSignificantlyLessAccurate && isFromSameProvider) {
			return true;
		}
		return false;
	}

	/** Checks whether two providers are the same */
	public static boolean isSameProvider(String provider1, String provider2) {
		if (provider1 == null) {
			return provider2 == null;
		}
		return provider1.equals(provider2);
	}
	
	public static String getNameForRestaurantId(String id){
	    /*<string name="bk_bakers">Bundu Khan Bakers</string>
		<string name="bk_lahore">Bundu Khan Lahore</string>
		<string name="bk_multan">Bundu Khan Multan</string>
		<string name="bk_balochi_sajji">Balochi Sajji</string>*/
	    HashMap<String, String> map = new HashMap<String, String>();    
	    map.put("7274", "Bundu Khan Bakers");
	    map.put("7", "Bundu Khan Lahore");
	    map.put("7273", "Bundu Khan Multan");
	    map.put("34", "Balochi Sajji");
	    
	    return map.get(id);
	    
	}

}
