/**
 * Constants.java
 * BooleanBites Ltd. (c) 2015
 * @author adilsoomro
 */
package com.saltnpepper.app.utils;

/**
 * @author adilsoomro
 *
 */
public class Constants {
    public static String IS_LOGGED_IN = "IS_LOGGED_IN";
    public static String LOGGED_IN_USER = "LOGGED_IN_USER";
    public static String SHARED_PREFS_FILE_NAME = "bk_app_shared_prefs";
    
    public static String BRANCH_ID_KEY = "BRANCH_ID_KEY";
    
    public static String BRANCH_LOCATATION_KEY = "BRANCH_LOCATATION_KEY";
    
    public static String RESTAURANT_ID_KEY = "RESTAURANT_ID_KEY";

    public static String RESTAURANT_ID = "RESTAURANT_ID";
    
    public static final String EATINGOUT_ID = "967";
    public static final String FAKE_EATINGOUT_ID = "7306";
    
    public static final String CITY = "Lahore";
    
    public static String IMEI_NUMBER = "IMEI_NUMBER";
    public static String CARRIER_NAME = "CARRIER_NAME";

    public static String test = "test";
    public static int n = 0;

}
