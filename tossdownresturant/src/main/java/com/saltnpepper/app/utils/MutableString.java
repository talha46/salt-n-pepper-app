package com.saltnpepper.app.utils;


public class MutableString{
	
	private String mutableString;
	
	public MutableString(String mutableString){
		this.mutableString = mutableString;
	}
	
	public MutableString(byte[] stringBytes){
		String stringValue = new String(stringBytes);
		this.mutableString = stringValue;
	}
	
	public String getString() {
		return mutableString;
	}

	public void setString(String mutableString) {
		this.mutableString = mutableString;
	} 

}
