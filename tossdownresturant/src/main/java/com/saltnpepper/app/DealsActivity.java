package com.saltnpepper.app;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.assist.SimpleImageLoadingListener;
import com.saltnpepper.app.datamodel.Deal;
import com.saltnpepper.app.datamodel.Deals;
import com.saltnpepper.app.datamodel.Eatout;
import com.saltnpepper.app.utils.MutableString;
import com.saltnpepper.app.utils.Util;
import com.saltnpepper.app.webservices.Webservice;

import java.util.ArrayList;
import java.util.HashMap;

public class DealsActivity extends Activity {

	private ListView mDealsList;
	private ArrayList<Eatout> mEatouts = null;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_deals_list_layout);

		mDealsList = (ListView) findViewById(R.id.dealsList);
		ImageView back_arrow = (ImageView) findViewById(R.id.back_arrow);
		back_arrow.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				finish();
			}
		});
		HashMap<String, String> param = new HashMap<String, String>();
		new GetDealsAsyncTask().execute(param);

	}

	private class GetDealsAsyncTask extends AsyncTask<HashMap<String, String>, Void, Deals> {

		private ProgressDialog pdialog;
		private MutableString errorMessage = new MutableString("");

		@Override
		protected void onPreExecute() {

			super.onPreExecute();
			pdialog = new ProgressDialog(DealsActivity.this);
			pdialog.setTitle("Fetching Deals.");
			pdialog.setMessage("Please wait...");
			pdialog.setCanceledOnTouchOutside(false);
			pdialog.setCancelable(false);
			pdialog.show();
		}

		@Override
		protected Deals doInBackground(HashMap<String, String>... params) {

			return Webservice.getDeals(params[0], DealsActivity.this);
		}

		@Override
		protected void onPostExecute(Deals result) {

			super.onPostExecute(result);
			mEatouts = result.getEatouts();
			if (pdialog != null && pdialog.isShowing()) {

				pdialog.dismiss();
				pdialog = null;
			}
			if (mEatouts != null && mEatouts.size() > 0) {

				MenuDetailsAdapter adapter = new MenuDetailsAdapter(DealsActivity.this);
				mDealsList.setAdapter(adapter);

			} else if (errorMessage.getString().length() > 0) {

				Util.showAlert(errorMessage.getString(), DealsActivity.this, false);
			}
		}
	}

	private class ViewHolder {
		TextView textDealName, textCurrency, textPrice, textDealDetails;
		ImageView dealImage, arrowImage;
		public ProgressBar progressbar;
		public RelativeLayout leftRl;
	}

	private class MenuDetailsAdapter extends BaseAdapter {

		private LayoutInflater inflater = null;

		public MenuDetailsAdapter(Context context) {

			inflater = LayoutInflater.from(context);
		}

		@Override
		public int getCount() {
			return mEatouts.size();
		}

		@Override
		public Object getItem(int position) {
			return mEatouts.get(position);
		}

		@Override
		public long getItemId(int position) {
			return position;
		}

		@Override
		public View getView(final int position, View convertView, ViewGroup parent) {

			ViewHolder holder = null;

			if (convertView == null) {

				holder = new ViewHolder();

				convertView = inflater.inflate(R.layout.deals_list_item_new, null);
				// holder.textName = (TextView)
				// convertView.findViewById(R.id.textName);
				holder.textDealName = (TextView) convertView.findViewById(R.id.textDealName);
				holder.textCurrency = (TextView) convertView.findViewById(R.id.textCurrency);
				holder.textPrice = (TextView) convertView.findViewById(R.id.textPrice);
				// holder.textTax = (TextView)
				// convertView.findViewById(R.id.textTax);
				holder.textDealDetails = (TextView) convertView.findViewById(R.id.textDealDetails);
				holder.dealImage = (ImageView) convertView.findViewById(R.id.dealImage);
				holder.progressbar = (ProgressBar) convertView.findViewById(R.id.progressbar);
				holder.arrowImage = (ImageView) convertView.findViewById(R.id.arrowImage);
				holder.leftRl = (RelativeLayout) convertView.findViewById(R.id.leftRl);
				convertView.setTag(holder);
			} else {

				holder = (ViewHolder) convertView.getTag();
			}
			final Eatout eatout = mEatouts.get(position);
			// holder.textName.setText(eatout.getName());
			final Deal eatoutDeal = eatout.getDeals().get(position);
			holder.textDealName.setText(eatoutDeal.getDealname());
			holder.textCurrency.setText(eatoutDeal.getCurrency() + " + \nTax");
			holder.textPrice.setText(eatoutDeal.getDealprice());
			// holder.textTax.setText(eatoutDeal.getTax());
			holder.textDealDetails.setText(eatoutDeal.getDealdetails());

			ImageLoader imageLoader = ImageLoader.getInstance();
			imageLoader.init(ImageLoaderConfiguration.createDefault(DealsActivity.this));
			//Util.urlLoadImage(imageLoader, holder.dealImage, eatout.getEatoutLogo());
			final ImageView dImage = holder.dealImage;

			final ProgressBar progressbar = holder.progressbar;
			String dealImageUrl = eatout.getDeals().get(position).getDealimg();
			Util.urlLoadImage(imageLoader, dealImageUrl, new SimpleImageLoadingListener() {
				public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {

					progressbar.setVisibility(View.GONE);
					super.onLoadingComplete(imageUri, view, loadedImage);
					dImage.setImageBitmap(loadedImage);
				}

				@Override
				public void onLoadingStarted(String imageUri, View view) {
					progressbar.setVisibility(View.VISIBLE);
					super.onLoadingStarted(imageUri, view);
				}

				@Override
				public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
					super.onLoadingFailed(imageUri, view, failReason);
					progressbar.setVisibility(View.GONE);
				}
			});

			holder.dealImage.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// start deaildetail activity
					Intent i=new Intent(DealsActivity.this, DealDetailActivity.class);
					i.putExtra("dealImageUrl", eatoutDeal.getDealimg());
					startActivity(i);
				}
			});
			holder.leftRl.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// show deaildetail dialog
					showDealDialog(eatoutDeal, eatout);
				}
			});
			return convertView;
		}
	}
	
	private void showDealDialog(Deal eatoutDeal, Eatout eatout){
		
		String dealInfo = "Price: " + eatoutDeal.getDealprice() +"\n"
				+ "Timing: " + eatoutDeal.getTiming() +"\n"
				+ "Detail: " + eatoutDeal.getDealdetails() +"\n"
				+ "Eatout Name: " + eatout.getName();
		Util.showMessage(eatoutDeal.getDealname(), dealInfo, DealsActivity.this);
	}
}
