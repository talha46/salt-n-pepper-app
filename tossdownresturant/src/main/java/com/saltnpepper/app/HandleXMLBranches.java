package com.saltnpepper.app;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by talha on 8/4/16.
 */
public class HandleXMLBranches{
    private String address = "address";
    private String r_name = "r_name";
    private String area = "area";
    private String map = "map";
    private String phone = "phone";

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getR_name() {
        return r_name;
    }

    public void setR_name(String r_name) {
        this.r_name = r_name;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public String getMap() {
        return map;
    }

    public void setMap(String map) {
        this.map = map;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    private String urlString = null;
    private XmlPullParserFactory xmlFactoryObject;
    public volatile boolean parsingComplete = true;
    String test = null;

    public HandleXMLBranches(String url){
        this.urlString = url;
    }



    public void parseXMLAndStoreIt(XmlPullParser myParser) {
        int event;
        String txt=null;

        try {
            event = myParser.getEventType();

            while (event != XmlPullParser.END_DOCUMENT) {
                String name=myParser.getName();

                switch (event){
                    case XmlPullParser.START_TAG:
                    {
                        break;
                    }

                    case XmlPullParser.TEXT:
                        txt = myParser.getText();
                        break;

                    case XmlPullParser.END_TAG:
                       if(name.equals("address")){
                            address = txt;
                        }

                        else if(name.equals("r_name")){
                            r_name = txt;//myParser.getAttributeValue(null,"value");
                        }

                        else if(name.equals("area")){
                            area = txt;//myParser.getAttributeValue(null,"value");
                        }

                        else if(name.equals("map")){
                            map = txt;//myParser.getAttributeValue(null,"value");
                        }
                        else if(name.equals("phone"))
                        {
                            phone = txt;
                        }
                        else{
                        }
                        break;
                }
                event = myParser.next();
            }
            parsingComplete = false;
        }

        catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void fetchXML(){
        Thread thread = new Thread(new Runnable(){
            @Override
            public void run() {
                try {
                    URL url = new URL(urlString);
                    HttpURLConnection conn = (HttpURLConnection)url.openConnection();

                    conn.setReadTimeout(10000 /* milliseconds */);
                    conn.setConnectTimeout(15000 /* milliseconds */);
                    conn.setRequestMethod("GET");
                    conn.setDoInput(true);
                    conn.connect();

                    InputStream stream = conn.getInputStream();
                    xmlFactoryObject = XmlPullParserFactory.newInstance();
                    XmlPullParser myparser = xmlFactoryObject.newPullParser();

                    myparser.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, false);
                    myparser.setInput(stream, null);

                    parseXMLAndStoreIt(myparser);
                    stream.close();
                }
                catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
        thread.start();
    }
}