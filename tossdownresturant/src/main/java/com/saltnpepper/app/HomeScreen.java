package com.saltnpepper.app;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.View;

import com.saltnpepper.app.utils.Constants;
import com.saltnpepper.app.utils.Util;

import java.util.ArrayList;

import static com.saltnpepper.app.RegistrationActivity.callFacebookLogout;

/**
 * Created by talha on 8/17/16.
 */
public class HomeScreen extends Activity {

    ViewPager viewPager;
    private static ViewPager mPager;
    private static int currentPage = 0;
    private static int NUM_PAGES = 0;
    private static final Integer[] IMAGES= {R.drawable.d1,R.drawable.d2,R.drawable.d3,R.drawable.d4};
    private ArrayList<Integer> ImagesArray = new ArrayList<Integer>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setContentView(R.layout.home_screen);

        viewPager = (ViewPager) findViewById(R.id.viewPager);
        PagerAdapter adapter = new CustomAdapter(HomeScreen.this);
        viewPager.setAdapter(adapter);
    }

    public void letsSignOut(View button) {
        Util.displayAlert(HomeScreen.this, "Are you sure you want to logout", new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                //clsoe facebook session
                callFacebookLogout(HomeScreen.this);
                if(DashboardActivity.getInstance() != null){
                    DashboardActivity.getInstance().unRegisterGCM();
                }
                Util.removeUserAndLogout(HomeScreen.this);
                Util.startNewMainActivity(HomeScreen.this, SigninActivity.class);
            }
        });
    }

    public void restDetailsClick(View v)
    {
        String id = v.getTag().toString();
        Intent i = new Intent(HomeScreen.this, RestuarantDetails.class);
        i.putExtra(Constants.RESTAURANT_ID_KEY, id);
        Constants.RESTAURANT_ID = id;
        //Toast.makeText(HomeScreen.this,id,Toast.LENGTH_LONG).show();
        startActivity(i);
    }


}
