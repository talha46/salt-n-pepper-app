package com.saltnpepper.app;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.saltnpepper.app.cart.ShoppingCart;
import com.saltnpepper.app.cart.ShoppingCart.ShoppingCartUpdateListener;
import com.saltnpepper.app.datamodel.DishDetail;
import com.saltnpepper.app.datamodel.MenuDetails;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

public class MenuItemDetailActivity extends Activity {

	private ListView mMenuList;
	private ArrayList<DishDetail> mDishes = null;
	private TextView mTextCheckoutAmount;

	@SuppressWarnings("unchecked")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);

		Bundle data = getIntent().getExtras();
		if(data!=null){
			mDishes = (ArrayList<DishDetail>) data.getSerializable("dishes");
			Collections.sort(mDishes, new DishComparator());
		}
		setContentView(R.layout.activity_dishes_list_layout);
		ShoppingCart.getInstance().calculateCheckoutAmount(mTextCheckoutAmount);
		
		mMenuList = (ListView) findViewById(R.id.menuList);
		DishDetailsAdapter adapter = new DishDetailsAdapter(this);
		mMenuList.setAdapter(adapter);

		mTextCheckoutAmount = (TextView) findViewById(R.id.checkout_amount);

		ImageView back_arrow = (ImageView) findViewById(R.id.back_arrow);
		back_arrow.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				finish();

			}
		});
		LinearLayout shopping_cart = (LinearLayout) findViewById(R.id.shopping_cart);
		shopping_cart.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				if (ShoppingCart.getInstance().getOrderItems() != null
						&& ShoppingCart.getInstance().getOrderItems().size() > 0) {
					startActivity(new Intent(MenuItemDetailActivity.this, CartDetailActivity.class));
				} else {
					Toast.makeText(MenuItemDetailActivity.this, "Your cart is empty.", Toast.LENGTH_SHORT).show();
				}
			}
		});
	}

	@Override
	protected void onResume() {

		super.onResume();
		ShoppingCart.setOnShoppingCartUpdateListener(new ShoppingCartUpdateListener() {
			
			@Override
			public void onShoppingCartUpdate() {
				
				ShoppingCart.getInstance().calculateCheckoutAmount(mTextCheckoutAmount);
			}
		});
		ShoppingCart.getInstance().calculateCheckoutAmount(mTextCheckoutAmount);
	}
	@Override
	protected void onPause() {
		
		super.onPause();
		ShoppingCart.setOnShoppingCartUpdateListener(null);
	}

	public class DishComparator implements Comparator<DishDetail> {
		@Override
		public int compare(DishDetail o1, DishDetail o2) {
			return o1.getName().compareTo(o2.getName());
		}
	}

	private class ViewHolder {
		RelativeLayout rl_bg;
		TextView name;
		TextView desc;
		TextView price;
	}

	private class DishDetailsAdapter extends BaseAdapter {

		private LayoutInflater inflater = null;

		public DishDetailsAdapter(Context context) {

			inflater = LayoutInflater.from(context);
		}

		@Override
		public int getCount() {

			return mDishes.size();
		}

		@Override
		public Object getItem(int position) {
			return mDishes.get(position);
		}

		@Override
		public long getItemId(int position) {
			return position;
		}

		@Override
		public View getView(final int position, View convertView, ViewGroup parent) {

			ViewHolder holder = null;

			if (convertView == null) {

				holder = new ViewHolder();

				convertView = inflater.inflate(R.layout.dishes_list_item, null);
				holder.rl_bg = (RelativeLayout) convertView.findViewById(R.id.rl_bg);
				holder.name = (TextView) convertView.findViewById(R.id.name);
				holder.desc = (TextView) convertView.findViewById(R.id.desc);
				holder.price = (TextView) convertView.findViewById(R.id.price);
				convertView.setTag(holder);
			} else {

				holder = (ViewHolder) convertView.getTag();
			}
			if ((position % 2) == 0) {
				holder.rl_bg.setBackgroundResource(R.drawable.list_item_bg_even);
			}
			final DishDetail dish = mDishes.get(position);
			holder.name.setText(dish.getName());
			holder.desc.setText(dish.getDescription());
			dish.showPrice(holder.price);
			
			convertView.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// Intent i = new Intent(MenuItemDetailActivity.this,
					// AddToCartActivity.class);
					// i.putExtra("dish", dish);
					// startActivity(i);
					showAddToCardDialog(dish);
				}
			});
			return convertView;
		}
	}

	private int itemQuantity = 1;
	void showAddToCardDialog(final DishDetail dish) {

		itemQuantity = 1;
		final Dialog dialog = new Dialog(MenuItemDetailActivity.this);
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		WindowManager.LayoutParams wmlp = dialog.getWindow().getAttributes();

		wmlp.gravity = Gravity.TOP | Gravity.CENTER_HORIZONTAL;

		wmlp.y = 120;
		dialog.setContentView(R.layout.dialog_add_to_cart_layout);
		dialog.getWindow().setBackgroundDrawable(new ColorDrawable(0));

		dialog.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
		dialog.setCanceledOnTouchOutside(false);
		
		TextView title = (TextView) dialog.findViewById(R.id.txt_title);
		title.setText(dish.getName());
		
		TextView text_price = (TextView) dialog.findViewById(R.id.text_price);
		dish.showPrice(text_price);
		
		final TextView text_total = (TextView) dialog.findViewById(R.id.text_total_price);
		dish.showTotalPrice(text_total, itemQuantity);
		
		final TextView text_quantity = (TextView) dialog.findViewById(R.id.text_quantity);
		text_quantity.setText(itemQuantity+"");
		
		View plusButton = (View) dialog.findViewById(R.id.buttonPlus);
		plusButton.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				if(itemQuantity<=1000){
					itemQuantity++;
					text_quantity.setText(itemQuantity+"");
					dish.showTotalPrice(text_total, itemQuantity);
				}
			}
		});
		View plusMinus = (View) dialog.findViewById(R.id.buttonMinus);
		plusMinus.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				if(itemQuantity > 1){
					itemQuantity--;
					text_quantity.setText(itemQuantity+"");
					dish.showTotalPrice(text_total, itemQuantity);
				}
			}
		});
		View closeButton = (View) dialog.findViewById(R.id.image_button_close);
		closeButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				dialog.dismiss();
			}
		});
		
		Button doneButton = (Button) dialog.findViewById(R.id.button_done);
		doneButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				ShoppingCart.getInstance().addItem(dish, itemQuantity);
				ShoppingCart.getInstance().setGst_percent(MenuDetails.getTax());
				ShoppingCart.getInstance().setDiscount_percent(MenuDetails.getDiscount());
				
				dialog.dismiss();
			}
		});
		
		dialog.show();
	}
}
