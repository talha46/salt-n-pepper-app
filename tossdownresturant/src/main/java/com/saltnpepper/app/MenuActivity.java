package com.saltnpepper.app;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.BaseExpandableListAdapter;
import android.widget.Button;
import android.widget.ExpandableListView;
import android.widget.ExpandableListView.OnChildClickListener;
import android.widget.ExpandableListView.OnGroupClickListener;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.saltnpepper.app.cart.ShoppingCart;
import com.saltnpepper.app.cart.ShoppingCart.ShoppingCartUpdateListener;
import com.saltnpepper.app.datamodel.DishDetail;
import com.saltnpepper.app.datamodel.MenuDetails;
import com.saltnpepper.app.utils.Constants;
import com.saltnpepper.app.utils.Util;
import com.saltnpepper.app.webservices.Webservice;

import java.util.ArrayList;
import java.util.LinkedHashMap;

public class MenuActivity extends Activity {

	private ExpandableListView mMenuList;
	private MenuDetailsAdapter menuAdapter;
	private TextView mTextCheckoutAmount;

	private String restaurantId;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_menu_list_layout);
		restaurantId = getIntent().getStringExtra(Constants.RESTAURANT_ID_KEY);
		mMenuList = (ExpandableListView) findViewById(R.id.menuList);
		Void params = null;
		new GetMenuDetailAsyncTask().execute(params);
		this.menuAdapter = new MenuDetailsAdapter(MenuActivity.this);
		mMenuList.setAdapter(menuAdapter);

		mTextCheckoutAmount = (TextView) findViewById(R.id.checkout_amount);

		mMenuList.setOnGroupClickListener(new OnGroupClickListener() {
			@Override
			public boolean onGroupClick(ExpandableListView parent, View v, int groupPosition, long id) {
				if (parent.isGroupExpanded(groupPosition)) {
					parent.collapseGroup(groupPosition);
				} else {
					parent.expandGroup(groupPosition);
				}
				return true;
			}
		});

		mMenuList.setOnChildClickListener(new OnChildClickListener() {

			@Override
			public boolean onChildClick(ExpandableListView list, View view, int groupPosition, int childPosition,
				long id) {
			    DishDetail dish = (DishDetail) menuAdapter.getChild(groupPosition, childPosition);

			    showAddToCardDialog(dish);
			    return true;
			}
		});

		/*LinearLayout shopping_cart = (LinearLayout) findViewById(R.id.menu_shopping_cart);
		shopping_cart.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				if (ShoppingCart.getInstance().getOrderItems() != null
						&& ShoppingCart.getInstance().getOrderItems().size() > 0) {
					startActivity(new Intent(MenuActivity.this, CartDetailActivity.class));
				} else {
					Toast.makeText(MenuActivity.this, "Your cart is empty.", Toast.LENGTH_SHORT).show();
				}
			}
		});*/
	}

	@Override
	protected void onResume() {

		super.onResume();
		ShoppingCart.setOnShoppingCartUpdateListener(new ShoppingCartUpdateListener() {

			@Override
			public void onShoppingCartUpdate() {

				ShoppingCart.getInstance().calculateCheckoutAmount(mTextCheckoutAmount);
			}
		});
		ShoppingCart.getInstance().calculateCheckoutAmount(mTextCheckoutAmount);
	}

	@Override
	protected void onPause() {

		super.onPause();
		ShoppingCart.setOnShoppingCartUpdateListener(null);
	}

	private class GetMenuDetailAsyncTask extends AsyncTask<Void, Void, MenuDetails> {

		private ProgressDialog pdialog;
		
		@Override
		protected void onPreExecute() {

			super.onPreExecute();
			pdialog = new ProgressDialog(MenuActivity.this);
			pdialog.setTitle("Menu Detail.");
			pdialog.setMessage("Please wait...");
			pdialog.setCanceledOnTouchOutside(false);
			pdialog.setCancelable(false);
			pdialog.show();
		}

		@Override
		protected MenuDetails doInBackground(Void... params) {

			return Webservice.getMenuDetails(restaurantId, MenuActivity.this);
		}

		@Override
		protected void onPostExecute(MenuDetails menuData) {

			super.onPostExecute(menuData);
			if (pdialog != null && pdialog.isShowing()) {

				pdialog.dismiss();
				pdialog = null;
			}
			if (menuData != null && menuData.getMenu() != null && menuData.getMenu().size() > 0) {
				menuAdapter.setMenu(menuData);				
			} else{
				Util.showAlert("There was an error in connecting to server, Please try again.", MenuActivity.this, false);
			}
		}
	}

	private class ViewHolder {
		RelativeLayout rl_bg;
		TextView name;
		TextView quantity;
		ImageView arrowImageView;
		//View sep;
	}

	private class ViewHolderChild {
		TextView name;
		TextView desc;
		TextView price;
	}

	private class MenuDetailsAdapter extends BaseExpandableListAdapter {

		private LayoutInflater inflater = null;
		private LinkedHashMap<String, ArrayList<DishDetail>> menuData = null;
		private Object[] categories = null;
		private Context context;
		
		public MenuDetailsAdapter(Context context) {
			inflater = LayoutInflater.from(context);
			this.context = context;
		}

		public void setMenu(MenuDetails mdata) {
			this.menuData = mdata.getMenu();
			categories = menuData.keySet().toArray();
			//Arrays.sort(categories);
			notifyDataSetChanged();
		}

		@Override
		public Object getChild(int groupPosition, int childPosition) {
			String category = (String) getGroup(groupPosition);
			ArrayList<DishDetail> dishes = menuData.get(category);
			return dishes.get(childPosition);

		}

		@Override
		public long getChildId(int groupPosition, int childPosition) {
			return 0;
		}

		@Override
		public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView,
				ViewGroup parent) {
			ViewHolderChild holder = null;

			if (convertView == null) {
				holder = new ViewHolderChild();
				convertView = inflater.inflate(R.layout.dishes_list_item, null);
				holder.name = (TextView) convertView.findViewById(R.id.name);
				holder.desc = (TextView) convertView.findViewById(R.id.desc);
				holder.price = (TextView) convertView.findViewById(R.id.price);
				convertView.setTag(holder);
			} else {

				holder = (ViewHolderChild) convertView.getTag();
			}

			DishDetail dish = (DishDetail) getChild(groupPosition, childPosition); // mDishes.get(position)
			holder.name.setText(dish.getName());
			holder.name.setTextColor(context.getResources().getColor(R.color.locations_group_item_text_color));
			holder.name.setTypeface(null, Typeface.NORMAL);
			holder.desc.setText(dish.getDescription());
			holder.desc.setTextColor(context.getResources().getColor(R.color.locations_group_item_text_color));
			holder.price.setTextColor(context.getResources().getColor(R.color.locations_group_item_text_color));
			dish.showPrice(holder.price);
			// convertView.setTag(dish);
			// convertView.setOnClickListener(this);
			return convertView;

		}

		@Override
		public int getChildrenCount(int groupPosition) {
			String category = (String) getGroup(groupPosition);
			ArrayList<DishDetail> dishes = menuData.get(category);
			return dishes.size();
		}

		@Override
		public Object getGroup(int groupPosition) {
			if (categories != null)
				return categories[groupPosition];
			return null;
		}

		@Override
		public int getGroupCount() {
			if (categories != null)
				return categories.length;
			return 0;
		}

		@Override
		public long getGroupId(int groupPosition) {
			return groupPosition;
		}

		@Override
		public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
			ViewHolder holder = null;

			if (convertView == null) {
				holder = new ViewHolder();

				convertView = inflater.inflate(R.layout.menu_category_item, null);
				View linear = convertView.findViewById(R.id.menu_category_linear);
				holder.rl_bg = (RelativeLayout) convertView;// ;
				holder.name = (TextView) linear.findViewById(R.id.name);
				holder.quantity = (TextView) linear.findViewById(R.id.menu_category_quantity);
				//holder.sep = convertView.findViewById(R.id.menu_category_separator);
				holder.arrowImageView = (ImageView) holder.rl_bg.findViewById(R.id.menu_category_arrow);
				convertView.setTag(holder);
			} else {

				holder = (ViewHolder) convertView.getTag();
			}
			// if ((groupPosition % 2) == 0) {
			// holder.rl_bg.setBackgroundResource(R.drawable.list_item_bg_even);
			// }
			String category = (String) getGroup(groupPosition);
			holder.name.setText(category);
			final ArrayList<DishDetail> dishes = menuData.get(category);
			if (dishes != null && dishes.size() > 0) {
				holder.quantity.setText(dishes.size() + " Items");
			}
			if (isExpanded) {
				holder.arrowImageView.setImageResource(R.drawable.down_arrow);
				holder.rl_bg.setBackgroundColor(context.getResources().getColor(R.color.menu_item_category_selected_color));
				//holder.name.setTextColor(R.color.menu_item_stepper_text_bg);
				//holder.sep.setBackgroundColor(context.getResources().getColor(R.color.theme_color_red));
			} else {
				holder.arrowImageView.setImageResource(R.drawable.right_arrow);
				holder.rl_bg.setBackgroundColor(context.getResources().getColor(R.color.menu_item_normal_color));
				//holder.name.setTextColor(context.getResources().getColor(R.color.menu_item_stepper_text_bg));
				//holder.sep.setBackgroundColor(context.getResources().getColor(android.R.color.black));
			}

			// convertView.setOnClickListener(new OnClickListener() {
			//
			// @Override
			// public void onClick(View v) {
			//
			// Intent i = new Intent(MenuActivity.this,
			// MenuItemDetailActivity.class);
			// i.putExtra("dishes", dishes);
			// startActivity(i);
			// }
			// });
			return convertView;
		}

		@Override
		public boolean hasStableIds() {
			return false;
		}

		@Override
		public boolean isChildSelectable(int groupPosition, int childPosition) {
			return true;
		}
	}

	private int itemQuantity = 1;

	void showAddToCardDialog(final DishDetail dish) {

		itemQuantity = 1;
		final Dialog dialog = new Dialog(MenuActivity.this);
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		WindowManager.LayoutParams wmlp = dialog.getWindow().getAttributes();

		wmlp.gravity = Gravity.CENTER | Gravity.CENTER_HORIZONTAL;

		dialog.setContentView(R.layout.dialog_add_to_cart_layout);
		dialog.getWindow().setBackgroundDrawable(new ColorDrawable(0));

		//dialog.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
		dialog.setCanceledOnTouchOutside(false);

		TextView title = (TextView) dialog.findViewById(R.id.txt_title);
		title.setText(dish.getName());
		//title.setTextColor(this.getResources().getColor(R.color.menu_item_stepper_text_bg));

		TextView text_price = (TextView) dialog.findViewById(R.id.text_price);
		dish.showPrice(text_price);

		final TextView text_total = (TextView) dialog.findViewById(R.id.text_total_price);
		dish.showTotalPrice(text_total, itemQuantity);

		final TextView text_quantity = (TextView) dialog.findViewById(R.id.text_quantity);
		text_quantity.setText(itemQuantity + "");

		ImageView plusButton = (ImageView) dialog.findViewById(R.id.buttonPlus);
		plusButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				if (itemQuantity <= 1000) {
					itemQuantity++;
					text_quantity.setText(itemQuantity + "");
					dish.showTotalPrice(text_total, itemQuantity);
				}
			}
		});
		ImageView plusMinus = (ImageView) dialog.findViewById(R.id.buttonMinus);
		plusMinus.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				if (itemQuantity > 1) {
					itemQuantity--;
					text_quantity.setText(itemQuantity + "");
					dish.showTotalPrice(text_total, itemQuantity);
				}
			}
		});
		Button closeButton = (Button) dialog.findViewById(R.id.image_button_close);
		closeButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				dialog.dismiss();
			}
		});

		Button doneButton = (Button) dialog.findViewById(R.id.button_done);
		doneButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
			    if (ShoppingCart.getInstance().canAddForResaurantId(restaurantId)) {
				ShoppingCart.getInstance().addItemForRestaurantId(dish, itemQuantity, restaurantId);
				ShoppingCart.getInstance().setGst_percent(MenuDetails.getTax());
				ShoppingCart.getInstance().setDiscount_percent(MenuDetails.getDiscount());
			    } else {
				//user will be asked to keep previous and discard previous.
				ShoppingCart.displayDiscardCartAlertAndAddNew(MenuActivity.this, new View.OnClickListener(){
				    @Override
				    public void onClick(View v) {
					ShoppingCart.dispose();
					ShoppingCart.getInstance().addItemForRestaurantId(dish, itemQuantity, restaurantId);
					ShoppingCart.getInstance().setGst_percent(MenuDetails.getTax());
					ShoppingCart.getInstance().setDiscount_percent(MenuDetails.getDiscount());					
				    }
				    
				});
			    }
			    dialog.dismiss();
			}
		});

		dialog.show();
	}
}
