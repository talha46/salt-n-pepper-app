package com.saltnpepper.app;

import android.content.Context;
import android.graphics.BitmapFactory;
import android.os.Environment;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by talha on 8/19/16.
 */
public class ImageAdapter extends BaseAdapter {
    private Context context;
    private List<String> path_images = new ArrayList<String>();
    GridView grid_data;

    public ImageAdapter(Context context, List<String> path_images) {
        this.context = context;
        this.path_images = path_images;
    }

    @Override
    public int getCount() {
        return path_images.size();
    }

    @Override
    public Object getItem(int i) {
        return BitmapFactory.decodeFile(path_images.get(i));
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View convertView, ViewGroup parent) {
        ImageView imageView = new ImageView(context);
        imageView.setImageBitmap(BitmapFactory.decodeFile(path_images.get(i)));
        imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
        imageView.setLayoutParams(new GridView.LayoutParams(145, 145));
        return imageView;
    }


    private void present_all_images() {
        final String FS = File.separator;
        final String directory = Environment.getExternalStorageDirectory().toString() + FS + "directory";

        List<String> path_images = new ArrayList<String>();

        path_images.add(directory + FS + "image01.png");
        path_images.add(directory + FS + "image02.png");
        path_images.add(directory + FS + "image03.png");

        grid_data = (GridView) grid_data.findViewById(R.id.gridview);

        //grid_data.setAdapter(new ImageAdapter(this, path_images));

        grid_data.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View v, int position, long id) {
            }
        });

    }
}