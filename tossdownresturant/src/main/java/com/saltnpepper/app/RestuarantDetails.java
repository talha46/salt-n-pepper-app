package com.saltnpepper.app;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;

import com.saltnpepper.app.utils.Constants;
import com.saltnpepper.app.utils.Util;

/**
 * Created by talha on 8/18/16.
 */
public class RestuarantDetails extends Activity {

    private HandleXMLBranches obj;
    String restId;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setContentView(R.layout.restuarant_details);

        restId = getIntent().getStringExtra(Constants.RESTAURANT_ID_KEY);

    }

    public void photoGallery(View v)
    {
        Intent i = new Intent(getApplicationContext(),PhotoGallery.class);
        startActivity(i);
    }

    public void showBranches(View v)
    {
        String url = "http://beta1.tossdown.com/api/eatout_branches?eatoutid=18";
        //String finalUrl = url + "email=" + em.getText().toString() + "&password=" + ps.getText().toString();


        /*obj = new HandleXMLBranches(url);
        obj.fetchXML();

        while(obj.parsingComplete);

            Intent i = new Intent(this,BranchesActivity.class);
            /*i.putExtra("city", obj.getCity());
            i.putExtra("eatoutid", obj.getEatoutid());
            i.putExtra("lat",obj.getLat());
            i.putExtra("lng", obj.getLng());
            //startActivity(i);
        Toast.makeText(RestuarantDetails.this,obj.getArea(),Toast.LENGTH_LONG).show();*/


        if (Util.isNetworkAvailable(RestuarantDetails.this)) {
            Intent i = new Intent(RestuarantDetails.this, LocationsActivity.class);
            i.putExtra(Constants.RESTAURANT_ID_KEY,restId);
            startActivity(i);
        }else{
            Util.showAlert("No Internet access.", RestuarantDetails.this, true);
        }
    }


    public void showDeals(View v)
    {
        if (Util.isNetworkAvailable(RestuarantDetails.this)) {
            startActivity(new Intent(RestuarantDetails.this, DealsActivity.class));
        }else{
            Util.showAlert("No Internet access.", RestuarantDetails.this, true);
        }
    }

    public void callSNP(View v)
    {
        Intent callIntent = new Intent(Intent.ACTION_DIAL);
        callIntent.setData(Uri.parse("tel:042111100678"));
        startActivity(callIntent);
    }

    public void SNPMenu(View v)
    {
        if (Util.isNetworkAvailable(RestuarantDetails.this)) {
            Intent intent = new Intent(RestuarantDetails.this, MenuActivity.class);
            intent.putExtra(Constants.BRANCH_ID_KEY, restId);
            startActivity(intent);
            // MenuActivity.class)
        } else {
            Util.showAlert("No Internet access.", RestuarantDetails.this, true);
        }
    }

    public void makeReservation(View v)
    {
        startActivity(new Intent(RestuarantDetails.this, ReserveTableActivity.class));
    }
}
