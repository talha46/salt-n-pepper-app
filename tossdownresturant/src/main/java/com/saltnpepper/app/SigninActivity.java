package com.saltnpepper.app;

import java.util.Arrays;
import java.util.HashMap;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;
import android.widget.Toast;

import com.saltnpepper.app.datamodel.User;
import com.saltnpepper.app.utils.Util;
import com.saltnpepper.app.webservices.Webservice;
import com.facebook.FacebookAuthorizationException;
import com.facebook.FacebookOperationCanceledException;
import com.facebook.Session;
import com.facebook.SessionState;
import com.facebook.UiLifecycleHelper;
import com.facebook.model.GraphUser;
import com.facebook.widget.FacebookDialog;
import com.facebook.widget.LoginButton;

public class SigninActivity extends Activity implements OnClickListener {

	private Button continueButton;
	private EditText emailEditText, passwordEditText;
	// private ImageView facebook_image;
	private ProgressDialog dialog = null;

	private View firstView, secondView;
	private ImageView backButton;

	Animation fadeOut = null;
	Animation fadeIn = null;
	private UiLifecycleHelper uiHelper;

	private Session.StatusCallback callback = new Session.StatusCallback() {
		@Override
		public void call(Session session, SessionState state, Exception exception) {
			onSessionStateChange(session, state, exception);
		}
	};

	private FacebookDialog.Callback dialogCallback = new FacebookDialog.Callback() {
		@Override
		public void onError(FacebookDialog.PendingCall pendingCall, Exception error, Bundle data) {
			Log.d("SigninActivity", String.format("Error: %s", error.toString()));
			Toast.makeText(SigninActivity.this, String.format("Error: %s", error.toString()), Toast.LENGTH_LONG).show();
		}

		@Override
		public void onComplete(FacebookDialog.PendingCall pendingCall, Bundle data) {
			Log.d("SigninActivity", "Success!");
		}
	};

	MyAnimationListener fadOutListener = new MyAnimationListener();
	MyAnimationListener fadInListener = new MyAnimationListener();

	private final String PENDING_ACTION_BUNDLE_KEY = "com.bundukhan.app:PendingAction";

	private enum PendingAction {
		NONE, POST_PHOTO, POST_STATUS_UPDATE
	}

	private PendingAction pendingAction = PendingAction.NONE;
	private GraphUser user = null;
	private boolean performClick = true;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		uiHelper = new UiLifecycleHelper(this, callback);
		uiHelper.onCreate(savedInstanceState);
		if (savedInstanceState != null) {
			String name = savedInstanceState.getString(PENDING_ACTION_BUNDLE_KEY);
			pendingAction = PendingAction.valueOf(name);
		}

		setContentView(R.layout.signin_activity);

		emailEditText = (EditText)findViewById(R.id.em);
		passwordEditText = (EditText)findViewById(R.id.ps);

		emailEditText.setOnFocusChangeListener(new View.OnFocusChangeListener(){
			public void onFocusChange(View v, boolean hasFocus){
				if(hasFocus)
					emailEditText.setHint("");
				else
					emailEditText.setHint("Email");
			}
		});

		passwordEditText.setOnFocusChangeListener(new View.OnFocusChangeListener(){
			public void onFocusChange(View v, boolean hasFocus){
				if(hasFocus)
					passwordEditText.setHint("");
				else
					passwordEditText.setHint("Password");
			}
		});

		//continueButton = (Button) findViewById(R.id.btn_signin);
		//emailEditText = (EditText) findViewById(R.id.text_email);
		//passwordEditText = (EditText) findViewById(R.id.text_pass);
		// facebook_image = (ImageView) findViewById(R.id.facebook_image);
		// facebook_image.setOnClickListener(this);
		//continueButton.setOnClickListener(this);

//		emailEditText.setOnEditorActionListener(new OnEditorActionListener() {
//			@Override
//			public boolean onEditorAction(TextView view, int key, KeyEvent event) {
//				if (key == EditorInfo.IME_ACTION_DONE) {
//					InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
//					imm.showSoftInput(passwordEditText, InputMethodManager.SHOW_IMPLICIT);
//					return true;
//				}
//				return false;
//			}
//		});
		/*passwordEditText.setOnEditorActionListener(new OnEditorActionListener() {
			@Override
			public boolean onEditorAction(TextView view, int key, KeyEvent event) {
				if (key == EditorInfo.IME_ACTION_GO) {
					// hide virtual keyboard
		            InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
		            imm.hideSoftInputFromWindow(passwordEditText.getWindowToken(), 
		                                      InputMethodManager.RESULT_UNCHANGED_SHOWN);
		            signin();
		            return true;
		        }
		        return false;
			}
		});

		final LoginButton authButton = (LoginButton) findViewById(R.id.authButton);
		authButton.setBackgroundResource(R.drawable.signin_facebook);
		authButton.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
		authButton.setReadPermissions(Arrays.asList("public_profile", "email"));
		authButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				authButton.setUserInfoChangedCallback(new LoginButton.UserInfoChangedCallback() {

					@Override
					public void onUserInfoFetched(GraphUser user) {

						SigninActivity.this.user = user;
						confirmFBUser();
					}
				});
				if (performClick) {
					performClick = false;
					authButton.performClick();
				}
			}
		});

		firstView = findViewById(R.id.sign_in_first_pane);
		secondView = findViewById(R.id.sign_in_second_pane);
		backButton = (ImageView) findViewById(R.id.btn_signin_back);

		secondView.setVisibility(View.GONE);
		backButton.setVisibility(View.GONE);

		fadOutListener.setVisibilty(View.GONE);
		fadeOut = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.fade_out);
		fadeOut.setAnimationListener(fadOutListener);

		fadInListener.setVisibilty(View.VISIBLE);
		fadeIn = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.fade_in);
		fadeIn.setAnimationListener(fadInListener);*/

	}

	private void onSessionStateChange(Session session, SessionState state, Exception exception) {
		if (pendingAction != PendingAction.NONE
				&& (exception instanceof FacebookOperationCanceledException || exception instanceof FacebookAuthorizationException)) {
			Util.showMessage("Canceled", "Permissions not granted", SigninActivity.this);
			pendingAction = PendingAction.NONE;
		}
		confirmFBUser();
	}

	private void confirmFBUser() {
		Session session = Session.getActiveSession();
		boolean enableButtons = (session != null && session.isOpened());

		if (enableButtons && user != null) {

			// String email = emailEditText.getText().toString().trim();

			HashMap<String, String> map = new HashMap<String, String>();
			// map.put("user_email", email);
			// map.put("user_pass", "");

			map.put("user_email", user.getProperty("email") + "");
			map.put("user_fbid", user.getId());
			map.put("app_secret", getResources().getString(R.string.facebook_app_id));

			new LoginAsyncTask().execute(map);

		}
	}

	@Override
	protected void onPause() {
		super.onPause();
		uiHelper.onPause();
	}

	@Override
	protected void onResume() {
		super.onResume();
		uiHelper.onResume();
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		uiHelper.onDestroy();
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		uiHelper.onActivityResult(requestCode, resultCode, data, dialogCallback);
	}

	public void goBack(View v) {
		fadInListener.setView(firstView);
		firstView.startAnimation(fadeIn);

		fadOutListener.setView(secondView);
		secondView.startAnimation(fadeOut);

		// backButton.startAnimation(fadeOut);
		backButton.setVisibility(View.GONE);
		/*
		 * firstView.setVisibility(View.VISIBLE);
		 * secondView.setVisibility(View.GONE);
		 * backButton.setVisibility(View.GONE);
		 */
	}

	public void letsSignIn(View v) {
		/*fadInListener.setView(secondView);
		secondView.startAnimation(fadeIn);

		fadOutListener.setView(firstView);
		firstView.startAnimation(fadeOut);

		backButton.setVisibility(View.VISIBLE);*/

		/*
		 * firstView.setVisibility(View.GONE);
		 * secondView.setVisibility(View.VISIBLE);
		 * backButton.setVisibility(View.VISIBLE);
		 *
		 */

		UserSignIn();

	}

	public void letsSignup(View v) {
		startActivity(new Intent(SigninActivity.this, RegistrationActivity.class));
	}

	public void checkLoginAndContinue() {
		if (Util.isLoggedIn(this)) {
			startActivity(new Intent(SigninActivity.this, HomeScreen.class));
			finish();
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public void onClick(View v) {

		int id = v.getId();
		/*
		 * if (id == R.id.facebook_image) {
		 * 
		 * showProgressDialog("Sigining In using Facebook", "Please wait....");
		 * 
		 * } else
		 *if (id == R.id.btn_signin) {
			 if (Util.isNetworkAvailable(SigninActivity.this)) {
				 signin();
			 }else{
				 Util.showAlert("No Internet access.", SigninActivity.this, true);
			 }
		}*/
	}
	
	private void UserSignIn(){
		String email = emailEditText.getText().toString().trim();
		String pasword = passwordEditText.getText().toString().trim();

		if (email.length() <= 0) {
			Util.showMessage("Please enter email", SigninActivity.this);
			return;
		}
		if (pasword.length() <= 0) {
			Util.showMessage("Please enter password", SigninActivity.this);
			return;
		}

		HashMap<String, String> map = new HashMap<String, String>();
		map.put("user_email", email);
		map.put("user_pass", pasword);
		new LoginAsyncTask().execute(map);
	}


	private class LoginAsyncTask extends AsyncTask<HashMap<String, String>, Void, User> {

		private boolean isUsingFb = false;

		@Override
		protected void onPreExecute() {

			super.onPreExecute();
			showProgressDialog("Login in progress.", "Please wait...");
		}

		@Override
		protected User doInBackground(HashMap<String, String>... map) {
			if (map[0] != null && map[0].containsKey("user_fbid")) {
				isUsingFb = true;
			}
			return Webservice.signIn(map[0], SigninActivity.this);
		}

		@Override
		protected void onPostExecute(User usr) {
			super.onPostExecute(usr);
			dismissProgressDialog();
			if (usr.isLoggedIn()) {
				Util.saveUser(usr, SigninActivity.this);
				checkLoginAndContinue();
			} else {
				if (isUsingFb) {

					String id = user.getId();
					Util.FB_ID = id;
					// System.out.println("User= " + user);
					// phone and Address of user is not available in Latest FB
					// SDK
					// String phone = user.getProperty(propertyName);

					String name = user.getName();
					String email = user.getProperty("email") + "";
					String dateOB = user.getBirthday();
					String location = "";
					if (user.getLocation() != null) {
						location = user.getLocation().getProperty("name") + "";
					}
					Intent i = new Intent(SigninActivity.this, RegistrationActivity.class);
					i.putExtra("isUsingFBLogin", true);
					i.putExtra("name", name);
					i.putExtra("email", email);
					i.putExtra("dateOB", dateOB);
					i.putExtra("location", location);
					startActivity(i);
				} else {
					Util.showMessage(usr.getErrorMessage(), SigninActivity.this);
				}
			}
		}

	}

	private void showProgressDialog(String title, String message) {

		if (dialog == null) {
			dialog = new ProgressDialog(SigninActivity.this);
		}
		dialog.setTitle(title);
		dialog.setCanceledOnTouchOutside(false);
		dialog.setMessage(message);
		dialog.show();
	}

	private void dismissProgressDialog() {
		if (dialog != null) {
			if (dialog.isShowing()) {
				dialog.dismiss();
				dialog = null;
			}
		}
	}

	private class MyAnimationListener implements Animation.AnimationListener {
		View view;
		int visibility = 0;

		public void setView(View view) {
			this.view = view;
		}

		public void setVisibilty(int visibility) {
			this.visibility = visibility;
		}

		@Override
		public void onAnimationStart(Animation animation) {
			// Called when the Animation starts
		}

		@Override
		public void onAnimationEnd(Animation animation) {
			// Called when the Animation ended
			// Since we are fading a View out we set the visibility
			// to GONE once the Animation is finished
			view.setVisibility(visibility);
		}

		@Override
		public void onAnimationRepeat(Animation animation) {
			// This is called each time the Animation repeats
		}
	}
}
