package com.saltnpepper.app.datamodel;

import java.io.Serializable;
import java.util.Currency;
import java.util.Locale;

import android.widget.TextView;

public class DishDetail implements Serializable{

	private static final long serialVersionUID = -384302461209172575L;
	private String mName;
	private String mPrice;
	private String mDescription;
	private String mCurrency;
	private boolean mIsSelected;
	
	public DishDetail(String name, String price, String desc, String currency) {

		this.mName = name;
		
		//Preventing a non-digit price to get set.
		int itemPriceInt = 0;
		try{
		    itemPriceInt = Integer.parseInt(price);
		}catch(Exception ex){
		    itemPriceInt = 0;
		}
		this.mPrice = ""+itemPriceInt;
		
		
		this.mDescription = desc;
		this.setCurrency(currency);
	}
	public void showPrice(TextView textView){
		String price = "";
		if (getCurrency().equals("PKR")) {
			price = "Rs. " + getPrice();
		} else {
		    Currency currency = null;
		    try{
			currency = Currency.getInstance(getCurrency());
		    }catch(Exception ex){
			currency = Currency.getInstance("PKR");
		    }
		    price = currency.getSymbol(Locale.US);
		price = price + ". " + getPrice();
		}
		textView.setText(price);
	}
	public void showTotalPrice(TextView textView, int count){
		
		String price = "";
		if (getCurrency().equals("PKR")) {
			int priceInt = 0;
			try{
				priceInt = Integer.parseInt(getPrice());
			}catch(Exception ex){};
			
			
			price = "Rs. " + (priceInt * count);
		} else {
		    Currency currency = null;
		    try{
			currency = Currency.getInstance(getCurrency());
		    }catch(Exception ex){
			currency = Currency.getInstance("PKR");
		    }
			price = currency.getSymbol(Locale.US);
			price = price + ". " + (Integer.parseInt(getPrice()) * count);
		}
		textView.setText(price);
	}
	
	public String getName() {
		return mName;
	}

	public void setName(String name) {
		this.mName = name;
	}

	public String getPrice() {
		return mPrice;
	}

	public void setPrice(String price) {
		this.mPrice = price;
	}

	public String getDescription() {
		return mDescription;
	}

	public void setDescription(String description) {
		this.mDescription = description;
	}

	public String getCurrency() {
	    if(mCurrency!=null && mCurrency.length()>0)
		return mCurrency;
	    else return "PKR";
	}

	public void setCurrency(String currency) {
		this.mCurrency = currency;
	}
	public boolean ismIsSelected() {
		return mIsSelected;
	}
	public void setmIsSelected(boolean mIsSelected) {
		this.mIsSelected = mIsSelected;
	}

}
