package com.saltnpepper.app.datamodel;

import java.util.ArrayList;

public class Eatout {
	
	private String mEatoutId;
	private String mUserName;
	private String mName;
	private String mEatoutLogo;
	
	private ArrayList<Deal> mDeals;

	public Eatout(String mEatoutId, String mUserName, String mName, String mEatoutLogo, ArrayList<Deal> mDeals) {
		super();
		this.mEatoutId = mEatoutId;
		this.mUserName = mUserName;
		this.mName = mName;
		this.mEatoutLogo = mEatoutLogo;
		this.mDeals = mDeals;
	}

	public String getEatoutId() {
		return mEatoutId;
	}

	public void setEatoutId(String mEatoutId) {
		this.mEatoutId = mEatoutId;
	}

	public String getUserName() {
		return mUserName;
	}

	public void setUserName(String mUserName) {
		this.mUserName = mUserName;
	}

	public String getName() {
		return mName;
	}

	public void setName(String mName) {
		this.mName = mName;
	}

	public String getEatoutLogo() {
		return mEatoutLogo;
	}

	public void setEatoutLogo(String mEatoutLogo) {
		this.mEatoutLogo = mEatoutLogo;
	}

	public ArrayList<Deal> getDeals() {
		return mDeals;
	}

	public void setDeals(ArrayList<Deal> mDeals) {
		this.mDeals = mDeals;
	}
	
	
}
