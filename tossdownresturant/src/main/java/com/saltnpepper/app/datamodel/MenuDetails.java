package com.saltnpepper.app.datamodel;

import com.saltnpepper.app.cart.ShoppingCart;

import org.xml.sax.Attributes;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.DefaultHandler;

import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

public class MenuDetails {

	private LinkedHashMap<String, ArrayList<DishDetail>> mMenuDetail;
	private String mCartCurrency;
	private static int tax;
	private static int discount;
	private static int minSpend;
	
	public MenuDetails(String xml) {
		try {
		    mCartCurrency = "PKR";
			MenuDetailHandler menuHandle = new MenuDetailHandler();
			menuHandle.parse(xml);
		} catch (Exception e) {
			
			e.printStackTrace();
		} 
	}
	
	public LinkedHashMap<String, ArrayList<DishDetail>> getMenu(){
		return this.mMenuDetail;
	}
	public static int getTax() {
		return tax;
	}

	public static int getDiscount() {
		return discount;
	}

	public static int getMinSpend() {
		return minSpend;
	}

	public static void setMinSpend(int minSpend) {
		MenuDetails.minSpend = minSpend;
	}

	private class MenuDetailHandler extends DefaultHandler {

		private StringBuffer mBuffer = new StringBuffer();
		
		private String mCategory;
		private String mName;
		private String mPrice;
		private String mDesc;
		private String mCurrency;
		
		
		public HashMap<String, ArrayList<DishDetail>> parse(String xml) throws IOException, SAXException, ParserConfigurationException {

			
			SAXParserFactory factory = SAXParserFactory.newInstance();
			SAXParser parser = factory.newSAXParser();
			XMLReader xmlreader = parser.getXMLReader();

			MenuDetailHandler menuDetailHandler = new MenuDetailHandler();

			xmlreader.setContentHandler(menuDetailHandler);

			xmlreader.parse(new InputSource(new StringReader(xml)));
			
			ShoppingCart.getInstance().setCurrency(mCartCurrency);
			
			return mMenuDetail;
		}

		@Override
		public void startElement(String namespaceURI, String localName, String qName, Attributes atts)
				throws SAXException {
			
			mBuffer.setLength(0);
			if(qName.equals("results")){
				
				mMenuDetail = new LinkedHashMap<String, ArrayList<DishDetail>>();
			}
			if (qName.equals("item")) {
				
				mCategory = atts.getValue("category");
			}
			if (qName.equals("price")) {
				
				mCurrency = atts.getValue("currency");
				if(mCurrency.length()>0 &&  !mCartCurrency.equalsIgnoreCase(mCurrency)){
					mCartCurrency = mCurrency;
				}
			}
		}

		@Override
		public void endElement(String uri, String localName, String qName) throws SAXException {

			if (qName.equals("tax")) {
				try{
					tax = Integer.parseInt(mBuffer.toString());
				}catch(Exception ex){
					ex.printStackTrace();
					tax = 0;
				}
			}else if (qName.equals("discount")) {
				try{
					discount = Integer.parseInt(mBuffer.toString());
				}catch(Exception ex){
					ex.printStackTrace();
					discount = 0;
				}
			}else if (qName.equals("minspend")) {
				try{
					setMinSpend(Integer.parseInt(mBuffer.toString()));
				}catch(Exception ex){
					ex.printStackTrace();
					setMinSpend(0);
				}
			}else if (qName.equals("name")) {
			
				mName = removeNonBreakableSpace(mBuffer);
			}else if(qName.equals("price")){
				
				mPrice = mBuffer.toString();
			}else if(qName.equals("desc")){
				
				mDesc = mBuffer.toString();
			}else if(qName.equals("item")){
				
				DishDetail dish = new DishDetail(mName, mPrice, mDesc, mCurrency);
				if(mMenuDetail.containsKey(mCategory)){
					
					mMenuDetail.get(mCategory).add(dish);
				}else{
					
					ArrayList<DishDetail> dishes = new ArrayList<DishDetail>();
					dishes.add(dish);
					mMenuDetail.put(mCategory, dishes);
				}
				mName = "";
				mPrice = "";
				mDesc = "";
			}
		}

		@Override
		public void characters(char[] ch, int start, int length) {
			mBuffer.append(ch, start, length);
		}

	}
	
	/**
	 * Remove first and last Non Breakable Space
	 * */
	private String removeNonBreakableSpace(StringBuffer stringBuffer){
		
		String result = "";
		if(stringBuffer != null){
			char firstChar = stringBuffer.charAt(0);
			int a = (int)firstChar;
			int nbsp = 0x00a0;
			
			if(a==nbsp){
				stringBuffer.deleteCharAt(0);
			}
			char lastChar = stringBuffer.charAt(stringBuffer.length()-1);
			int b = (int)lastChar;
			if(b==nbsp){
				stringBuffer.deleteCharAt(stringBuffer.length()-1);
			}
			result = stringBuffer.toString();
		}
		return result;
	}
}
