package com.saltnpepper.app.datamodel;

import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.xml.sax.Attributes;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.DefaultHandler;

public class Locations {

	private ArrayList<Location> mLocations = null;

	public Locations(String xml) {

		LocationsHandler locationsHandler = new LocationsHandler(); 
		try {
			mLocations = locationsHandler.parse(xml);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public ArrayList<Location> getLocations() {
		return mLocations;
	}

	private class LocationsHandler extends DefaultHandler {

		private StringBuffer mBuffer = new StringBuffer();

		private String mAddress;
		private String mLatitude;
		private String mLongitude;
		private String mPhoneNumber;
		private String mBranchId;

		public ArrayList<Location> parse(String xml) throws IOException, SAXException, ParserConfigurationException {

			SAXParserFactory factory = SAXParserFactory.newInstance();
			SAXParser parser = factory.newSAXParser();
			XMLReader xmlreader = parser.getXMLReader();
			LocationsHandler menuDetailHandler = new LocationsHandler();
			xmlreader.setContentHandler(menuDetailHandler);
			xmlreader.parse(new InputSource(new StringReader(xml)));

			return mLocations;
		}

		@Override
		public void startElement(String namespaceURI, String localName, String qName, Attributes atts)
				throws SAXException {
			
			mBuffer.setLength(0);
			if (qName.equals("branches")) {
				mLocations = new ArrayList<Location>();
			}
			if (qName.equals("branch")) {
				mBranchId = atts.getValue("id");
			}
		}

		@Override
		public void endElement(String uri, String localName, String qName) throws SAXException {

			if (qName.equals("address")) {

				mAddress = mBuffer.toString();
			} else if (qName.equals("map")) {

				String map = mBuffer.toString();
				if (map != null && map.length() > 0) {
					String[] splits = map.split(",");
					mLatitude = splits[0];
					mLongitude = splits[1];
				}
			} else if (qName.equals("phone")) {

				mPhoneNumber = mBuffer.toString();
			} else if (qName.equals("branch")) {

				Location location = new Location(mAddress, mLatitude, mLongitude, mPhoneNumber, mBranchId);
				mLocations.add(location);
				mAddress = "";
				mLatitude = "";
				mLongitude = "";
				mPhoneNumber = "";
				mBranchId = "";
			}
		}

		@Override
		public void characters(char[] ch, int start, int length) {
			mBuffer.append(ch, start, length);
		}

	}
}
