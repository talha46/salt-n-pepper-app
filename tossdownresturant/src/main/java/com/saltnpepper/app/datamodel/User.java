/**
 * User.java
 * BooleanBites Ltd. (c) 2015
 * @author adilsoomro
 */
package com.saltnpepper.app.datamodel;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import java.io.IOException;
import java.io.StringReader;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import com.saltnpepper.app.utils.Util;

/**
 * @author adilsoomro
 * 
 */
public class User {

	private String id;
	private String name;
	private String fullname;
	private String email;
	private String address;
	private String city;
	private String area;
	private String dob;
	private String phone;
	private String cellphone;
	private String userImageURL;
	private String errorMessage;
	private boolean loggedIn;

	public User(NodeList user) {
		super();
		Element userElement = (Element) user.item(0);
		String id = Util.getStringValueFromElementForKey(userElement, "user_id");
		;
		String name = Util.getStringValueFromElementForKey(userElement, "user_fullname");
		;
		String fullname = Util.getStringValueFromElementForKey(userElement, "user_fullname");
		;
		String email = Util.getStringValueFromElementForKey(userElement, "user_email");
		;
		String address = Util.getStringValueFromElementForKey(userElement, "user_address");
		;
		//String area = Util.getStringValueFromElementForKey(userElement, "user_area");
		;
		String city = Util.getStringValueFromElementForKey(userElement, "user_city");
		;
		String dob = Util.getStringValueFromElementForKey(userElement, "user_dob");
		;
		String phone = Util.getStringValueFromElementForKey(userElement, "user_phone");
		;
		String cellphone = Util.getStringValueFromElementForKey(userElement, "user_cphone");
		;
		//String userImageURL = Util.getStringValueFromElementForKey(userElement, "user_pic");
		;

		this.id = id;
		this.name = name;
		this.fullname = fullname;
		this.email = email;
		this.address = address;
		//this.area = area;
		this.city = city;
		this.dob = dob;
		this.phone = phone;
		this.cellphone = cellphone;
		//this.userImageURL = userImageURL;
		this.loggedIn = true;
	}

	public User(String id, String name, String fullname, String email, String address, String dob, String phone,
			String cellphone, String userImageURL) {
		super();
		this.id = id;
		this.name = name;
		this.fullname = fullname;
		this.email = email;
		this.address = address;
		this.dob = dob;
		this.phone = phone;
		this.cellphone = cellphone;
		this.userImageURL = userImageURL;
		this.loggedIn = true;
	}

	public User() {
		super();
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getFullname() {
		return fullname;
	}

	public void setFullname(String fullname) {
		this.fullname = fullname;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getDob() {
		return dob;
	}

	public void setDob(String dob) {
		this.dob = dob;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getCellphone() {
		return cellphone;
	}

	public void setCellphone(String cellphone) {
		this.cellphone = cellphone;
	}

	public String getUserImageURL() {
		return userImageURL;
	}

	public void setUserImageURL(String userImageURL) {
		this.userImageURL = userImageURL;
	}

	public String getErrorMessage() {
		return errorMessage;
	}

	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getArea() {
		return area;
	}

	public void setArea(String area) {
		this.area = area;
	}

	public boolean isLoggedIn() {
		return loggedIn;
	}

	public void setLoggedIn(boolean loggedIn) {
		this.loggedIn = loggedIn;
	}

	public static User parseSignInResponse(String xml) {
		String errorMessage = "Error occured while signing in.";
		if (xml != null && xml.length() > 0) {
			DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
			DocumentBuilder db;
			try {
				db = dbf.newDocumentBuilder();
				InputSource is = new InputSource();
				is.setCharacterStream(new StringReader(xml));

				Document doc = db.parse(is);
				doc.getDocumentElement().normalize();

				NodeList resultNodeList = doc.getElementsByTagName("signin_result");
				Element result = (Element) resultNodeList.item(0);

				String status = Util.getStringValueFromElementForKey(result, "signin_status");

				if (status.equalsIgnoreCase("success")) {
					// login successful
					NodeList user = result.getElementsByTagName("user_creds");
					User userObj = new User(user);

					return userObj;
				}
			} catch (Exception e) {
				e.printStackTrace();
				errorMessage = e.getMessage();
				//Log.d("My Error:" errorMessage);
			}
		}
		// There was an error.
		User u = new User();
		u.setErrorMessage(errorMessage);
		return u;
	}

	public static User parseSignUpResponse(String xml) {
		String errorMessage = "Error occured while signing up.";
		if (xml != null && xml.length() > 0) {
			DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
			DocumentBuilder db;
			
			try {
				db = dbf.newDocumentBuilder();
				InputSource is = new InputSource();
				is.setCharacterStream(new StringReader(xml));

				Document doc = db.parse(is);
				doc.getDocumentElement().normalize();

				NodeList resultNodeList = doc.getElementsByTagName("signup_result");
				Element result = (Element) resultNodeList.item(0);

				String status = Util.getStringValueFromElementForKey(result, "signup_status");



				if (status.equalsIgnoreCase("success")) {
					// login successful

					NodeList user = result.getElementsByTagName("user_creds");
					User userObj = new User(user);

					return userObj;
				}else{
					errorMessage = Util.getStringValueFromElementForKey(result, "message");
				}
			} catch (Exception e) {
				e.printStackTrace();
				errorMessage = e.getMessage();
			}
		}
		// There was an error.
		User u = new User();
		u.setErrorMessage(errorMessage);
		return u;
	}

}
