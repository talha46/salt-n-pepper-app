package com.saltnpepper.app.datamodel;

public class Location {

	private String mAddress;
	private String mLatitude;
	private String mLongitude;
	private String mPhoneNumber;
	private String mBranchId;
	
	public Location(String mAddress, String mLatitude, String mLongitude, String mPhoneNumber, String mBranchId) {
		super();
		this.mAddress = mAddress;
		this.mLatitude = mLatitude;
		this.mLongitude = mLongitude;
		this.mPhoneNumber = mPhoneNumber;
		this.mBranchId = mBranchId;
	}
	
	public String getPhoneNumber() {
		return mPhoneNumber;
	}
	public void setPhoneNumber(String phoneNumber) {
		this.mPhoneNumber = phoneNumber;
	}
	public String getAddress() {
		return mAddress;
	}
	public void setAddress(String address) {
		this.mAddress = address;
	}
	public String getLatitude() {
		return mLatitude;
	}
	public void setLatitude(String latitude) {
		this.mLatitude = latitude;
	}
	public String getLongitude() {
		return mLongitude;
	}
	public void setLongitude(String longitude) {
		this.mLongitude = longitude;
	}
	public String getBranchId() {
		return mBranchId;
	}
	public void setBranchId(String branchId) {
		this.mBranchId = branchId;
	}
	
}
