package com.saltnpepper.app.datamodel;

import org.xml.sax.Attributes;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.DefaultHandler;

import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

public class Deals {

	private ArrayList<Eatout> mEatouts = null;
	
	public Deals(String xml) {

		EatoutHandler eatoutHandler = new EatoutHandler();
		try {
			mEatouts = eatoutHandler.parse(xml);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public ArrayList<Eatout> getEatouts() {
		return mEatouts;
	}

	private class EatoutHandler extends DefaultHandler {

		private StringBuffer mBuffer = new StringBuffer();
		private String mEatoutId;
		private String mUserName;
		private String mName;
		private String mEatoutLogo;
		private ArrayList<Deal> mDeals = null;

		private String id;
		private String dealimg;
		private String timing;
		private String dealname;
		private String dealprice;
		private String currency;
		private String dealdetails;
		private String tax;

		public ArrayList<Eatout> parse(String xml) throws IOException, SAXException, ParserConfigurationException {

			SAXParserFactory factory = SAXParserFactory.newInstance();
			SAXParser parser = factory.newSAXParser();
			XMLReader xmlreader = parser.getXMLReader();
			EatoutHandler eatoutHandler = new EatoutHandler();
			xmlreader.setContentHandler(eatoutHandler);
			xmlreader.parse(new InputSource(new StringReader(xml)));

			return mEatouts;
		}

		@Override
		public void startElement(String namespaceURI, String localName, String qName, Attributes atts)
				throws SAXException {

			mBuffer.setLength(0);
			if (qName.equals("Results")) {

				mEatouts = new ArrayList<Eatout>();
			} else if (qName.equals("eatout")) {

				mEatoutId = atts.getValue("eatoutid");
			} else if (qName.equals("name")) {

				mUserName = atts.getValue("username");
			} else if (qName.equals("deals")) {

				mUserName = atts.getValue("id");
			} else if (qName.equals("dealprice")) {

				currency = atts.getValue("currency");
			}
		}

		@Override
		public void endElement(String uri, String localName, String qName) throws SAXException {

			if (qName.equals("name")) {

				mName = mBuffer.toString();
			} else if (qName.equals("eatout_logo")) {

				mEatoutLogo = mBuffer.toString();
			} else if (qName.equals("timing")) {

				timing = mBuffer.toString();
			} else if (qName.equals("dealname")) {

				dealname = mBuffer.toString();
			} else if (qName.equals("dealimg")) {

				dealimg = mBuffer.toString();
			} else if (qName.equals("dealprice")) {

				dealprice = mBuffer.toString();
			} else if (qName.equals("dealdetails")) {

				dealdetails = mBuffer.toString();
			} else if (qName.equals("tax")) {

				tax = mBuffer.toString();
			} else if (qName.equals("deals")) {

				if (mDeals == null) {
					mDeals = new ArrayList<Deal>();
				}
				Deal deal = new Deal(id, dealimg, timing, dealname, dealprice, currency, dealdetails, tax);
				mDeals.add(deal);
			} else if (qName.equals("eatout")) {

				Eatout eatout = new Eatout(mEatoutId, mUserName, mName, mEatoutLogo, mDeals);
				if(mEatouts == null){
					mEatouts = new ArrayList<Eatout>();
				}
				mEatouts.add(eatout);
			}
		}

		@Override
		public void characters(char[] ch, int start, int length) {
			mBuffer.append(ch, start, length);
		}

	}
}
