package com.saltnpepper.app.datamodel;

import com.saltnpepper.app.utils.Constants;

import org.xml.sax.Attributes;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.DefaultHandler;

import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

/**
 * Created by talha on 8/24/16.
 */
public class Photos {
    private ArrayList<Photo> mPhotos = null;
    private ArrayList<Photo> snpImages;

    public Photos(String xml) {

        snpImages = new ArrayList<Photo>();
        PhotoHandler photoHandler = new PhotoHandler();
        try {
            mPhotos = photoHandler.parse(xml);
            Constants.test = Integer.toString(mPhotos.size());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public ArrayList<Photo> getPhotos() {
        return mPhotos;
    }

    private class PhotoHandler extends DefaultHandler {

        private StringBuffer mBuffer = new StringBuffer();

        private ArrayList<Photo> mPhotos = null;
        private ArrayList<String> photos = null;

        private String imageId;
        private String imageName;
        private String mUserId;


        private String like;

        public ArrayList<Photo> parse(String xml) throws IOException, SAXException, ParserConfigurationException {

            SAXParserFactory factory = SAXParserFactory.newInstance();
            SAXParser parser = factory.newSAXParser();
            XMLReader xmlreader = parser.getXMLReader();
            PhotoHandler photoHandler = new PhotoHandler();
            xmlreader.setContentHandler(photoHandler);
            xmlreader.parse(new InputSource(new StringReader(xml)));
            //Constants.test = mPhotos.toString();
            return mPhotos;
        }

        @Override
        public void startElement(String namespaceURI, String localName, String qName, Attributes atts)
                throws SAXException {

            mBuffer.setLength(0);
            if (qName.equals("result")) {

                mPhotos = new ArrayList<Photo>();
                photos = new ArrayList<String>();
            } /*else if (qName.equals("eatout_image")) {

                mUserId = atts.getValue("userid");
            } else if (qName.equals("image_name")) {

                like = atts.getValue("like");
            }*/
        }

        @Override
        public void endElement(String uri, String localName, String qName) throws SAXException {

            if(qName.equals("image_name"))
            {
                imageName = mBuffer.toString();

            }
            else if (qName.equals("image_id")) {

                imageId = mBuffer.toString();
            }
             else if (qName.equals("eatout_image")) {

                Photo photo = new Photo(imageId, imageName, mPhotos);

                if(mPhotos == null){
                   mPhotos = new ArrayList<Photo>();
                }
                mPhotos.add(photo);


                photos.add(photo.getImageName());
               // Constants.test = Integer.toString(mPhotos.size());//mPhotos.get(0).getImageName();
                //Constants.n = mPhotos.size();
                snpImages = mPhotos;
            }


        }

        @Override
        public void characters(char[] ch, int start, int length) {
            mBuffer.append(ch, start, length);
        }

    }

    public ArrayList<Photo> getSnpImages() {
        return snpImages;
    }
}
