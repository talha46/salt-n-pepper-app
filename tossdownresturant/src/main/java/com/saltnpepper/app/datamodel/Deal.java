package com.saltnpepper.app.datamodel;

public class Deal {

	private String id;
	private String dealimg;
	private String timing;
	private String dealname;
	private String dealprice;
	private String currency;
	private String dealdetails;
	private String tax;
	
	public Deal(String id, String dealimg, String timing, String dealname, String dealprice, String currency,
			String dealdetails, String tax) {
		super();
		this.id = id;
		this.dealimg = dealimg;
		this.timing = timing;
		this.dealname = dealname;
		this.dealprice = dealprice;
		this.currency = currency;
		this.dealdetails = dealdetails;
		this.tax = tax;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getDealimg() {
		return dealimg;
	}
	public void setDealimg(String dealimg) {
		this.dealimg = dealimg;
	}
	public String getTiming() {
		return timing;
	}
	public void setTiming(String timing) {
		this.timing = timing;
	}
	public String getDealname() {
		return dealname;
	}
	public void setDealname(String dealname) {
		this.dealname = dealname;
	}
	public String getDealprice() {
		return dealprice;
	}
	public void setDealprice(String dealprice) {
		this.dealprice = dealprice;
	}
	public String getCurrency() {
		return currency;
	}
	public void setCurrency(String currency) {
		this.currency = currency;
	}
	public String getDealdetails() {
		return dealdetails;
	}
	public void setDealdetails(String dealdetails) {
		this.dealdetails = dealdetails;
	}
	public String getTax() {
		return tax;
	}
	public void setTax(String tax) {
		this.tax = tax;
	}
	
	
}
