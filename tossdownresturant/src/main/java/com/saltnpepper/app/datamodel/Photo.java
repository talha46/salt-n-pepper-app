package com.saltnpepper.app.datamodel;

import java.util.ArrayList;

/**
 * Created by talha on 8/24/16.
 */
public class Photo {
    private String imageId;
    private String imageName;
    private ArrayList<Photo> mPhotos;

    public Photo(String imgid, String imgname)
    {
        super();
        imageId = imgid;
        imageName = imgname;
    }

    public Photo(String imageNamesss)
    {
        imageName = imageNamesss;
    }


    public Photo(String imgid, String imgname,ArrayList<Photo> photos)
    {
        super();
        imageId = imgid;
        imageName = imgname;
        mPhotos = photos;
    }

    public String getImageId() {
        return imageId;
    }

    public void setImageId(String imageId) {
        this.imageId = imageId;
    }

    public String getImageName() {
        return imageName;
    }

    public void setImageName(String imageName) {
        this.imageName = imageName;
    }

    public ArrayList<Photo> getmPhotos() {
        return mPhotos;
    }

    public void setmPhotos(ArrayList<Photo> mPhotos) {
        this.mPhotos = mPhotos;
    }
}
