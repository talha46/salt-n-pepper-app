/**
 * Response.java
 * BooleanBites Ltd. (c) 2015
 * @author adilsoomro
 */
package com.saltnpepper.app.datamodel;

import java.io.IOException;
import java.io.StringReader;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import com.saltnpepper.app.utils.Util;

/**
 * @author adilsoomro
 *
 */
public class FeedbackResponse {

	
	private String message;
	private boolean success;
	
	
	
	public String getMessage() {
	    return message;
	}



	public void setMessage(String message) {
	    this.message = message;
	}



	public boolean isSuccess() {
	    return success;
	}



	public void setSuccess(boolean success) {
	    this.success = success;
	}



	public static FeedbackResponse parseResponse(String xml){
	    if(xml !=null && xml.length()>0){
		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		DocumentBuilder db;
		try {
		    db = dbf.newDocumentBuilder();
		    InputSource is = new InputSource();
		    is.setCharacterStream(new StringReader(xml));

		    Document doc = db.parse(is);
		    doc.getDocumentElement().normalize();

		    NodeList resultNodeList = doc.getElementsByTagName("feedback_return");
		    Element result = (Element) resultNodeList.item(0);

		    String status = Util.getStringValueFromElementForKey(result, "feedback_status");

		    if (status.equalsIgnoreCase("success")) {
			//login successful
			NodeList userNode = result.getElementsByTagName("feedback_creds");
			Element user = (Element) userNode.item(0);
			String message = Util.getStringValueFromElementForKey(user, "message");
			FeedbackResponse userObj = new FeedbackResponse();
			userObj.setMessage(message);
			userObj.setSuccess(true);
			return userObj;
		    }
		}catch (ParserConfigurationException e) {
		    e.printStackTrace();
		} catch (SAXException e) {
		    e.printStackTrace();
		} catch (IOException e) {
		    e.printStackTrace();
		}
	    }
	    //There was an error.
	    FeedbackResponse u = new FeedbackResponse();
	    u.setMessage("There was an error in connecting to server.");
	    return u;
	}
	
}
