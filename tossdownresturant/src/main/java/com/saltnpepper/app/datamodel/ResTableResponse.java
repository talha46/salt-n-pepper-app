package com.saltnpepper.app.datamodel;

import java.io.IOException;
import java.io.StringReader;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.xml.sax.Attributes;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.DefaultHandler;

public class ResTableResponse {

	private String webResponse;
	private String message;

	public ResTableResponse() {

	}

	public static ResTableResponse parseResTableResponse(String xml) {

		try {
			return ResTableResponseHandler.parse(xml);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	private static class ResTableResponseHandler extends DefaultHandler {

		private StringBuffer buffer = new StringBuffer();
		private static ResTableResponse res;

		public static ResTableResponse parse(String xml) throws IOException, SAXException, ParserConfigurationException {

			res = null;
			SAXParserFactory factory = SAXParserFactory.newInstance();
			SAXParser parser = factory.newSAXParser();
			XMLReader xmlreader = parser.getXMLReader();

			ResTableResponseHandler resTableResponseHandler = new ResTableResponseHandler();

			xmlreader.setContentHandler(resTableResponseHandler);

			xmlreader.parse(new InputSource(new StringReader(xml)));
			return res;
		}

		@Override
		public void startElement(String namespaceURI, String localName, String qName, Attributes atts)
				throws SAXException {
			buffer.setLength(0);
		}

		@Override
		public void endElement(String uri, String localName, String qName) throws SAXException {

			if (qName.equals("message")) {
				res = new ResTableResponse();
				res.setMessage(buffer.toString());
			}
		}

		@Override
		public void characters(char[] ch, int start, int length) {
			buffer.append(ch, start, length);
		}

	}
}
