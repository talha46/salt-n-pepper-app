package com.saltnpepper.app;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.BaseExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.ExpandableListView.OnGroupClickListener;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.SimpleImageLoadingListener;
import com.saltnpepper.app.datamodel.Location;
import com.saltnpepper.app.datamodel.Locations;
import com.saltnpepper.app.utils.Constants;
import com.saltnpepper.app.utils.Util;
import com.saltnpepper.app.webservices.Webservice;

import java.util.ArrayList;
import java.util.Locale;

public class LocationsActivity extends Activity {

	public static int TYPE_VIEW_LOCATION = 0;
	public static int TYPE_SELECT_MENU = 1;
	public static int TYPE_SELECT_AND_PASSBACK = 3;
	public static String ACTIVITY_TYPE = "screen_type";
	private ExpandableListView mLocationsList;
	private ArrayList<Location> mLocations = null;

	String restID;

	/*
	 * We're now using location activity to branch menus. This screenType tells
	 * the purpose of this activity.
	 */
	private int screenType;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_locations_list_layout);
		Intent intent = getIntent();
		restID = intent.getStringExtra(Constants.RESTAURANT_ID_KEY);
		screenType = intent.getIntExtra(ACTIVITY_TYPE, TYPE_VIEW_LOCATION);

		mLocationsList = (ExpandableListView) findViewById(R.id.locationsList);
		Void params = null;
		new GetLocationsAsyncTask().execute(params);
		mLocationsList.setOnGroupClickListener(new OnGroupClickListener() {
			@Override
			public boolean onGroupClick(ExpandableListView parent, View v, int groupPosition, long id) {
				if (screenType == TYPE_VIEW_LOCATION) {
					if (parent.isGroupExpanded(groupPosition)) {
						parent.collapseGroup(groupPosition);
					} else {
						parent.expandGroup(groupPosition);
					}
				} else {
					Location loc = mLocations.get(groupPosition);
					if (screenType == TYPE_SELECT_MENU) {
						Intent intent = new Intent(LocationsActivity.this, MenuActivity.class);
						intent.putExtra(Constants.BRANCH_ID_KEY, loc.getBranchId());
						startActivity(intent);
					} else if (screenType == TYPE_SELECT_AND_PASSBACK) {
						Intent intent = getIntent();
						intent.putExtra(Constants.BRANCH_ID_KEY, loc.getBranchId());
						intent.putExtra(Constants.BRANCH_LOCATATION_KEY, loc.getAddress()); 
						setResult(RESULT_OK, intent);
						finish();
					}
				}
				return true;
			}
		});
	}

	private class GetLocationsAsyncTask extends AsyncTask<Void, Void, Locations> {

		private ProgressDialog pdialog;
		@Override
		protected void onPreExecute() {

			super.onPreExecute();
			pdialog = new ProgressDialog(LocationsActivity.this);
			pdialog.setTitle("Fetching locations.");
			pdialog.setMessage("Please wait...");
			pdialog.setCanceledOnTouchOutside(false);

			pdialog.setCancelable(false);
			pdialog.show();
		}

		@Override
		protected Locations doInBackground(Void... params) {

			return Webservice.getLocations(LocationsActivity.this,restID);
		}

		@Override
		protected void onPostExecute(Locations result) {

			super.onPostExecute(result);
			mLocations = result.getLocations();
			if (pdialog != null && pdialog.isShowing()) {

				pdialog.dismiss();
				pdialog = null;
			}
			if (result != null && result.getLocations() != null && result.getLocations().size() > 0) {

				MenuDetailsExpandableAdapter adapter = new MenuDetailsExpandableAdapter(LocationsActivity.this);
				mLocationsList.setAdapter(adapter);
			} else{

				Util.showAlert("There was an error in connecting to server.", LocationsActivity.this, false);
			}
		}
	}

	private class ViewHolder {
		TextView phone;
		TextView address;
		ImageView arrowImage;
		public RelativeLayout parentRl;
		public ImageView mapImage;
		public ProgressBar progressbar;
		public ImageView dialPhone;
		public ImageView showMap;
	}

//	private class MenuDetailsAdapter extends BaseAdapter {
//
//		private LayoutInflater inflater = null;
//
//		public MenuDetailsAdapter(Context context) {
//
//			inflater = LayoutInflater.from(context);
//		}
//
//		@Override
//		public int getCount() {
//
//			return mLocations.size();
//		}
//
//		@Override
//		public Object getItem(int position) {
//			return mLocations.get(position);
//		}
//
//		@Override
//		public long getItemId(int position) {
//			return position;
//		}
//
//		@Override
//		public View getView(final int position, View convertView, ViewGroup parent) {
//
//			ViewHolder holder = null;
//
//			if (convertView == null) {
//
//				holder = new ViewHolder();
//
//				convertView = inflater.inflate(R.layout.locations_list_item, null);
//				holder.phone = (TextView) convertView.findViewById(R.id.textPhone);
//				holder.address = (TextView) convertView.findViewById(R.id.textAddress);
//				convertView.setTag(holder);
//			} else {
//
//				holder = (ViewHolder) convertView.getTag();
//			}
//			Location location = mLocations.get(position);
//			if (location.getPhoneNumber() == null || location.getPhoneNumber().length() < 1)
//				holder.phone.setText("NA");
//			else
//				holder.phone.setText(location.getPhoneNumber());
//			holder.address.setText(location.getAddress());
//			// convertView.setOnClickListener(this);
//
//			return convertView;
//		}
//	}

	private class MenuDetailsExpandableAdapter extends BaseExpandableListAdapter {

		private LayoutInflater inflater = null;
		int mapWidth = 300;
		int mapHeight = 150;

		public MenuDetailsExpandableAdapter(Context context) {
			inflater = LayoutInflater.from(context);
			DisplayMetrics metrics = new DisplayMetrics();
			getWindowManager().getDefaultDisplay().getMetrics(metrics);

			mapHeight = metrics.heightPixels;
			mapWidth = metrics.widthPixels;
		}

		@Override
		public Object getChild(int groupPosition, int childPosition) {
			Location loc = mLocations.get(groupPosition);
			return loc;
		}

		@Override
		public long getChildId(int groupPosition, int childPosition) {

			return 0;
		}

		@Override
		public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView,
				ViewGroup parent) {
			ViewHolder holder = null;

			if (convertView == null) {

				holder = new ViewHolder();

				convertView = inflater.inflate(R.layout.locations_child_item, null);
				holder.phone = (TextView) convertView.findViewById(R.id.textPhone);
				holder.mapImage = (ImageView) convertView.findViewById(R.id.mapImage);
				holder.progressbar = (ProgressBar) convertView.findViewById(R.id.progressbar);
				holder.dialPhone = (ImageView) convertView.findViewById(R.id.dialPhone);
				holder.showMap = (ImageView) convertView.findViewById(R.id.showMap);

				convertView.setTag(holder);
			} else {

				holder = (ViewHolder) convertView.getTag();
			}
			final Location loc = (Location) getChild(groupPosition, childPosition);
			holder.phone.setText(loc.getPhoneNumber());
			String mapImageUrl = "http://maps.google.com/maps/api/staticmap?zoom=12&size=" + mapWidth + "x" + mapWidth
					/ 2 + "&sensor=false&markers=" + loc.getLatitude() + "," + loc.getLongitude();
			// "800x300&sensor=false&markers=19.387687,-99.251732";
			ImageLoader imageLoader = ImageLoader.getInstance();
			imageLoader.init(ImageLoaderConfiguration.createDefault(LocationsActivity.this));
			// Util.urlLoadImage(imageLoader, holder.mapImage, mapImageUrl);

			final ImageView mapimg = holder.mapImage;
			final ProgressBar progressbar = holder.progressbar;
			Util.urlLoadImage(imageLoader, mapImageUrl, new SimpleImageLoadingListener() {
				public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {

					progressbar.setVisibility(View.GONE);
					super.onLoadingComplete(imageUri, view, loadedImage);
					mapimg.setImageBitmap(loadedImage);
				}

				@Override
				public void onLoadingStarted(String imageUri, View view) {
					progressbar.setVisibility(View.VISIBLE);
					super.onLoadingStarted(imageUri, view);
				}
			});

			holder.dialPhone.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					String phone = loc.getPhoneNumber();
					Intent intent = new Intent(Intent.ACTION_CALL);
					intent.setData(Uri.parse("tel:" + phone));
					startActivity(intent);
				}
			});
			holder.showMap.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					String uri = String.format(Locale.ENGLISH, "http://maps.google.com/maps?&daddr=%s,%s (%s)",
							loc.getLatitude(), loc.getLongitude(), loc.getAddress());
					Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(uri));
					intent.setClassName("com.google.android.apps.maps", "com.google.android.maps.MapsActivity");
					try {
						startActivity(intent);
					} catch (ActivityNotFoundException ex) {
						try {
							Intent unrestrictedIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(uri));
							startActivity(unrestrictedIntent);
						} catch (ActivityNotFoundException innerEx) {
							Toast.makeText(LocationsActivity.this, "Please install a maps application",
									Toast.LENGTH_LONG).show();
						}
					}
				}
			});
			return convertView;
		}

		@Override
		public int getChildrenCount(int groupPosition) {
			return 1;
		}

		@Override
		public Object getGroup(int groupPosition) {

			return mLocations.get(groupPosition);
		}

		@Override
		public int getGroupCount() {

			return mLocations.size();
		}

		@Override
		public long getGroupId(int groupPosition) {

			return groupPosition;
		}

		@Override
		public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {

			ViewHolder holder = null;

			if (convertView == null) {

				holder = new ViewHolder();

				convertView = inflater.inflate(R.layout.locations_list_group_item, null);
				holder.address = (TextView) convertView.findViewById(R.id.textAddress);
				holder.arrowImage = (ImageView) convertView.findViewById(R.id.arrowImage);
				holder.parentRl = (RelativeLayout) convertView.findViewById(R.id.parentRl);

				convertView.setTag(holder);
			} else {

				holder = (ViewHolder) convertView.getTag();
			}
			final Location location = mLocations.get(groupPosition);
			holder.address.setText(location.getAddress());
			if (isExpanded) {
				holder.arrowImage.setImageResource(R.drawable.down_arrow);
				//
				
				//holder.parentRl.setBackgroundColor(LocationsActivity.this.getResources().getColor(R.color.menu_item_selected_color));
				holder.parentRl.setPressed(true);// setBackgroundColor(R.color.menu_item_selected_color);
				// holder.sep.setBackgroundColor(R.color.theme_color_red);
			} else {
				holder.arrowImage.setImageResource(R.drawable.right_arrow);
				// holder.parentRl.setBackgroundColor(R.drawable.list_item_bg_odd);
				//holder.parentRl.setPressed(false);
				// holder.sep.setBackgroundColor(android.R.color.black);
				holder.parentRl.setBackgroundColor(LocationsActivity.this.getResources().getColor(R.color.menu_item_normal_color));
			}

			return convertView;
		}

		@Override
		public boolean hasStableIds() {

			return false;
		}

		@Override
		public boolean isChildSelectable(int groupPosition, int childPosition) {

			return false;
		}

	}

//	private int selected_position = 0;

//	@Override
//	public void onCreateContextMenu(ContextMenu menu, View v, ContextMenuInfo menuInfo) {
//
//		super.onCreateContextMenu(menu, v, menuInfo);
//		MenuInflater inflater = getMenuInflater();
//		menu.setHeaderTitle(R.string.app_name);
//		inflater.inflate(R.menu.location_options_menu, menu);
//	}

//	@Override
//	public boolean onMenuItemSelected(int featureId, MenuItem item) {
//		int id = item.getItemId();
//		Location loc = mLocations.get(selected_position);
//		if (id == R.id.item_call) {
//
//			String phone = loc.getPhoneNumber();
//			Intent intent = new Intent(Intent.ACTION_CALL);
//			intent.setData(Uri.parse("tel:" + phone));
//			startActivity(intent);
//
//		} else if (id == R.id.item_show_map) {
//
//			String uri = String.format(Locale.ENGLISH, "http://maps.google.com/maps?&daddr=%s,%s (%s)",
//					loc.getLatitude(), loc.getLongitude(), loc.getAddress());
//			Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(uri));
//			intent.setClassName("com.google.android.apps.maps", "com.google.android.maps.MapsActivity");
//			try {
//				startActivity(intent);
//			} catch (ActivityNotFoundException ex) {
//				try {
//					Intent unrestrictedIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(uri));
//					startActivity(unrestrictedIntent);
//				} catch (ActivityNotFoundException innerEx) {
//					Toast.makeText(this, "Please install a maps application", Toast.LENGTH_LONG).show();
//				}
//			}
//		}
//		return true;
//	}

}
