/**
 * SelectLocationsActivity.java
 * BooleanBites Ltd. (c) 2015
 * @author adilsoomro
 */
package com.saltnpepper.app;

import android.app.Activity;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.assist.SimpleImageLoadingListener;
import com.saltnpepper.app.utils.Util;

/**
 * @author adilsoomro
 * 
 */
public class DealDetailActivity extends Activity {
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		this.setContentView(R.layout.activity_deal_detail_layout);
		String dealImageUrl = null;
		Bundle b = getIntent().getExtras();
		if(b != null){
			dealImageUrl = b.getString("dealImageUrl");
		}
		
		ImageView back_arrow = (ImageView) findViewById(R.id.back_arrow);
		back_arrow.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				finish();
			}
		});
		
		final ProgressBar progressbar = (ProgressBar) findViewById(R.id.progressbar);
		final ImageView arrowImage = (ImageView) findViewById(R.id.dealImage);
		ImageLoader imageLoader = ImageLoader.getInstance();
		imageLoader.init(ImageLoaderConfiguration.createDefault(DealDetailActivity.this));
		Util.urlLoadImage(imageLoader, dealImageUrl, new SimpleImageLoadingListener() {
			public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {

				progressbar.setVisibility(View.GONE);
				super.onLoadingComplete(imageUri, view, loadedImage);
				arrowImage.setImageBitmap(loadedImage);
			}

			@Override
			public void onLoadingStarted(String imageUri, View view) {
				progressbar.setVisibility(View.VISIBLE);
				super.onLoadingStarted(imageUri, view);
			}

			@Override
			public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
				super.onLoadingFailed(imageUri, view, failReason);
				progressbar.setVisibility(View.GONE);
			}
		});

	}

	
}
