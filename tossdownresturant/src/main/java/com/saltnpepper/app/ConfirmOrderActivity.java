package com.saltnpepper.app;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.saltnpepper.app.cart.CartItem;
import com.saltnpepper.app.cart.ShoppingCart;
import com.saltnpepper.app.datamodel.User;
import com.saltnpepper.app.utils.Constants;
import com.saltnpepper.app.utils.Util;
import com.saltnpepper.app.webservices.Webservice;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Date;
import java.util.HashMap;
import java.util.Set;

public class ConfirmOrderActivity extends Activity {

	private Button btn_place_order;
	private EditText text_name, text_lanline, text_mobile, text_delivery_address, text_delivery_address_area;
	// private EditText text_city, text_area;
	Spinner /* spiner_area, */spiner_city;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_confirm_order_layout);

		spiner_city = (Spinner) findViewById(R.id.spiner_city);
		String[] times = new String[] { "Lahore", "Multan", "Gujranwala" };
		ArrayAdapter<String> citySpinnerArrayAdapter = new ArrayAdapter<String>(this, R.layout.spiner_item, times);
		spiner_city.setAdapter(citySpinnerArrayAdapter);
		spiner_city.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> arg0, View arg1, int position, long arg3) {
				/*
				 * if (position == 0) {
				 * 
				 * String[] areas = new String[] { "DHA", "Model Town" };
				 * ArrayAdapter<String> areaSpinnerArrayAdapter = new
				 * ArrayAdapter<String>(ConfirmOrderActivity.this,
				 * R.layout.spiner_item, areas);
				 * spiner_area.setAdapter(areaSpinnerArrayAdapter);
				 * spiner_area.setSelection(0); } else if (position == 1) {
				 * 
				 * String[] areas = new String[] { "F10", "F9" };
				 * ArrayAdapter<String> areaSpinnerArrayAdapter = new
				 * ArrayAdapter<String>(ConfirmOrderActivity.this,
				 * R.layout.spiner_item, areas);
				 * spiner_area.setAdapter(areaSpinnerArrayAdapter);
				 * spiner_area.setSelection(0); }
				 */
			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
			}
		});

		// spiner_area = (Spinner) findViewById(R.id.spiner_area);

		text_name = (EditText) findViewById(R.id.text_name);
		text_lanline = (EditText) findViewById(R.id.text_lanline);
		text_mobile = (EditText) findViewById(R.id.text_mobile);
		text_delivery_address = (EditText) findViewById(R.id.text_delivery_address);
		// text_city = (EditText) findViewById(R.id.text_delivery_address_city);
		// text_area = (EditText) findViewById(R.id.text_delivery_address_area);
		btn_place_order = (Button) findViewById(R.id.btn_place_order);
		btn_place_order.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				if (text_name.getText().toString().trim().length() < 1
						|| text_lanline.getText().toString().trim().length() < 1
						|| text_mobile.getText().toString().trim().length() < 1
						|| text_delivery_address.getText().toString().trim().length() < 1) {

					Toast.makeText(ConfirmOrderActivity.this, "Please fill all fields.", Toast.LENGTH_LONG).show();
				} else {
					try {
						InputMethodManager imm = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
						imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
					} catch (Exception e) {
					}
					Void params = null;
					new SubmitOrderAsyncTask().execute(params);
				}
			}
		});

		User user = Util.getLoggedInUser(ConfirmOrderActivity.this);

		text_name.setText(user.getFullname());
		text_lanline.setText(user.getCellphone());
		text_mobile.setText(user.getPhone());
		text_delivery_address.setText(user.getAddress());
		// text_city.setText(user.getCity());
		// text_area.setText(user.getArea());
		text_delivery_address_area = (EditText) findViewById(R.id.text_delivery_address_area);
		text_delivery_address_area.setText(user.getArea());
		if (user.getCity().equalsIgnoreCase(times[1])) {
			spiner_city.setSelection(1);
		} else {
			spiner_city.setSelection(0);
		}
	}

	private class SubmitOrderAsyncTask extends AsyncTask<Void, Void, String> {

		private ProgressDialog pdialog;

		@Override
		protected void onPreExecute() {

			super.onPreExecute();
			pdialog = new ProgressDialog(ConfirmOrderActivity.this);
			pdialog.setTitle("Placing order...");
			pdialog.setMessage("Please wait...");
			pdialog.setCanceledOnTouchOutside(false);
			pdialog.setCancelable(false);
			pdialog.show();
		}

		@Override
		protected String doInBackground(Void... params) {

			String result = null;
			String name = text_name.getText().toString().trim();
			String landline = text_lanline.getText().toString().trim();
			String mobile = text_mobile.getText().toString().trim();
			String address = text_delivery_address.getText().toString().trim();
			String area = text_delivery_address_area.getText().toString().trim();// spiner_area.getSelectedItem().toString();
			String city = spiner_city.getSelectedItem().toString();
			try {
			    ShoppingCart cart = ShoppingCart.getInstance();
				HashMap<String, CartItem> cartItems = cart.getOrderItems();
				if (cartItems != null && cartItems.size() > 0) {

					JSONObject orderObject = new JSONObject();
					JSONArray itemsArray = new JSONArray();
					Set<String> itemNames = cartItems.keySet();
					for (String itemName : itemNames) {

						CartItem item = cartItems.get(itemName);
						if (item != null) {
							try {
								JSONObject jObj = new JSONObject();
								jObj.put("dish_name", item.getItemName());
								jObj.put("dish_qty", item.getItemQuantity());
								jObj.put("dish_price", item.getItemPrice());
								itemsArray.put(jObj);

							} catch (JSONException e) {
								e.printStackTrace();
							}
						}
					}
					orderObject.put("items", itemsArray);
					String items = orderObject.toString();
					
					HashMap<String, String> map = new HashMap<String, String>();
					map.put("order", items);
					map.put("order_date", new Date().toString());
					map.put("name", name);
					map.put("mobile_phone", mobile);
					map.put("landline_phone", landline);
					map.put("address", address+", "+area+", "+city);
					map.put("discount", "" + cart.getDiscount_percent());
					map.put("tax", "" + cart.getGst_percent());
					map.put("city", city);
					map.put("area", area);
					map.put("note", Util.getNameForRestaurantId(cart.getRestaurantId()));
					map.put("rid", Constants.FAKE_EATINGOUT_ID);
					
					return Webservice.submitOder(map, ConfirmOrderActivity.this);
				}

			} catch (Exception e) {
				e.printStackTrace();
				result = e.getMessage();
			}
			return result;
		}

		@Override
		protected void onPostExecute(String result) {

			super.onPostExecute(result);
			if (pdialog != null && pdialog.isShowing()) {

				pdialog.dismiss();
				pdialog = null;
			}
			if (result != null) {
				showAlert(result, ConfirmOrderActivity.this);
			} else {
				// Util.showAlert("No response found.",
				// ConfirmOrderActivity.this);
				Toast.makeText(ConfirmOrderActivity.this, "Error occured, Please try again.", Toast.LENGTH_LONG).show();
			}
		}
	}

	private void showAlert(String message, final Activity context) {

		if (context != null) {

			Util.displayAlert(context, message, new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					Intent menuActivity = new Intent(context, DashboardActivity.class);
					menuActivity.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
					startActivity(menuActivity);
				}
			});
		}
	}
}
