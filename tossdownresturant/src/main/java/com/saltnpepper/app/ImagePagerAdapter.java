package com.saltnpepper.app;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import java.util.ArrayList;

/**
 * Created by talha on 8/25/16.
 */
public class ImagePagerAdapter extends PagerAdapter {

    Context context;
    ArrayList<String> links;
    ArrayList<Bitmap> imgs;// = new ArrayList<Bitmap>();
    ImageView imageView;

    public int[] imageId = {R.drawable.d1, R.drawable.d2, R.drawable.d3, R.drawable.d4, R.drawable.d5, R.drawable.d6,
            R.drawable.d7, R.drawable.d8, R.drawable.d9, R.drawable.d10};

    public ImagePagerAdapter(Context context, ArrayList<Bitmap> img_links) {
        this.context = context;
        this.imgs = img_links;

    }

    public ImagePagerAdapter(Context context) {
        this.context = context;

    }


    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        // TODO Auto-generated method stub

        LayoutInflater inflater = ((Activity) context).getLayoutInflater();

        View viewItem = inflater.inflate(R.layout.image_item_view_pager, container, false);
        imageView = (ImageView) viewItem.findViewById(R.id.imageView_view_pager);
        //imageView.setImageResource(imageId[position]);
        imageView.setImageBitmap(imgs.get(position));


        ((ViewPager) container).addView(viewItem);

        return viewItem;
    }

    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return imageId.length;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        // TODO Auto-generated method stub

        return view == ((View) object);
    }


    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        // TODO Auto-generated method stub
        ((ViewPager) container).removeView((View) object);
    }



}