package com.saltnpepper.app;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.saltnpepper.app.cart.ShoppingCart;
import com.saltnpepper.app.datamodel.User;
import com.saltnpepper.app.utils.Constants;
import com.saltnpepper.app.utils.Util;
import com.saltnpepper.app.webservices.Webservice;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.SimpleImageLoadingListener;

public class ReserveTableActivity extends Activity implements OnClickListener {

	private Spinner spiner_date, spiner_time, spiner_guests;
	private EditText text_request, text_name, text_email, text_phone;
	private Button btn_reserve;
	TextView branchLocationButton;
	ImageView userImageView;
	String branchId = null;

	@Override
	protected void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_res_table_layout);
		initUI();
	}

	private void initUI() {

		// reservation date
		spiner_date = (Spinner) findViewById(R.id.spiner_date);
		String[] dates = new String[17];
		dates[0] = getResources().getString(R.string.reservation_date_hint);
		SimpleDateFormat sdf = new SimpleDateFormat("EEEE, dd MMMM yyyy", Locale.ENGLISH);
		Calendar c = Calendar.getInstance();
		for (int i = 1; i < dates.length; i++) {
			c.add(Calendar.DATE, 1);
			dates[i] = sdf.format(c.getTime());
		}
		ArrayAdapter<String> datesSpinnerArrayAdapter = new ArrayAdapter<String>(this, R.layout.spiner_item, dates);
		spiner_date.setAdapter(datesSpinnerArrayAdapter);
		spiner_date.setSelection(0);

		// reservation time
		spiner_time = (Spinner) findViewById(R.id.spiner_time);
		String[] times = new String[] { "8:00 AM", "7:00 AM", "7:30 AM", "8:00 AM", "8:30 AM", "9:00 AM", "9:30 AM",
				"10:00 AM", "10:30 AM", "11:00 AM", "11:30 AM", "12:00 PM", "12:30 PM", "1:00 PM", "1:30 PM",
				"2:00 PM", "2:30 PM", "3:00 PM", "3:30 PM", "4:00 PM", "4:30 PM", "5:00 PM", "5:30 PM", "6:00 PM",
				"6:30 PM", "7:00 PM", "7:30 PM", "8:00 PM", "8:30 PM", "9:00 PM", "9:30 PM", "10:00 PM", "10:30 PM",
				"11:00 PM", "11:30 PM" };
		ArrayAdapter<String> timeSpinnerArrayAdapter = new ArrayAdapter<String>(this, R.layout.spiner_item, times);
		spiner_time.setAdapter(timeSpinnerArrayAdapter);
		spiner_time.setSelection(0);

		// number of guessts
		spiner_guests = (Spinner) findViewById(R.id.spiner_guests);
		String[] guests = new String[51];
		guests[0] = getResources().getString(R.string.no_of_guests);
		for (int i = 1; i < guests.length; i++) {
			guests[i] = i + 1 + "";
		}
		ArrayAdapter<String> guestsSpinnerArrayAdapter = new ArrayAdapter<String>(this, R.layout.spiner_item, guests);
		spiner_guests.setAdapter(guestsSpinnerArrayAdapter);
		spiner_guests.setSelection(0);

		text_request = (EditText) findViewById(R.id.text_request);
		text_name = (EditText) findViewById(R.id.text_name);
		text_email = (EditText) findViewById(R.id.text_email);
		text_phone = (EditText) findViewById(R.id.text_phone);
		btn_reserve = (Button) findViewById(R.id.res_btn);
		//userImageView = (ImageView) findViewById(R.id.res_user_image);
		branchLocationButton = (TextView) findViewById(R.id.reserve_branch_locations);
		btn_reserve.setOnClickListener(this);

		User user = Util.getLoggedInUser(ReserveTableActivity.this);

		ImageLoader imageLoader = ImageLoader.getInstance();
		imageLoader.init(ImageLoaderConfiguration.createDefault(ReserveTableActivity.this));
		Util.urlLoadImage(imageLoader, user.getUserImageURL(), new SimpleImageLoadingListener() {
			public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
				super.onLoadingComplete(imageUri, view, loadedImage);
				// userImageView.setImageBitmap(Util.getRoundedShape(loadedImage));
				Util.makeMaskImage(ReserveTableActivity.this, userImageView, loadedImage);
			}
		});

		text_name.setText(user.getFullname());
		text_email.setText(user.getEmail());
		text_phone.setText(user.getCellphone());
	}

	public void selectBranchLocation(View v) {
		if (Util.isNetworkAvailable(this)) {
			Intent intent = new Intent(this, LocationsActivity.class);
			Toast.makeText(ReserveTableActivity.this, "this field is ok!", Toast.LENGTH_LONG);
			intent.putExtra(LocationsActivity.ACTIVITY_TYPE, LocationsActivity.TYPE_SELECT_AND_PASSBACK);
			startActivityForResult(intent, LocationsActivity.TYPE_SELECT_AND_PASSBACK);
		} else {
			Util.showAlert("No Internet access.", this, false);
		}
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		if (resultCode == RESULT_OK) {
			if (requestCode == LocationsActivity.TYPE_SELECT_AND_PASSBACK) {
				branchId = data.getStringExtra(Constants.BRANCH_ID_KEY);
				String branchLocation = data.getStringExtra(Constants.BRANCH_LOCATATION_KEY);
				branchLocationButton.setText(branchLocation);
			}
		}

	}

	@Override
	public void onClick(View v) {

		int id = v.getId();
		if (id == R.id.res_btn) {
			if (branchId == null) {
				Toast.makeText(this, "Please select a branch location", Toast.LENGTH_LONG).show();
				return;
			}
			if (spiner_date.getSelectedItemPosition() == 0) {
				Toast.makeText(ReserveTableActivity.this, "Select a date.", Toast.LENGTH_LONG).show();
				return;
			} else if (spiner_guests.getSelectedItemPosition() == 0) {
				Toast.makeText(ReserveTableActivity.this, "Select number of guests.", Toast.LENGTH_LONG).show();
				return;
			} else if (text_name.getText().length() < 1 || text_phone.getText().length() < 1) {
				Toast.makeText(ReserveTableActivity.this, "Full name and Phone is necessary.", Toast.LENGTH_LONG)
						.show();
				return;
			}
			Void params = null;
			new ReserveTableAsyncTask().execute(params);
		}
	}

	private class ReserveTableAsyncTask extends AsyncTask<Void, Void, String> {

		private ProgressDialog pdialog;

		@Override
		protected void onPreExecute() {

			super.onPreExecute();
			pdialog = new ProgressDialog(ReserveTableActivity.this);
			pdialog.setTitle("Table reservation...");
			pdialog.setMessage("Please wait...");
			pdialog.setCanceledOnTouchOutside(false);
			pdialog.setCancelable(false);

			pdialog.setButton(DialogInterface.BUTTON_NEGATIVE, "Cancel", new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int which) {
					cancel(true);
				}
			});

			pdialog.show();
		}

		@Override
		protected String doInBackground(Void... params) {

			HashMap<String, String> map = new HashMap<String, String>();
			map.put("name", text_name.getText().toString());
			map.put("emai", text_email.getText().toString());
			map.put("contact_number", text_phone.getText().toString());
			map.put("res_date", spiner_date.getSelectedItem().toString());
			map.put("res_time", spiner_time.getSelectedItem().toString());
			map.put("guest", spiner_guests.getSelectedItem().toString());
			map.put("request", text_request.getText().toString());
			map.put("bid", branchId);
			map.put("note",branchLocationButton.getText().toString());
			return Webservice.reserveTable(map, ReserveTableActivity.this);
		}

		@Override
		protected void onPostExecute(String result) {

			super.onPostExecute(result);
			if (pdialog != null && pdialog.isShowing()) {

				pdialog.dismiss();
				pdialog = null;
			}
			if (result != null) {
				// Toast.makeText(ReserveTableActivity.this, result,
				// Toast.LENGTH_LONG).show();
				// Util.showAlert(result, ReserveTableActivity.this);
				Util.displayAlert(ReserveTableActivity.this, result, new OnClickListener() {

					@Override
					public void onClick(View arg0) {
						finish();
					}
				});
			}
		}

		@Override
		protected void onCancelled() {

			super.onCancelled();
			if (pdialog != null && pdialog.isShowing()) {

				pdialog.dismiss();
				pdialog = null;
			}
		}
	}

}
