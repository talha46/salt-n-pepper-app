package com.saltnpepper.app.cart;

import java.util.Currency;
import java.util.HashMap;
import java.util.Locale;
import java.util.Set;

import android.app.Activity;
import android.app.Dialog;
import android.graphics.drawable.ColorDrawable;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;

import com.saltnpepper.app.R;
import com.saltnpepper.app.datamodel.DishDetail;

public class ShoppingCart {

    private static HashMap<String, CartItem> mOrderItems = null;
    private static String sCurrency;
    private static String restaurantId;
    private static int gst_percent = 16;
    private static int mTotalCheckoutAmount;
    private static int mGstAmount;
    private static int mGrossTotal;
    private static int discount_percent, mDiscountAmount;

    
	private static ShoppingCart instance = null;

    public static ShoppingCartUpdateListener shoppingCartUpdateListener = null;

	public interface ShoppingCartUpdateListener {
		void onShoppingCartUpdate();
	}

	public static void setOnShoppingCartUpdateListener(ShoppingCartUpdateListener listenr) {
		shoppingCartUpdateListener = listenr;
	}

	public ShoppingCart() {
		sCurrency = "PKR";
		restaurantId = null;
	}

	public static ShoppingCart getInstance() {
		if (instance == null) {
			instance = new ShoppingCart();
		}
		return instance;
	}
	public String getRestaurantId(){
	    return restaurantId;
	}
	public boolean canAddForResaurantId(String restaurant) {
		if (restaurantId == null) {
			return true;
		}
		if (restaurantId.equalsIgnoreCase(restaurant)) {
			return true;
		}
		return false;
	}

	public void addItemForRestaurantId(DishDetail dish, int count, String resId) {
		if (this.canAddForResaurantId(resId)) {
			restaurantId = resId;
			this.addItem(dish, count);
		}
	}

	public void addItem(DishDetail dish, int count) {

		if (mOrderItems == null) {
			mOrderItems = new HashMap<String, CartItem>();
		}

		if (mOrderItems.containsKey(dish.getName())) {
			mOrderItems.get(dish.getName()).updateItemQuantity(count);
		} else {
			CartItem cartItem = new CartItem(dish);
			cartItem.setItemQuantity(count);
			mOrderItems.put(dish.getName(), cartItem);
		}

		if (shoppingCartUpdateListener != null) {
			shoppingCartUpdateListener.onShoppingCartUpdate();
		}
	}

	public int calculateCheckoutAmount(TextView textView) {
		int total = 0;

		if (mOrderItems != null && mOrderItems.size() > 0) {

			Set<String> keys = mOrderItems.keySet();
			if (keys != null && keys.size() > 0) {

				for (String key : keys) {

					CartItem item = mOrderItems.get(key);
					String itemPrice = item.getItemPrice();

					int itemPriceInt = 0;
					try {
						itemPriceInt = Integer.parseInt(itemPrice);
					} catch (Exception ex) {
						itemPriceInt = 0;
					}

					total = total + (itemPriceInt * item.getItemQuantity());
				}
			}
		}
		if (textView != null) {
			String amount = "";
			if (sCurrency.equals("PKR")) {
				amount = "Rs. " + total;
			} else {
			    Currency currency = null;
			    try{
				currency = Currency.getInstance(sCurrency);
			    }catch(Exception ex){
				currency = Currency.getInstance("PKR");
			    }
				amount = currency.getSymbol(Locale.US);
				amount = amount + ". " + total;
			}
			textView.setText(amount);
		}
		return total;
	}

	public int calculateGSTAmount(TextView textView) {
		int total = calculateCheckoutAmount(null);

		if (textView != null) {
			String amount = "";
			if (sCurrency.equals("PKR")) {
				amount = "Rs. " + total;
			} else {
				Currency currency = Currency.getInstance(sCurrency);
				amount = currency.getSymbol(Locale.US);
				amount = amount + ". " + total;
			}
			textView.setText(amount);
		}
		return total;
	}

	public static void dispose() {
		instance = null;
		mOrderItems = null;
		restaurantId = null;
	}

	public void removeSelectedItems() {

		// Iterator<DishDetail> iterator = mOrderItems.iterator();
		// while(iterator.hasNext()){
		//
		// DishDetail dish = iterator.next();
		// if(dish.ismIsSelected()){
		// iterator.remove();
		// }
		// }
	}

	public void updateCheckoutDetails(TextView totalTextView, TextView gstTextView, TextView discountTextView, TextView grossTotalTextView) {

		mTotalCheckoutAmount = calculateCheckoutAmount(totalTextView);

		mGstAmount = (mTotalCheckoutAmount * gst_percent) / 100;

		if (gstTextView != null) {
			String amount = "";
			if (sCurrency.equals("PKR")) {
				amount = "Rs. " + mGstAmount;
			} else {
				Currency currency = Currency.getInstance(sCurrency);
				amount = currency.getSymbol(Locale.US);
				amount = amount + ". " + mGstAmount;
			}
			gstTextView.setText(amount);
		}

		mDiscountAmount = (mTotalCheckoutAmount * discount_percent) / 100;
		if(discountTextView != null){
			String amount = "";
			if (sCurrency.equals("PKR")) {
				amount = "Rs. " + mDiscountAmount;
			} else {
				Currency currency = Currency.getInstance(sCurrency);
				amount = currency.getSymbol(Locale.US);
				amount = amount + ". " + mDiscountAmount;
			}
			discountTextView.setText(amount);
		}
		mGrossTotal = mTotalCheckoutAmount + mGstAmount - mDiscountAmount;

		if (grossTotalTextView != null) {
			String amount = "";
			if (sCurrency.equals("PKR")) {
				amount = "Rs. " + mGrossTotal;
			} else {
				Currency currency = Currency.getInstance(sCurrency);
				amount = currency.getSymbol(Locale.US);
				amount = amount + ". " + mGrossTotal;
			}
			grossTotalTextView.setText(amount);
		}
		
	}

	public static void displayDiscardCartAlertAndAddNew(final Activity context, final OnClickListener discardListener) {

		final Dialog dialog = new Dialog(context);
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		WindowManager.LayoutParams wmlp = dialog.getWindow().getAttributes();

		wmlp.gravity = Gravity.CENTER;
		
		dialog.setContentView(R.layout.dialog_ok_alert_layout);
		dialog.getWindow().setBackgroundDrawable(new ColorDrawable(0));

		//dialog.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
		dialog.setCanceledOnTouchOutside(false);

		TextView title = (TextView) dialog.findViewById(R.id.titleText);
		title.setText(R.string.app_name);

		TextView messageText = (TextView) dialog.findViewById(R.id.messageText);
		messageText.setText("You've added items from a different location menu, Do you want to discard previous items?");

		Button ok_button = (Button) dialog.findViewById(R.id.ok_button);
		ok_button.setText("Keep previous");
		ok_button.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				dialog.dismiss();
			}

		});

		Button cancel_button = (Button) dialog.findViewById(R.id.dialog_cancel_button);
		cancel_button.setText("Discard Previous");
		cancel_button.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				dialog.dismiss();
				discardListener.onClick(v);
				
			}

		});
		dialog.show();
	}

	// public void updateCheckoutAmountView(TextView textView) {
	//
	// if (textView != null) {
	// String amount = "";
	// if (sCurrency.equals("PKR")) {
	// amount = "Rs. " + sCheckoutAmount;
	// } else {
	// Currency currency = Currency.getInstance(sCurrency);
	// amount = currency.getSymbol(Locale.US);
	// amount = amount + ". " + sCheckoutAmount;
	// }
	// textView.setText(amount);
	// }
	// }

	public String getCurrency() {
		return sCurrency;
	}

	public void setCurrency(String currency) {
		sCurrency = currency;
	}

	public HashMap<String, CartItem> getOrderItems() {
		return mOrderItems;
	}

	public static int getTotalCheckoutAmount() {
		return mTotalCheckoutAmount;
	}

	public static int getGstAmount() {
		return mGstAmount;
	}

	public int getGrossTotal() {
		return mGrossTotal;
	}
	public int getGst_percent() {
		return gst_percent;
	}

	public void setGst_percent(int gst_percent) {
		ShoppingCart.gst_percent = gst_percent;
	}

	public int getDiscount_percent() {
		return discount_percent;
	}

	public void setDiscount_percent(int discount_percent) {
		ShoppingCart.discount_percent = discount_percent;
	}

}
