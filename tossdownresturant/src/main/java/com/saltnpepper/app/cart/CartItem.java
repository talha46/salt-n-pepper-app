package com.saltnpepper.app.cart;

import com.saltnpepper.app.datamodel.DishDetail;

public class CartItem {

	private String mItemName;
	private int mItemQuantity;
	private String mItemPrice;
	
	public CartItem(DishDetail dish){
		
		this.mItemName = dish.getName();
		this.mItemQuantity = 0;
		this.mItemPrice = dish.getPrice();
	}
	public String getItemName() {
		return mItemName;
	}
	public void setItemName(String mDishName) {
		this.mItemName = mDishName;
	}
	public int getItemQuantity() {
		return mItemQuantity;
	}
	public int updateItemQuantity(int count) {
		return mItemQuantity = mItemQuantity + count;
	}
	public void setItemQuantity(int mDishQuantity) {
		this.mItemQuantity = mDishQuantity;
	}
	public String getItemPrice() {
		return mItemPrice;
	}
	public void setItemPrice(String mDishPrice) {
		this.mItemPrice = mDishPrice;
	}
}
