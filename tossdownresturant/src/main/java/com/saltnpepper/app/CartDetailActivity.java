package com.saltnpepper.app;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.Currency;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.saltnpepper.app.cart.CartItem;
import com.saltnpepper.app.cart.ShoppingCart;
import com.saltnpepper.app.cart.ShoppingCart.ShoppingCartUpdateListener;
import com.saltnpepper.app.datamodel.DishDetail;
import com.saltnpepper.app.datamodel.MenuDetails;
import com.saltnpepper.app.utils.Util;

public class CartDetailActivity extends Activity {

	private ListView shoppingCartList;
	private Map<String, CartItem> mCartItems = null;
	private TextView mTextCheckoutAmount;
	private CartDetailsAdapter mAdapter;
	private TextView mTextViewTotal, mTextViewGstValue, mTextViewGrandTotal;
	private TextView mTextViewGstLable;
	private TextView mTextViewDiscountLable, mTextViewDiscountValue;
	
	@SuppressWarnings("unchecked")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);

		HashMap<String, CartItem> cartItems = ShoppingCart.getInstance().getOrderItems();
		mCartItems = new TreeMap<String, CartItem>(cartItems);

		setContentView(R.layout.activity_shopping_cart_detail_layout);

		shoppingCartList = (ListView) findViewById(R.id.shoppingCartList);
		if (mCartItems != null && mCartItems.size() > 0) {
			mAdapter = new CartDetailsAdapter(this);
			shoppingCartList.setAdapter(mAdapter);
		}
		mTextCheckoutAmount = (TextView) findViewById(R.id.checkout_amount);

		ImageView back_arrow = (ImageView) findViewById(R.id.back_arrow);
		back_arrow.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				finish();
			}
		});
		// final TextView textEdit = (TextView) findViewById(R.id.textEdit);
		// textEdit.setOnClickListener(new OnClickListener() {
		//
		// @Override
		// public void onClick(View v) {
		// if(textEdit.getText().equals(getResources().getString(R.string.edit))){
		//
		// isEditMode = true;
		// textEdit.setText(R.string.done);
		// mAdapter.notifyDataSetChanged();
		// }else{
		// removeItemsFromCart();
		// textEdit.setText(R.string.done);
		// }
		// }
		// });
		mTextViewTotal = (TextView) findViewById(R.id.total_value);
		
		mTextViewGstLable = (TextView) findViewById(R.id.gst);
		mTextViewGstValue = (TextView) findViewById(R.id.gst_value);
		mTextViewGstLable.setText("GST " + MenuDetails.getTax() + " %");
		if(MenuDetails.getTax()==0){
			RelativeLayout rlGST = (RelativeLayout) findViewById(R.id.rlGST);
			rlGST.setVisibility(View.GONE);
			mTextViewGstLable.setVisibility(View.GONE);
			mTextViewGstValue.setVisibility(View.GONE);
		}
		mTextViewDiscountLable = (TextView) findViewById(R.id.discount);
		mTextViewDiscountValue = (TextView) findViewById(R.id.discount_value);
		mTextViewDiscountLable.setText("Discount " + MenuDetails.getDiscount() + " %");
		if(MenuDetails.getDiscount()==0){
			RelativeLayout rlDiscount = (RelativeLayout) findViewById(R.id.rlDiscount);
			rlDiscount.setVisibility(View.GONE);
			mTextViewDiscountLable.setVisibility(View.GONE);
			mTextViewDiscountValue.setVisibility(View.GONE);
		}
		mTextViewGrandTotal = (TextView) findViewById(R.id.grand_total_value);
		Button button_submit = (Button) findViewById(R.id.button_submit);
		button_submit.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				
				if(ShoppingCart.getInstance().getGrossTotal()<MenuDetails.getMinSpend()){
					//Util.showMessage("We accept order with minimum cost Rs: " + MenuDetails.getMinSpend() +", please add some more items into your basket.", CartDetailActivity.this);
					Util.showMessage(getString(R.string.order_less_than_minspend, MenuDetails.getMinSpend()), CartDetailActivity.this);
				}else{
					startActivity(new Intent(CartDetailActivity.this, ConfirmOrderActivity.class));
				}
			}
		});
	}

//	private void removeItemsFromCart() {
//
//		ShoppingCart.getInstance().removeSelectedItems();
//		ShoppingCart.getInstance().updateCheckoutDetails(mTextViewTotal, mTextViewGstValue, mTextViewGrandTotal);
//		mAdapter.notifyDataSetChanged();
//	}

	@Override
	protected void onResume() {

		super.onResume();
		ShoppingCart.setOnShoppingCartUpdateListener(new ShoppingCartUpdateListener() {

			@Override
			public void onShoppingCartUpdate() {

				ShoppingCart.getInstance().calculateCheckoutAmount(mTextCheckoutAmount);
			}
		});
		ShoppingCart.getInstance().updateCheckoutDetails(mTextViewTotal, mTextViewGstValue, mTextViewDiscountValue, mTextViewGrandTotal);
	}

	@Override
	protected void onPause() {

		super.onPause();
		ShoppingCart.setOnShoppingCartUpdateListener(null);
	}

	public class DishComparator implements Comparator<DishDetail> {
		@Override
		public int compare(DishDetail o1, DishDetail o2) {
			return o1.getName().compareTo(o2.getName());
		}
	}

	private class ViewHolder {
		LinearLayout rl_bg;
		TextView name;
		TextView price;
		TextView quantity;
		// CheckBox check;
	}

	// private boolean isEditMode = false;

	private class CartDetailsAdapter extends BaseAdapter {

		private LayoutInflater inflater = null;
		private List<String> keyList = null;
		private String currency = null;

		public CartDetailsAdapter(Context context) {

			inflater = LayoutInflater.from(context);
			currency = ShoppingCart.getInstance().getCurrency();
			Set<String> keys = mCartItems.keySet();
			keyList = new ArrayList<String>(keys);
		}

		@Override
		public int getCount() {

			return keyList.size();
		}

		@Override
		public Object getItem(int position) {
			return keyList.get(position);
		}

		@Override
		public long getItemId(int position) {
			return position;
		}

		@Override
		public View getView(final int position, View convertView, ViewGroup parent) {

			ViewHolder holder = null;

			if (convertView == null) {

				holder = new ViewHolder();

				convertView = inflater.inflate(R.layout.shopping_cart_list_item, null);
				holder.rl_bg = (LinearLayout) convertView.findViewById(R.id.rl_bg);
				holder.name = (TextView) convertView.findViewById(R.id.name);
				holder.price = (TextView) convertView.findViewById(R.id.price);
				holder.quantity = (TextView) convertView.findViewById(R.id.text_qty);
				// holder.check = (CheckBox)
				// convertView.findViewById(R.id.check);
				convertView.setTag(holder);
			} else {

				holder = (ViewHolder) convertView.getTag();
			}
			if ((position % 2) == 0) {
				holder.rl_bg.setBackgroundResource(R.drawable.list_item_bg_even);
			}
			String dishName = keyList.get(position);
			final CartItem cartItem = mCartItems.get(dishName);

			holder.name.setText(cartItem.getItemName());
			holder.quantity.setText(Integer.toString(cartItem.getItemQuantity()));

			String price = "";
			if (currency.equals("PKR")) {
				price = "Rs. " + cartItem.getItemPrice();
			} else {
				Currency curr = Currency.getInstance(currency);
				price = curr.getSymbol(Locale.US);
				price = price + ". " + cartItem.getItemPrice();
			}
			holder.price.setText(price);
			convertView.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					showAddToCardDialog(cartItem);
				}
			});
			// if(isEditMode){
			// holder.check.setVisibility(View.VISIBLE);
			// }else{
			// holder.check.setVisibility(View.GONE);
			// }
			// holder.check.setOnCheckedChangeListener(new
			// OnCheckedChangeListener() {
			//
			// @Override
			// public void onCheckedChanged(CompoundButton buttonView, boolean
			// isChecked) {
			// //mShoppingCartDishes.get(position).setmIsSelected(isChecked);
			// }
			// });
			return convertView;
		}
	}

	private int itemQuantity = 1;

	void showAddToCardDialog(final CartItem cartItem) {

		itemQuantity = cartItem.getItemQuantity();
		final Dialog dialog = new Dialog(CartDetailActivity.this);
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		WindowManager.LayoutParams wmlp = dialog.getWindow().getAttributes();

		wmlp.gravity = Gravity.TOP | Gravity.CENTER_HORIZONTAL;

		wmlp.y = 120;
		dialog.setContentView(R.layout.dialog_edit_cart_layout);
		dialog.getWindow().setBackgroundDrawable(new ColorDrawable(0));

		dialog.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
		dialog.setCanceledOnTouchOutside(false);

		TextView title = (TextView) dialog.findViewById(R.id.txt_title);
		title.setText(cartItem.getItemName());

		TextView text_price = (TextView) dialog.findViewById(R.id.text_price);
		showPrice(cartItem, text_price);

		final TextView text_total = (TextView) dialog.findViewById(R.id.text_total_price);
		showTotalPrice(cartItem, text_total);

		final TextView text_quantity = (TextView) dialog.findViewById(R.id.text_quantity);
		text_quantity.setText(itemQuantity + "");

		View plusButton =  dialog.findViewById(R.id.buttonPlus);
		plusButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				if (itemQuantity <= 1000) {
					itemQuantity++;
					text_quantity.setText(itemQuantity + "");
					showTotalPrice(cartItem, text_total);
				}
			}
		});
		View plusMinus = dialog.findViewById(R.id.buttonMinus);
		plusMinus.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				if (itemQuantity > 1) {
					itemQuantity--;
					text_quantity.setText(itemQuantity + "");
					showTotalPrice(cartItem, text_total);
				}
			}
		});
		Button closeButton = (Button) dialog.findViewById(R.id.image_button_close);
		closeButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				dialog.dismiss();
			}
		});

		Button doneButton = (Button) dialog.findViewById(R.id.button_done);
		doneButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {

				String key = cartItem.getItemName();
				ShoppingCart.getInstance().getOrderItems().get(key).setItemQuantity(itemQuantity);
				ShoppingCart.getInstance().updateCheckoutDetails(mTextViewTotal, mTextViewGstValue, mTextViewDiscountValue, mTextViewGrandTotal);
				mCartItems = ShoppingCart.getInstance().getOrderItems();
				updateListAdapter();

				dialog.dismiss();
			}
		});

		Button button_remove = (Button) dialog.findViewById(R.id.button_remove);
		button_remove.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				String key = cartItem.getItemName();
				ShoppingCart.getInstance().getOrderItems().remove(key);
				ShoppingCart.getInstance().updateCheckoutDetails(mTextViewTotal, mTextViewGstValue, mTextViewDiscountValue, mTextViewGrandTotal);
				mCartItems = ShoppingCart.getInstance().getOrderItems();
				updateListAdapter();

				dialog.dismiss();
			}
		});
		dialog.show();
	}

	private void updateListAdapter(){
		if (mCartItems != null && mCartItems.size() > 0) {
			mAdapter = new CartDetailsAdapter(this);
			shoppingCartList.setAdapter(mAdapter);
		}else{
			Toast.makeText(this, "Cart is empty. Please select ", Toast.LENGTH_SHORT).show();
			finish();
		}
	}
	private void showPrice(CartItem cartItem, TextView text_price) {
		String price = "";
		final String currency = ShoppingCart.getInstance().getCurrency();
		if (currency.equals("PKR")) {
			price = "Rs. " + cartItem.getItemPrice();
		} else {
			Currency curr = Currency.getInstance(currency);
			price = curr.getSymbol(Locale.US);
			price = price + ". " + cartItem.getItemPrice();
		}
		text_price.setText(price);
	}

	private void showTotalPrice(CartItem cartItem, TextView text_total) {
		String toal_price = "";
		final String currency = ShoppingCart.getInstance().getCurrency();
		if (currency.equals("PKR")) {
			toal_price = "Rs. " + (Integer.parseInt(cartItem.getItemPrice()) * itemQuantity);
		} else {
			Currency curr = Currency.getInstance(currency);
			toal_price = curr.getSymbol(Locale.US);
			toal_price = toal_price + ". " + (Integer.parseInt(cartItem.getItemPrice()) * itemQuantity);
		}
		text_total.setText(toal_price);
	}

}
