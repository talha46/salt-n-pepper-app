package com.saltnpepper.app;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.assist.SimpleImageLoadingListener;
import com.saltnpepper.app.datamodel.Photo;
import com.saltnpepper.app.datamodel.Photos;
import com.saltnpepper.app.utils.MutableString;
import com.saltnpepper.app.utils.Util;
import com.saltnpepper.app.webservices.Webservice;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by talha on 8/19/16.
 */
public class PhotoGallery extends Activity {

    private ArrayList<Photo> mPhotos = null;
    private GridView gridview;
    //TextView test;
    public Drawable[] mThumbIds;// = {};
    ArrayList<Bitmap> imgLinks;

        @Override
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            this.setContentView(R.layout.photo_gallery);

            imgLinks = new ArrayList<Bitmap>();
            gridview = (GridView) findViewById(R.id.gridview);
           // test = (TextView)findViewById(R.id.textTest);
            //gridview.setAdapter(new ImageAdapter(this));
            HashMap<String, String> param = new HashMap<String, String>();
            new GetPhotosAsyncTask().execute(param);

            /*int iDisplayWidth = getResources().getDisplayMetrics().widthPixels ;

            int iImageWidth = iDisplayWidth/3;
            gridview.setColumnWidth( iImageWidth );
            gridview.setStretchMode( GridView.NO_STRETCH ) ;*/

            gridview.setVerticalScrollBarEnabled(false);

            gridview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                public void onItemClick(AdapterView<?> parent, View v, int position, long id){
                    // Send intent to SingleViewActivity
                    Intent i = new Intent(getApplicationContext(), PhotoViewActivity.class);

                    // Pass image index
                    i.putExtra("id", position);
                    startActivity(i);
                }
            });


        }






    private class GetPhotosAsyncTask extends AsyncTask<HashMap<String, String>, Void, Photos> {

        private ProgressDialog pdialog;
        private MutableString errorMessage = new MutableString("");

        @Override
        protected void onPreExecute() {

            super.onPreExecute();
            pdialog = new ProgressDialog(PhotoGallery.this);
            pdialog.setTitle("Fetching Images.");
            pdialog.setMessage("Please wait...");
            pdialog.setCanceledOnTouchOutside(false);
            pdialog.setCancelable(false);
            pdialog.show();
        }

        @Override
        protected Photos doInBackground(HashMap<String, String>... params) {

            return Webservice.getPhotos(params[0], PhotoGallery.this);
        }

        @Override
        protected void onPostExecute(Photos result) {


            super.onPostExecute(result);
             mPhotos = result.getSnpImages();
            //ArrayList<Photo> snp = result.getSnpImages();
            //Toast.makeText(PhotoGallery.this, Constants.test,Toast.LENGTH_LONG).show();

            if (pdialog != null && pdialog.isShowing()) {

                pdialog.dismiss();
                pdialog = null;
            }
            if (mPhotos != null && mPhotos.size() > 0) {



                /*for(int i = 0; i < mPhotos.size(); i++)
                {
                    //Drawable d = LoadImage(mPhotos.get(i).getImageName());
                    //Bitmap anImage = ((BitmapDrawable) d).getBitmap();

                    //Toast.makeText(PhotoGallery.this, mPhotos.get(i).getImageName(),Toast.LENGTH_LONG).show();
                    //mThumbIds[i] = d;

                    //new LoadImage().execute(mPhotos.get(i).getImageName());


                }*/

                PhotoDetailsAdapter adapter = new PhotoDetailsAdapter(PhotoGallery.this);
                gridview.setAdapter(adapter);
                //test.setText(result.getSnpImages().get(0).getImageName());

            } else if (errorMessage.getString().length() > 0) {

                Util.showAlert(errorMessage.getString(), PhotoGallery.this, false);
            }
        }


    }


    private class ViewHolder {
        ImageView dealImage, arrowImage;
        public ProgressBar progressbar;
        //public RelativeLayout leftRl;
    }


    private class PhotoDetailsAdapter extends BaseAdapter {

        private LayoutInflater inflater = null;

        public PhotoDetailsAdapter(Context context) {

            inflater = LayoutInflater.from(context);
        }

        @Override
        public int getCount() {
            return mPhotos.size();
        }

        @Override
        public Object getItem(int position) {
            return mPhotos.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {

            ViewHolder holder = null;

            if (convertView == null) {

                holder = new ViewHolder();

                convertView = inflater.inflate(R.layout.image_item, null);
                // holder.textName = (TextView)
                // convertView.findViewById(R.id.textName);
                //holder.textDealName = (TextView) convertView.findViewById(R.id.textDealName);
                //holder.textCurrency = (TextView) convertView.findViewById(R.id.textCurrency);
               // holder.textPrice = (TextView) convertView.findViewById(R.id.textPrice);
                // holder.textTax = (TextView)
                // convertView.findViewById(R.id.textTax);
               // holder.textDealDetails = (TextView) convertView.findViewById(R.id.textDealDetails);
                holder.dealImage = (ImageView) convertView.findViewById(R.id.imageView);
                holder.progressbar = (ProgressBar) convertView.findViewById(R.id.progressbarimg);
                holder.arrowImage = (ImageView) convertView.findViewById(R.id.arrowImage);
                //holder.leftRl = (RelativeLayout) convertView.findViewById(R.id.leftRl);
                convertView.setTag(holder);
            } else {

                holder = (ViewHolder) convertView.getTag();
            }
            //final Photo photo = mPhotos.get(position);
            // holder.textName.setText(eatout.getName());
           // final Deal eatoutDeal = eatout.getDeals().get(position);
           // holder.textDealName.setText(eatoutDeal.getDealname());
            //holder.textCurrency.setText(eatoutDeal.getCurrency() + " + \nTax");
           // holder.textPrice.setText(eatoutDeal.getDealprice());
            // holder.textTax.setText(eatoutDeal.getTax());
            //holder.textDealDetails.setText(eatoutDeal.getDealdetails());

            ImageLoader imageLoader = ImageLoader.getInstance();
            imageLoader.init(ImageLoaderConfiguration.createDefault(PhotoGallery.this));
            //Util.urlLoadImage(imageLoader, holder.dealImage, eatout.getEatoutLogo());
            final ImageView dImage = holder.dealImage;

            final ProgressBar progressbar = holder.progressbar;
            final String dealImageUrl = mPhotos.get(position).getImageName();


            Util.urlLoadImage(imageLoader, dealImageUrl, new SimpleImageLoadingListener() {
                public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {

                    progressbar.setVisibility(View.GONE);
                    super.onLoadingComplete(imageUri, view, loadedImage);
                    dImage.setImageBitmap(loadedImage);
                    imgLinks.add(loadedImage);
                }

                @Override
                public void onLoadingStarted(String imageUri, View view) {
                    progressbar.setVisibility(View.VISIBLE);
                    super.onLoadingStarted(imageUri, view);
                }

                @Override
                public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
                    super.onLoadingFailed(imageUri, view, failReason);
                    progressbar.setVisibility(View.GONE);
                }
            });

            holder.dealImage.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    // start deaildetail activity
                    Intent i=new Intent(PhotoGallery.this, PhotoViewActivity.class);
                    //i.putExtra("position", position);
                    i.putExtra("imgArray",imgLinks);

                    startActivity(i);
                }
            });
            /*holder.leftRl.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    // show deaildetail dialog
                    showDealDialog(eatoutDeal, eatout);
                }
            });*/
            return convertView;
        }
    }


    /*private class LoadImage extends AsyncTask<String, String, Bitmap> {

        Dialog dialog;
        Bitmap bitmap;



        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            //dialog = new ProgressDialog(context);
            // dialog.setTitle("Loading Image...");
            //dialog.show();

        }

        @Override
        protected Bitmap doInBackground(String... params) {

            try {
                bitmap = BitmapFactory.decodeStream((InputStream) new URL(params[0]).getContent());
            } catch (IOException e) {
                e.printStackTrace();
            }
            return bitmap;
        }

        @Override
        protected void onPostExecute(Bitmap result) {
            if (result != null) {
                //img.setImageBitmap(result);
                //bt = result;
                imgLinks.add(result);
                //dialog.dismiss();
            } else {
                dialog.dismiss();
                Toast.makeText(PhotoGallery.this, "Image Does Not Exist...", Toast.LENGTH_LONG).show();
            }

        }

    }*/

}
