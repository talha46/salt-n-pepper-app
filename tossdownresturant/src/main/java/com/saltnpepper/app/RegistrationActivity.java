/**
 * RegistrationActivity.java
 * BooleanBites Ltd. (c) 2015
 * @author adilsoomro
 */
package com.saltnpepper.app;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.HashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.provider.MediaStore.MediaColumns;
import android.support.v4.content.IntentCompat;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.saltnpepper.app.datamodel.User;
import com.saltnpepper.app.utils.Util;
import com.saltnpepper.app.webservices.Webservice;
import com.facebook.Session;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.SimpleImageLoadingListener;

/**
 * @author adilsoomro
 *
 */
public class RegistrationActivity extends Activity {
	static final int REQ_IMAGE_FROM_GALERY = 0300;
	static final int REQ_IMAGE_FROM_CAMERA = 0313;
	static final int DATE_DIALOG_ID = 7585856;

	public static final int TYPE_PROFILE = 1;
	public static final int TYPE_SIGN_UP = 0;
	public static String ACTIVITY_TYPE = "screen_type";

	private EditText usernameField;
	private EditText phoneField;
	private EditText emailField;
	private EditText passwordField;
	private EditText landlineField;
	private EditText addressField;
	private EditText cityField;
	private EditText areaField;

	private TextView userDobButton;

	private ImageView userImageView;

	private ImageView gelleryImageView;
	private ImageView cameraImageView;

	private View logoutImageView;

	//private Bitmap originalBitmap;
	String imagePath = null;
	private int mYear;
	private int mMonth;
	private int mDay;
	private int screenType = 0;
	private boolean isUsingFBLogin = false;

	private ProgressDialog dialog = null;
    static Context c;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.setContentView(R.layout.activity_registration_layout);
		screenType = getIntent().getIntExtra(ACTIVITY_TYPE, TYPE_SIGN_UP);
		usernameField = (EditText) findViewById(R.id.reg_text_name);
		emailField = (EditText) findViewById(R.id.reg_text_email);
		passwordField = (EditText) findViewById(R.id.reg_text_password);
		View passwordLayout = findViewById(R.id.reg_password_layout);
		phoneField = (EditText) findViewById(R.id.reg_text_phone);
		landlineField = (EditText) findViewById(R.id.reg_text_landline);
		addressField = (EditText) findViewById(R.id.reg_text_address);
		cityField = (EditText) findViewById(R.id.reg_text_address_city);
		//areaField = (EditText) findViewById(R.id.reg_text_address_area);
		userDobButton = (TextView) findViewById(R.id.reg_user_dob);
		Button signUpButton = (Button) findViewById(R.id.reg_sign_up);

        c =this;


		/*userImageView = (ImageView) findViewById(R.id.reg_user_image);
		gelleryImageView = (ImageView) findViewById(R.id.reg_select_from_gallery);
		cameraImageView = (ImageView) findViewById(R.id.reg_select_from_camera);*/
		//logoutImageView =  findViewById(R.id.reg_logout);


		usernameField.setOnFocusChangeListener(new View.OnFocusChangeListener(){
			public void onFocusChange(View v, boolean hasFocus){
				if(hasFocus)
					usernameField.setHint("");
				else
					usernameField.setHint("Full Name");
			}
		});

		emailField.setOnFocusChangeListener(new View.OnFocusChangeListener(){
			public void onFocusChange(View v, boolean hasFocus){
				if(hasFocus)
					emailField.setHint("");
				else
					emailField.setHint("Email");
			}
		});

		passwordField.setOnFocusChangeListener(new View.OnFocusChangeListener(){
			public void onFocusChange(View v, boolean hasFocus){
				if(hasFocus)
					passwordField.setHint("");
				else
					passwordField.setHint("Password");
			}
		});

		phoneField.setOnFocusChangeListener(new View.OnFocusChangeListener(){
			public void onFocusChange(View v, boolean hasFocus){
				if(hasFocus)
					phoneField.setHint("");
				else
					phoneField.setHint("Mobile Number");
			}
		});

		landlineField.setOnFocusChangeListener(new View.OnFocusChangeListener(){
			public void onFocusChange(View v, boolean hasFocus){
				if(hasFocus)
					landlineField.setHint("");
				else
					landlineField.setHint("Phone Number");
			}
		});

		addressField.setOnFocusChangeListener(new View.OnFocusChangeListener(){
			public void onFocusChange(View v, boolean hasFocus){
				if(hasFocus)
					addressField.setHint("");
				else
					addressField.setHint("Address");
			}
		});

		cityField.setOnFocusChangeListener(new View.OnFocusChangeListener(){
			public void onFocusChange(View v, boolean hasFocus){
				if(hasFocus)
					cityField.setHint("");
				else
					cityField.setHint("City");
			}
		});

		mYear = 1990;
		mMonth = -1;
		mDay = 0;

		if (screenType == TYPE_PROFILE) {
			usernameField.setEnabled(false);
			emailField.setEnabled(false);
			passwordField.setEnabled(false);
			phoneField.setEnabled(false);
			landlineField.setEnabled(false);
			addressField.setEnabled(false);
			cityField.setEnabled(false);
			//areaField.setEnabled(false);
			userDobButton.setEnabled(false);
			//userImageView.setEnabled(false);

			User user = Util.getLoggedInUser(RegistrationActivity.this);

			ImageLoader imageLoader = ImageLoader.getInstance();
			imageLoader.init(ImageLoaderConfiguration.createDefault(RegistrationActivity.this));
			Util.urlLoadImage(imageLoader, user.getUserImageURL(), new SimpleImageLoadingListener() {
				public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
					super.onLoadingComplete(imageUri, view, loadedImage);
					//userImageView.setImageBitmap(Util.getRoundedShape(loadedImage));
					Util.makeMaskImage(RegistrationActivity.this, userImageView, loadedImage);
				}
			});
			usernameField.setText(user.getFullname());
			emailField.setText(user.getEmail());
			// passwordField.setText(user.);
			phoneField.setText(user.getCellphone());
			landlineField.setText(user.getPhone());
			addressField.setText(user.getAddress());
			cityField.setText(user.getCity());
			//areaField.setText(user.getArea());
			userDobButton.setText(user.getDob());

			/*gelleryImageView.setVisibility(View.GONE);
			cameraImageView.setVisibility(View.GONE);
			passwordLayout.setVisibility(View.GONE);
			signUpButton.setVisibility(View.GONE);

		} else {
			logoutImageView.setVisibility(View.GONE);
			Bundle bundle = getIntent().getExtras();
			
			if (bundle != null && bundle.getBoolean("isUsingFBLogin")) {
				isUsingFBLogin = bundle.getBoolean("isUsingFBLogin");
				String name = bundle.getString("name");
				
				String email = bundle.getString("email");
				String dateOB = bundle.getString("dateOB");
				if (dateOB != null && dateOB.length()>8) {
					dateOB = dateOB.replace("/", "-");
					try{
						String[] splits = dateOB.split("-");
						mDay = Integer.parseInt(splits[0]);
						mMonth = Integer.parseInt(splits[1])-1;
						mYear = Integer.parseInt(splits[2]);
					}catch (Exception e) {
						e.printStackTrace();
					}
				}
				String location = bundle.getString("location");

				usernameField.setText(name);
				emailField.setText(email);
				addressField.setText(location);
				userDobButton.setText(dateOB);

				String imageUrl = "http://graph.facebook.com/" + Util.FB_ID + "/picture?type=large";
				ImageLoader imageLoader = ImageLoader.getInstance();
				imageLoader.init(ImageLoaderConfiguration.createDefault(RegistrationActivity.this));
				Util.urlLoadImage(imageLoader, imageUrl, new SimpleImageLoadingListener() {
					public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
						super.onLoadingComplete(imageUri, view, loadedImage);

						String filePath = getExternalFilesDir(null) + "/" + Util.FB_ID + ".JPG";
						File file = new File(filePath);
						writeExternalToCache(loadedImage, file);
						//userImageView.setImageBitmap(Util.getRoundedShape(loadedImage));
						Util.makeMaskImage(RegistrationActivity.this, userImageView, loadedImage);
					}
				});
				// User user = Util.getLoggedInUser(RegistrationActivity.this);
				// user.setUserImageURL(imageUrl);
				// Util.saveUser(user, RegistrationActivity.this);
			}*/
		}

	}



	public static final int BUFFER_SIZE = 1024 * 8;

	void writeExternalToCache(Bitmap bitmap, File file) {
		try {
			file.createNewFile();
			FileOutputStream fos = new FileOutputStream(file);
			final BufferedOutputStream bos = new BufferedOutputStream(fos, BUFFER_SIZE);
			bitmap.compress(CompressFormat.JPEG, 100, bos);
			bos.flush();
			bos.close();
			fos.close();
			imagePath = file.getAbsolutePath();

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {

		}

	}

	public void letsSignOut(View button) {
		Util.displayAlert(RegistrationActivity.this, "Are you sure you want to logout", new OnClickListener() {

			@Override
			public void onClick(View v) {
				//clsoe facebook session
				callFacebookLogout(RegistrationActivity.this);
				if(DashboardActivity.getInstance() != null){
					DashboardActivity.getInstance().unRegisterGCM();
				}
				Util.removeUserAndLogout(RegistrationActivity.this);
				Util.startNewMainActivity(RegistrationActivity.this, SigninActivity.class);
			}
		});
	}


	public static void callFacebookLogout(Context context) {
	    Session session = Session.getActiveSession();
	    if (session != null) {

	        if (!session.isClosed()) {
	            session.closeAndClearTokenInformation();
	        }
	    } else {

	        session = new Session(context);
	        Session.setActiveSession(session);

	        session.closeAndClearTokenInformation();
	    }

	}




	public void letsSignup(View button) {
		String name = usernameField.getText().toString().trim();
		String email = emailField.getText().toString().trim();
		String password = passwordField.getText().toString().trim();
		String address = addressField.getText().toString().trim();
		//String area = areaField.getText().toString().trim();
		String city = cityField.getText().toString().trim();
		String phone = phoneField.getText().toString().trim();
		String landline = landlineField.getText().toString().trim();
		String dob = mYear + "/" + mMonth + "/" + mDay;



		if (name.length() <= 0) {
            Toast.makeText(this, "Please enter name.", Toast.LENGTH_LONG).show();
            return;
        }
        else
        {
            String expression = "^[a-zA-Z ]*$";
            CharSequence inputStr = name;
            Pattern pattern = Pattern.compile(expression);
            Matcher matcher = pattern.matcher(inputStr);
            if(!matcher.matches())
            {
                //if pattern matches
                Toast.makeText(this, "Please enter a valid Name.", Toast.LENGTH_LONG).show();
                return;
            }


        }


        //isValidEmail(email);



		if (email.length() <= 0) {
			Toast.makeText(this, "Please enter email.", Toast.LENGTH_LONG).show();
			return;
		}
        else
        if(!android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches())
        {
            Toast.makeText(c, "Please Enter a valid Email.", Toast.LENGTH_LONG).show();
            return;
        }

		if (password.length() <= 0) {
			Toast.makeText(this, "Please enter password.", Toast.LENGTH_LONG).show();
			return;
		}

        if (phone.length() <= 0) {
            Toast.makeText(this, "Please enter Mobile Number.", Toast.LENGTH_LONG).show();
            return;
        }
        else {
            for (int i = 0; i < phone.length(); i++) {
                if (!Character.isDigit(phone.charAt(i))) {
                    Toast.makeText(this, "Please enter a valid Mobile Number.", Toast.LENGTH_LONG).show();
                    return;
                }
            }

        }


        if (landline.length() <= 0) {
            Toast.makeText(this, "Please enter Phone Number.", Toast.LENGTH_LONG).show();
            return;
        }
        else {
            for (int i = 0; i < landline.length(); i++) {
                if (!Character.isDigit(landline.charAt(i))) {
                    Toast.makeText(this, "Please enter a valid Phone Number.", Toast.LENGTH_LONG).show();
                    return;
                }
            }

        }

        if (mYear == 0 || mMonth < 0 || mDay == 0) {
            Toast.makeText(this, "Please select date of birth.", Toast.LENGTH_LONG).show();
            return;
        }

		if (address.length() <= 0) {
			Toast.makeText(this, "Please enter address.", Toast.LENGTH_LONG).show();
			return;
		}
		/*if (area.length() <= 0) {
			Toast.makeText(this, "Please enter area.", Toast.LENGTH_LONG).show();
			return;
		}*/
		if (city.length() <= 0) {
			Toast.makeText(this, "Please enter city.", Toast.LENGTH_LONG).show();
			return;
		}



		if (!Util.isNetworkAvailable(RegistrationActivity.this)) {

			Util.showAlert("No Internet access.", RegistrationActivity.this, true);
			return;
		}
		HashMap<String, String> map = new HashMap<String, String>();
		map.put("user_fullname", name);
		map.put("user_email", email);
		map.put("user_pass", password);
		map.put("user_address", address);
		map.put("user_city", city);
		map.put("user_area", "Model town");
		map.put("user_dob", dob);
		map.put("user_phone", landline);
		map.put("user_cphone", phone);

		if(isUsingFBLogin){

			map.put("user_fbid", Util.FB_ID);
			map.put("app_secret", getResources().getString(R.string.facebook_app_id));
		}
		//Removing image from sign up on clients demand.
		new SignupAsyncTask().execute(map, null/*imagePath*/);
	}

	public void checkLoginAndContinue() {
		if (Util.isLoggedIn(this)) {
			Util.startNewMainActivity(this, HomeScreen.class);
		}
	}

	private class SignupAsyncTask extends AsyncTask<Object, Void, User> {

		@Override
		protected void onPreExecute() {

			super.onPreExecute();
			showProgressDialog("Sign up in progress.", "Please wait...");
		}

		@SuppressWarnings("unchecked")
		@Override
		protected User doInBackground(Object... objects) {
			return Webservice.signUp((HashMap<String, String>) objects[0], (String) objects[1], RegistrationActivity.this);
		}

		@Override
		protected void onPostExecute(User user) {
			super.onPostExecute(user);
			dismissProgressDialog();
			if (user.isLoggedIn()) {

				//user.setUserImageURL(imagePath);
				Util.saveUser(user, RegistrationActivity.this);
				checkLoginAndContinue();
			} else {
				Util.showMessage(user.getErrorMessage(), RegistrationActivity.this);
			}
		}
	}

	private void showProgressDialog(String title, String message) {

		if (dialog == null) {
			dialog = new ProgressDialog(RegistrationActivity.this);
		}
		dialog.setTitle(title);
		dialog.setCanceledOnTouchOutside(false);
		dialog.setMessage(message);
		dialog.show();
	}

	private void dismissProgressDialog() {
		if (dialog != null) {
			if (dialog.isShowing()) {
				dialog.dismiss();
				dialog = null;
			}
		}
	}

	@SuppressWarnings("deprecation")
	public void selectDob(View button) {
		showDialog(DATE_DIALOG_ID);
	}

	private DatePickerDialog.OnDateSetListener mDateSetListener = new DatePickerDialog.OnDateSetListener() {
		@Override
		public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
			mYear = year;
			mMonth = monthOfYear;
			mDay = dayOfMonth;
			RegistrationActivity.this.userDobButton.setText(new StringBuilder()
			// Month is 0 based so add 1
					.append(mMonth + 1).append("-").append(mDay).append("-").append(mYear).append(" "));

		}
	};

	public void selectUserImage(View button) {
		if (button.getTag().toString().equalsIgnoreCase("0")) {
			Intent intent = new Intent(Intent.ACTION_PICK,
					android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
			intent.setType("image/*");
			startActivityForResult(Intent.createChooser(intent, "Select File"), REQ_IMAGE_FROM_GALERY);
		} else {
			Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
			File f = new File(android.os.Environment.getExternalStorageDirectory(), "temp.jpg");
			intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(f));
			startActivityForResult(intent, REQ_IMAGE_FROM_CAMERA);
		}
		// selectImage();

	}

	@Override
	protected Dialog onCreateDialog(int id) {
		switch (id) {
		case DATE_DIALOG_ID:
			return new DatePickerDialog(this, mDateSetListener, mYear, mMonth, mDay);
		}
		return null;
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		if (resultCode == RESULT_OK) {
			if (requestCode == REQ_IMAGE_FROM_CAMERA) {

				File f = new File(Environment.getExternalStorageDirectory().toString());
		        for (File temp : f.listFiles()) {
		            if (temp.getName().equals("temp.jpg")) {
		                f = temp;
		                break;
		            }
		        }

		        try {
		            Bitmap bitmap;
		            bitmap = decodeFile(f);
	                if (bitmap == null) {
	                    return;
	                }
	                this.imagePath = f.getAbsolutePath();
	                Util.makeMaskImage(RegistrationActivity.this, userImageView, bitmap);
		        } catch (Exception e) {
		            e.printStackTrace();
		        }

			} else if (requestCode == REQ_IMAGE_FROM_GALERY) {
				Uri selectedImageUri = data.getData();

				String tempPath = getPath(selectedImageUri, RegistrationActivity.this);
				imagePath = tempPath;
				Log.i("RegistrationActivity", "Gallery FilePath " + imagePath);
				BitmapFactory.Options btmapOptions = new BitmapFactory.Options();
				Bitmap originalBitmap = BitmapFactory.decodeFile(tempPath, btmapOptions);
				//userImageView.setImageBitmap(Util.getRoundedShape(originalBitmap));
				Util.makeMaskImage(RegistrationActivity.this, userImageView, originalBitmap);
			}
		}
	}

	private Bitmap decodeFile(File f) {
        try {
            BitmapFactory.Options o2 = new BitmapFactory.Options();
            return BitmapFactory.decodeStream(new FileInputStream(f), null, o2);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

	public String getPath(Uri uri, Activity activity) {
		String[] projection = { MediaColumns.DATA };
		@SuppressWarnings("deprecation")
		Cursor cursor = activity.managedQuery(uri, projection, null, null, null);
		int column_index = cursor.getColumnIndexOrThrow(MediaColumns.DATA);
		cursor.moveToFirst();
		return cursor.getString(column_index);
	}
}
