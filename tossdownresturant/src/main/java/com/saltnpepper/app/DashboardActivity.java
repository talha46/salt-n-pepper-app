package com.saltnpepper.app;

import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;

import org.json.JSONObject;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import android.graphics.Bitmap;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.Toast;

import com.saltnpepper.app.cart.ShoppingCart;
import com.saltnpepper.app.datamodel.User;
import com.saltnpepper.app.utils.Util;
import com.saltnpepper.app.webservices.Webservice;
import com.facebook.FacebookAuthorizationException;
import com.facebook.FacebookOperationCanceledException;
import com.facebook.FacebookRequestError;
import com.facebook.HttpMethod;
import com.facebook.Request;
import com.facebook.RequestAsyncTask;
import com.facebook.Response;
import com.facebook.Session;
import com.facebook.SessionState;
import com.facebook.UiLifecycleHelper;
import com.facebook.model.GraphUser;
import com.facebook.widget.FacebookDialog;
import com.facebook.widget.LoginButton;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.SimpleImageLoadingListener;

public class DashboardActivity extends FragmentActivity {

	private static DashboardActivity instance = null;
	public static final String EXTRA_MESSAGE = "message";
	public static final String PROPERTY_REG_ID = "registration_id";
	private static final String PROPERTY_APP_VERSION = "appVersion";
	private final static int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;

	/**
	 * Substitute you own sender ID here. This is the project number you got
	 * from the API Console, as described in "Getting Started."
	 */
	String SENDER_ID = "896487364955";

	private final String TAG = "DashboardActivity";
	private GoogleCloudMessaging gcm;

	private static Context context;
	private ImageView userImageView;
	String regid;

	private ImageView user_checkin;

	private PendingAction pendingAction = PendingAction.NONE;
	private LocationManager locationManager;

	private enum PendingAction {
		NONE, POST_PHOTO, POST_STATUS_UPDATE
	}

	private final String PENDING_ACTION_BUNDLE_KEY = "com.bundukhan.app:PendingAction";

	private UiLifecycleHelper uiHelper;

	private Session.StatusCallback callback = new Session.StatusCallback() {
		@Override
		public void call(Session session, SessionState state, Exception exception) {
			onSessionStateChange(session, state, exception);
		}
	};

	private FacebookDialog.Callback dialogCallback = new FacebookDialog.Callback() {
		@Override
		public void onError(FacebookDialog.PendingCall pendingCall, Exception error, Bundle data) {
			Log.d("SigninActivity", String.format("Error: %s", error.toString()));
			Toast.makeText(DashboardActivity.this, String.format("Error: %s", error.toString()), Toast.LENGTH_LONG)
					.show();
		}

		@Override
		public void onComplete(FacebookDialog.PendingCall pendingCall, Bundle data) {
			Log.d("SigninActivity", "Success!");
		}
	};

	public static DashboardActivity getInstance() {
		return instance;
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		uiHelper = new UiLifecycleHelper(this, callback);
		uiHelper.onCreate(savedInstanceState);
		if (savedInstanceState != null) {
			String name = savedInstanceState.getString(PENDING_ACTION_BUNDLE_KEY);
			pendingAction = PendingAction.valueOf(name);
		}
		instance = this;
		setContentView(R.layout.activity_dashboard_tablelayout);
		userImageView = (ImageView) findViewById(R.id.user_profile_image);
		context = this;
		context = getApplicationContext();

		final LoginButton authButton = (LoginButton) findViewById(R.id.authButton);
		authButton.setBackgroundResource(R.drawable.signin_facebook);
		authButton.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
		// authButton.setReadPermissions(Arrays.asList("public_profile",
		// "email"));
		authButton.setPublishPermissions(Arrays.asList("publish_actions"));
		authButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				authButton.setUserInfoChangedCallback(new LoginButton.UserInfoChangedCallback() {
					@Override
					public void onUserInfoFetched(GraphUser user) {
						checkInIntoBK();
					}
				});
			}
		});
		user_checkin = (ImageView) findViewById(R.id.user_checkin);
		user_checkin.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				checkInIntoBK();
				/*
				 * if(hasPublishPermission()){ checkInIntoBK(); }else{
				 * authButton.performClick(); }
				 */

			}
		});

		// Check device for Play Services APK. If check succeeds, proceed with
		// GCM registration.
		if (checkPlayServices()) {
			gcm = GoogleCloudMessaging.getInstance(this);
			regid = getRegistrationId(context);

			if (regid.isEmpty()) {
				registerInBackground();
			}
		} else {
			Log.i(TAG, "No valid Google Play Services APK found.");
		}
		// Preload user bitmap
		User user = Util.getLoggedInUser(this);
		ImageLoader imageLoader = ImageLoader.getInstance();
		imageLoader.init(ImageLoaderConfiguration.createDefault(DashboardActivity.this));
		Util.urlLoadImage(imageLoader, user.getUserImageURL(), new SimpleImageLoadingListener() {
			public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
				super.onLoadingComplete(imageUri, view, loadedImage);
				// userImageView.setImageBitmap(Util.getRoundedShape(loadedImage));
			}
		});

		ShoppingCart.dispose();

		locationManager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);
		Location lastNetwork = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
		Location lastGps = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
		Util.saveLocation(Util.isBetterLocation(lastNetwork, lastGps) ? lastGps : lastNetwork, this);
	}

	private boolean hasRequestedPermission = false;

	private void checkInIntoBK() {
		hasRequestedPermission = false;

		final String PERMISSIONS = "publish_actions";
		Session.openActiveSession(DashboardActivity.this, true, new Session.StatusCallback() {
			// callback when session changes state
			@Override
			public void call(Session session, SessionState state, Exception exception) {
				if (session != null && session.isOpened()) {
					// Check for publish permissions
					if (!hasPublishPermission()) {
						if (hasRequestedPermission) {
							Util.showMessage("Check-In failed", "App doesn't have Check-In permission.",
									DashboardActivity.this);
							return;
						}
						hasRequestedPermission = true;
						Session.NewPermissionsRequest newPermissionsRequest = new Session.NewPermissionsRequest(
								DashboardActivity.this, PERMISSIONS);
						session.requestNewPublishPermissions(newPermissionsRequest);
						return;
					}
					Intent intent = new Intent(DashboardActivity.this, CheckInActivity.class);
					startActivity(intent);
				}
			}
		});

	}

	private boolean hasPublishPermission() {
		Session session = Session.getActiveSession();
		Log.d("DashboardActivity", "Permissions: " + session.getPermissions().toString());
		return session != null && session.getPermissions().contains("publish_actions");
	}

	@Override
	protected void onPause() {
		super.onPause();
		uiHelper.onPause();
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		uiHelper.onDestroy();
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		uiHelper.onActivityResult(requestCode, resultCode, data, dialogCallback);
	}

	@Override
	public void onResume() {
		super.onResume();
		uiHelper.onResume();
		if (!Util.isLoggedIn(DashboardActivity.this)) {
			finish();
			return;
		}
		Criteria criteria = new Criteria();
		String provider = locationManager.getBestProvider(criteria, true);
		if (provider != null) {
			Log.i("DashboardActivity:onResume()", "Provider" + provider);
			locationManager.requestLocationUpdates(provider, 0, 0, locationListener);
		} else {
			Log.i("DashboardActivity:onResume()", "Provider is null");
		}
	}

	public void showProfile(View profile) {
		Intent intent = new Intent(DashboardActivity.this, RegistrationActivity.class);
		intent.putExtra(RegistrationActivity.ACTIVITY_TYPE, RegistrationActivity.TYPE_PROFILE);
		startActivity(intent);
	}

	public void menuItemClicked(View v) {

		int id = v.getId();
		
//		if (!Util.isNetworkAvailable(DashboardActivity.this)) {
//			Util.showAlert("No Internet access.", DashboardActivity.this, true);
//			return;
//		}
		if (id == R.id.image_res) {

			startActivity(new Intent(DashboardActivity.this, ReserveTableActivity.class));
		} else if (id == R.id.image_our_menu) {

			if (Util.isNetworkAvailable(DashboardActivity.this)) {
				Intent intent = new Intent(DashboardActivity.this, SelectLocationsActivity.class);
				intent.putExtra(LocationsActivity.ACTIVITY_TYPE, LocationsActivity.TYPE_SELECT_MENU);
				startActivity(intent);// new Intent(DashboardActivity.this,
				// MenuActivity.class)
			} else {
				Util.showAlert("No Internet access.", DashboardActivity.this, true);
			}
		} else if (id == R.id.image_my_basket) {

			if (ShoppingCart.getInstance().getOrderItems() != null
					&& ShoppingCart.getInstance().getOrderItems().size() > 0) {
				startActivity(new Intent(DashboardActivity.this, CartDetailActivity.class));
			} else {
				Toast.makeText(DashboardActivity.this, "Basket is empty.", Toast.LENGTH_SHORT).show();
			}
		} else if (id == R.id.image_locations) {
			if (Util.isNetworkAvailable(DashboardActivity.this)) {
				startActivity(new Intent(DashboardActivity.this, LocationsActivity.class));
			}else{
				Util.showAlert("No Internet access.", DashboardActivity.this, true);
			}
		} else if (id == R.id.image_deals) {
			if (Util.isNetworkAvailable(DashboardActivity.this)) {
				startActivity(new Intent(DashboardActivity.this, DealsActivity.class));
			}else{
				Util.showAlert("No Internet access.", DashboardActivity.this, true);
			}
		} else if (id == R.id.image_feedback) {
			startActivity(new Intent(DashboardActivity.this, FeedbackActivity.class));
		}
	}

	// ==========================================================================================
	// Google Cloud messaging code.
	// ==========================================================================================

	/**
	 * Check the device to make sure it has the Google Play Services APK. If it
	 * doesn't, display a dialog that allows users to download the APK from the
	 * Google Play Store or enable it in the device's system settings.
	 */
	private boolean checkPlayServices() {

		int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);
		if (resultCode != ConnectionResult.SUCCESS) {
			if (GooglePlayServicesUtil.isUserRecoverableError(resultCode)) {

				GooglePlayServicesUtil.getErrorDialog(resultCode, this, PLAY_SERVICES_RESOLUTION_REQUEST).show();
			} else {

				Log.i("DashboardActivity", "This device is not supported.");
				Toast.makeText(DashboardActivity.this, "This device doesn't support Google Play services.",
						Toast.LENGTH_LONG).show();
				finish();
			}
			return false;
		}
		return true;
	}

	/**
	 * Gets the current registration ID for application on GCM service.
	 * <p>
	 * If result is empty, the app needs to register.
	 * 
	 * @return registration ID, or empty string if there is no existing
	 *         registration ID.
	 */
	private String getRegistrationId(Context context) {
		String registrationId = Util.getStringWithKey(PROPERTY_REG_ID, context);
		if (registrationId == null || registrationId.isEmpty()) {
			Log.i(TAG, "Registration not found.");
			return "";
		}
		// Check if app was updated; if so, it must clear the registration ID
		// since the existing regID is not guaranteed to work with the new
		// app version.
		int registeredVersion = 0;
		try {
			registeredVersion = Integer.parseInt(Util.getStringWithKey(PROPERTY_APP_VERSION, context));
		} catch (Exception ex) {
		}
		;
		int currentVersion = getAppVersion(context);
		if (registeredVersion != currentVersion) {
			Log.i(TAG, "App version changed.");
			return "";
		}
		return registrationId;
	}

	/**
	 * @return Application's version code from the {@code PackageManager}.
	 */
	private static int getAppVersion(Context context) {
		try {
			PackageInfo packageInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
			return packageInfo.versionCode;
		} catch (NameNotFoundException e) {
			// should never happen
			throw new RuntimeException("Could not get package name: " + e);
		}
	}

	/**
	 * Registers the application with GCM servers asynchronously.
	 * <p>
	 * Stores the registration ID and app versionCode in the application's
	 * shared preferences.
	 */
	private void registerInBackground() {

		Void params = null;
		new RegisterAsyncTask().execute(params);
	}

	private class RegisterAsyncTask extends AsyncTask<Void, Void, String> {

		@Override
		protected String doInBackground(Void... params) {

			String msg = "";
			try {
				if (gcm == null) {
					gcm = GoogleCloudMessaging.getInstance(context);
				}
				regid = gcm.register(SENDER_ID);
				msg = "Device registered, registration ID=" + regid;

				// You should send the registration ID to your server over HTTP,
				// so it can use GCM/HTTP or CCS to send messages to your app.
				// The request to your server should be authenticated if your
				// app
				// is using accounts.
				sendRegistrationIdToBackend();

				// Persist the regID - no need to register again.
				storeRegistrationId(context, regid);

			} catch (IOException ex) {
				msg = "Error :" + ex.getMessage();
				// If there is an error, don't just keep trying to register.
				// Require the user to click a button again, or perform
				// exponential back-off.
			}
			return msg;
		}

		@Override
		protected void onPostExecute(String msg) {
			super.onPostExecute(msg);
			Log.i(TAG, msg);
		}

		/**
		 * Sends the registration ID to your server over HTTP, so it can use
		 * GCM/HTTP or CCS to send messages to your app. Not needed for this
		 * demo since the device sends upstream messages to a server that echoes
		 * back the message using the 'from' address in the message.
		 */
		private void sendRegistrationIdToBackend() {

			if (regid != null) {
				HashMap<String, String> map = new HashMap<String, String>();
				map.put("user_device_type", "android");
				map.put("user_push_token", regid);
				User user = Util.getLoggedInUser(DashboardActivity.this);
				if (user != null) {
					map.put("user_id", user.getId());
				}
				Webservice.savePushToken(map, DashboardActivity.this);
			}
		}

		/**
		 * Stores the registration ID and app versionCode in the application's
		 * {@code SharedPreferences}.
		 * 
		 * @param context
		 *            application's context.
		 * @param regId
		 *            registration ID
		 */
		private void storeRegistrationId(Context context, String regId) {
			int appVersion = getAppVersion(context);
			Log.i(TAG, "Saving regId on app version " + appVersion);
			Util.saveStringWithKey(PROPERTY_REG_ID, regId, context);
			Util.saveStringWithKey(PROPERTY_APP_VERSION, "" + appVersion, context);

		}
	}

	private void onSessionStateChange(Session session, SessionState state, Exception exception) {
		if (pendingAction != PendingAction.NONE
				&& (exception instanceof FacebookOperationCanceledException || exception instanceof FacebookAuthorizationException)) {
			Util.showMessage("Canceled", "Permissions not granted", DashboardActivity.this);
			pendingAction = PendingAction.NONE;
		}
	}

	private class UnRegisterAsyncTask extends AsyncTask<Void, Void, String> {

		@Override
		protected String doInBackground(Void... params) {

			String msg = "";
			try {
				if (gcm == null) {
					gcm = GoogleCloudMessaging.getInstance(context);
				}
				gcm.unregister();
				msg = "Device unregistered from GCM";

			} catch (IOException ex) {
				msg = "Error :" + ex.getMessage();
			}
			return msg;
		}

		@Override
		protected void onPostExecute(String msg) {
			super.onPostExecute(msg);
			Log.d(TAG, msg);
		}
	}

	public void unRegisterGCM() {
		Void params = null;
		new UnRegisterAsyncTask().execute(params);
	}

	// Define a listener that responds to location updates
	LocationListener locationListener = new LocationListener() {
		public void onLocationChanged(Location location) {
			// Called when a new location is found by the network location
			// provider.
			// makeUseOfNewLocation(location);
			Util.saveLocation(location, DashboardActivity.this);
			locationManager.removeUpdates(this);
			Log.i("GPS Location:onLocationChanged()",
					"lat:" + location.getLatitude() + " lat:" + location.getLongitude());
		}

		public void onStatusChanged(String provider, int status, Bundle extras) {
		}

		public void onProviderEnabled(String provider) {
			Log.i("GPS Location:onProviderEnabled()", "provider:" + provider);
		}

		public void onProviderDisabled(String provider) {
			Log.i("GPS Location:onProviderDisabled()", "provider:" + provider);
		}
	};
}
