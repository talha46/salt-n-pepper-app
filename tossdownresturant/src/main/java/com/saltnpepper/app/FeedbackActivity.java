package com.saltnpepper.app;

import java.util.HashMap;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.saltnpepper.app.datamodel.FeedbackResponse;
import com.saltnpepper.app.datamodel.User;
import com.saltnpepper.app.utils.Constants;
import com.saltnpepper.app.utils.Util;
import com.saltnpepper.app.webservices.Webservice;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.SimpleImageLoadingListener;

public class FeedbackActivity extends Activity {
	private Spinner spinerDate, spinerGuests;
	TextView branchLocationButton;
	String branchId = null;
	EditText commentsField;
	ImageView userImageView;
	private ProgressDialog dialog = null;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_feedback_layout);		

		branchLocationButton = (TextView) findViewById(R.id.feedback_branch_locations);
		commentsField = (EditText) findViewById(R.id.feedback_text_comments);
		spinerGuests = (Spinner) findViewById(R.id.feedback_guests);
		spinerDate = (Spinner) findViewById(R.id.spiner_time);
		userImageView = (ImageView) findViewById(R.id.feedback_user_image);

		String[] guests = new String[51];
		guests[0] = getResources().getString(R.string.no_of_guests);
		for (int i = 1; i < guests.length; i++) {
			guests[i] = i + 1 + "";
		}
		ArrayAdapter<String> guestsSpinnerArrayAdapter = new ArrayAdapter<String>(this, R.layout.spiner_item, guests);
		spinerGuests.setAdapter(guestsSpinnerArrayAdapter);
		spinerGuests.setSelection(0);

		String[] dates = { "Please Select", "Breakfast", "Lunch", "Dinner", "Home Delivery" };
		ArrayAdapter<String> datesSpinnerArrayAdapter = new ArrayAdapter<String>(this, R.layout.spiner_item, dates);
		spinerDate.setAdapter(datesSpinnerArrayAdapter);
		spinerDate.setSelection(0);

		User user = Util.getLoggedInUser(FeedbackActivity.this);

		ImageLoader imageLoader = ImageLoader.getInstance();
		imageLoader.init(ImageLoaderConfiguration.createDefault(FeedbackActivity.this));
		Util.urlLoadImage(imageLoader, user.getUserImageURL(), new SimpleImageLoadingListener() {
			public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
				super.onLoadingComplete(imageUri, view, loadedImage);
				//userImageView.setImageBitmap(Util.getRoundedShape(loadedImage));
				Util.makeMaskImage(FeedbackActivity.this, userImageView, loadedImage);
			}
		});
	}

	public void selectBranchLocation(View v) {
		if (Util.isNetworkAvailable(this)) {
			Intent intent = new Intent(this, LocationsActivity.class);
			intent.putExtra(LocationsActivity.ACTIVITY_TYPE, LocationsActivity.TYPE_SELECT_AND_PASSBACK);
			startActivityForResult(intent, LocationsActivity.TYPE_SELECT_AND_PASSBACK);
		} else {
			Util.showAlert("No Internet access.", this, false);
		}
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		if (resultCode == RESULT_OK) {
			if (requestCode == LocationsActivity.TYPE_SELECT_AND_PASSBACK) {
				branchId = data.getStringExtra(Constants.BRANCH_ID_KEY);
				String branchLocation = data.getStringExtra(Constants.BRANCH_LOCATATION_KEY);
				branchLocationButton.setText(branchLocation);
			}
		}

	}

	public void sendFeedback(View v) {
		String comments = commentsField.getText().toString().trim();

		if (branchId == null) {
			Toast.makeText(this, "Please select a branch location", Toast.LENGTH_LONG).show();
			return;
		}
		if (spinerDate.getSelectedItemPosition() == 0) {
			Toast.makeText(this, "Please select the date of your visit.", Toast.LENGTH_LONG).show();
			return;
		} else if (spinerGuests.getSelectedItemPosition() == 0) {
			Toast.makeText(this, "Select number of guests.", Toast.LENGTH_LONG).show();
			return;
		}
		if (comments.length() <= 0) {
			Toast.makeText(this, "Please some comments.", Toast.LENGTH_LONG).show();
			return;
		}
		HashMap<String, String> map = new HashMap<String, String>();
		map.put("location_id", branchId);
		map.put("user_id", Util.getLoggedInUser(this).getId());
		map.put("guests_count", spinerGuests.getSelectedItem().toString());
		map.put("meal", spinerDate.getSelectedItem().toString());
		map.put("comments", comments);

		new SendFeedbackAsyncTask().execute(map);

	}

	private class SendFeedbackAsyncTask extends AsyncTask<Object, Void, FeedbackResponse> {

		@Override
		protected void onPreExecute() {

			super.onPreExecute();
			showProgressDialog("Sending feedback.", "Please wait...");
		}

		@SuppressWarnings("unchecked")
		@Override
		protected FeedbackResponse doInBackground(Object... objects) {
			return Webservice.sendFeedback((HashMap<String, String>) objects[0], FeedbackActivity.this);
		}

		@Override
		protected void onPostExecute(FeedbackResponse response) {
			super.onPostExecute(response);
			dismissProgressDialog();
			if (response.isSuccess()) {
				// will exits
				Util.showAlert(response.getMessage(), FeedbackActivity.this, false);
			} else {
				// won't exit
				Util.showMessage(response.getMessage(), FeedbackActivity.this);
			}
		}

	}

	private void showProgressDialog(String title, String message) {

		if (dialog == null) {
			dialog = new ProgressDialog(FeedbackActivity.this);
		}
		dialog.setTitle(title);
		dialog.setCanceledOnTouchOutside(false);
		dialog.setMessage(message);
		dialog.show();
	}

	private void dismissProgressDialog() {
		if (dialog != null) {
			if (dialog.isShowing()) {
				dialog.dismiss();
				dialog = null;
			}
		}
	}
}
