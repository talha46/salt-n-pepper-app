package com.saltnpepper.app;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.facebook.UiLifecycleHelper;

/**
 * Created by talha on 8/16/16.
 */
public class Home extends Activity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.home);

    }

    public void Signup(View v) {
        startActivity(new Intent(Home.this, RegistrationActivity.class));
    }

    public void SignIn(View v) {
        startActivity(new Intent(Home.this, SigninActivity.class));
    }
}
