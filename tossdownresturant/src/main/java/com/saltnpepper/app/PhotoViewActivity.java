package com.saltnpepper.app;

import android.app.Activity;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.widget.Toast;

import java.util.ArrayList;

/**
 * Created by talha on 8/19/16.
 */
public class PhotoViewActivity extends Activity {

    ViewPager viewPager;
    private ArrayList<Bitmap> mPhotos = null;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.photo_view);

       mPhotos = getIntent().getParcelableArrayListExtra("imgArray");

        try
        {
            viewPager = (ViewPager)findViewById(R.id.view_pg_photos);
            PagerAdapter adapter = new ImagePagerAdapter(PhotoViewActivity.this, mPhotos);
            viewPager.setAdapter(adapter);
        }
        catch (Exception e)
        {
            Toast.makeText(PhotoViewActivity.this,e.getMessage(),Toast.LENGTH_LONG).show();
        }


        // Get intent data
        //Intent i = getIntent();

        // Selected image id
        //int position = i.getExtras().getInt("id");
        //ImageAdapter imageAdapter = new ImageAdapter(this);

        //ImageView imageView = (ImageView) findViewById(R.id.SingleView);
        //imageView.setImageResource(imageAdapter.mThumbIds[position]);


        /*viewPager = (ViewPager) findViewById(R.id.viewPager);
        PagerAdapter adapter = new CustomAdapter(PhotoViewActivity.this);
        viewPager.setAdapter(adapter);

        String dealImageUrl = null;
        Bundle b = getIntent().getExtras();
        if(b != null){
            dealImageUrl = b.getString("dealImageUrl");
            //Toast.makeText(PhotoViewActivity.this,dealImageUrl,Toast.LENGTH_LONG).show();
        }

        //dealImageUrl = getIntent().getStringExtra("dealImageUrl");

        ImageView back_arrow = (ImageView) findViewById(R.id.back_arrow);
        back_arrow.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                finish();
            }
        });*/

        /*final ProgressBar progressbar = (ProgressBar) findViewById(R.id.progressBarphoto);
        final ImageView arrowImage = (ImageView) findViewById(R.id.SingleView);
        ImageLoader imageLoader = ImageLoader.getInstance();
        imageLoader.init(ImageLoaderConfiguration.createDefault(PhotoViewActivity.this));
        Util.urlLoadImage(imageLoader, dealImageUrl, new SimpleImageLoadingListener() {
            public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {

                progressbar.setVisibility(View.GONE);
                super.onLoadingComplete(imageUri, view, loadedImage);
                arrowImage.setImageBitmap(loadedImage);
            }

            @Override
            public void onLoadingStarted(String imageUri, View view) {
                progressbar.setVisibility(View.VISIBLE);
                super.onLoadingStarted(imageUri, view);
            }

            @Override
            public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
                super.onLoadingFailed(imageUri, view, failReason);
                progressbar.setVisibility(View.GONE);

            }
        });*/

    }

}
