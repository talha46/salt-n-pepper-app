package com.saltnpepper.app.webservices;

import android.content.Context;
import android.location.Location;
import android.util.Log;

import com.saltnpepper.app.cart.ShoppingCart;
import com.saltnpepper.app.datamodel.Deals;
import com.saltnpepper.app.datamodel.FeedbackResponse;
import com.saltnpepper.app.datamodel.Locations;
import com.saltnpepper.app.datamodel.MenuDetails;
import com.saltnpepper.app.datamodel.Photos;
import com.saltnpepper.app.datamodel.ResTableResponse;
import com.saltnpepper.app.datamodel.User;
import com.saltnpepper.app.utils.Constants;
import com.saltnpepper.app.utils.MutableString;
import com.saltnpepper.app.utils.Util;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class Webservice {

	private static final String TD_LIVE_SERVER_URL = "http://tossdown.com/"; // live
	private static final String TD_BETA_SERVER_URL = "http://beta1.tossdown.com/"; // beta
	private static final String TOSSDOWN_URL = TD_LIVE_SERVER_URL;
	private static final String USER_SERVER_URL = "http://www.horizonwebsolutions.net/test/snp/api/"; // beta
	
	public static HashMap<String, String> addMetaDataToMap(HashMap<String, String> map, Context c) {
		// &lat=31.463495&lng=74.319262&&
		String imei = Util.getStringWithKey(Constants.IMEI_NUMBER, c);
		if(imei != null)
			map.put("imei", imei);
		else
			map.put("imei", "");
		
		String carrier = Util.getStringWithKey(Constants.CARRIER_NAME, c);
		if(carrier!=null)
			map.put("network", carrier);
		else
			map.put("network", "");
		
		if (map.get("rid") == null) {
			// if rid is null before, add default id
			map.put("rid", Constants.RESTAURANT_ID);
		}
		// set location
		Location lastLocation = Util.getLocation(c);
		if (lastLocation != null) {
			map.put("lat", String.valueOf(lastLocation.getLatitude()));
			map.put("lng", String.valueOf(lastLocation.getLongitude()));
		}
		map.put("device", "android");
		map.put("platform", "android");// feature=Bundu%20Khan%20Deals

		return map;
	}
	
	public static String mapToPOSTString(HashMap<String, String> map, Context c) {
		String postString = "";
		map = addMetaDataToMap(map, c);
		for (Map.Entry<String, String> entry : map.entrySet()) {
			String encodedValue ="";
			try {
				encodedValue = URLEncoder.encode(entry.getValue(), "utf-8");
			} catch (UnsupportedEncodingException e) {
				e.printStackTrace();
			}
			postString = postString + entry.getKey() + "=" + encodedValue+ "&";
		}
		return postString;
	}
	
	public static User signIn(HashMap<String, String> map, Context c) {
		// http://www.horizonwebsolutions.net/test/bunduapp/signin_processing_unit.php
		String urlString = USER_SERVER_URL + "signin";

		String params = mapToPOSTString(map, c);
		map.put("feature", "SignIn");

		String data = openHttpPostConnection(urlString, params);

		return User.parseSignInResponse(data);
	}

	public static FeedbackResponse sendFeedback(HashMap<String, String> map, Context c) {
		// http://www.horizonwebsolutions.net/test/bunduapp/feedback_save.php
		String urlString = USER_SERVER_URL + "feedback_save.php";
		map.put("feature", "SendFeedback");
		String params = mapToPOSTString(map, c);
		String data = openHttpPostConnection(urlString, params);
		return FeedbackResponse.parseResponse(data);
	}

	public static void savePushToken(HashMap<String, String> map, Context c) {

		String urlString = USER_SERVER_URL + "user_token_save";
		map.put("feature", "SavePushToken");
		String params = mapToPOSTString(map, c);
		String data = openHttpPostConnection(urlString, params);
		Log.d("Webservice", "Push token save response: " + data);
	}

	public static User signUp(HashMap<String, String> map, String picturePath, Context c) {
		// http://www.horizonwebsolutions.net/test/bunduapp/signup_processing_unit.php
		/*
		 * user_fullname,
		 * user_email,user_pass,user_address,user_dob,user_phone,user_cphone
		 * ,user_pic, {img file}
		 */
		map.put("feature", "SignUp");
		map = addMetaDataToMap(map, c);
		String urlString = USER_SERVER_URL + "signup";
		String data = openHttpPostConnection(urlString, map, picturePath);

		return User.parseSignUpResponse(data);
	}

	public static String reserveTable(HashMap<String, String> map, Context c) {

		String result = null;

		String urlString = TOSSDOWN_URL + "api/save_res/?rid=" + Constants.FAKE_EATINGOUT_ID;
		map.put("feature", "ReserveTable");
		map.put("rid", Constants.FAKE_EATINGOUT_ID);
		String params = mapToPOSTString(map, c);
		String data = openHttpPostConnection(urlString, params);
		if (data != null && data.trim().length() > 0) {
			ResTableResponse res = ResTableResponse.parseResTableResponse(data);
			if (res != null) {
				result = res.getMessage();
			} else {
				return data;
			}
		}

		return result;
	}

	public static MenuDetails getMenuDetails(String brancchId, Context c) {

		String urlString = TOSSDOWN_URL + "api/menu/?";
		HashMap<String, String> map = new HashMap<String, String>();
		map.put("feature", "GetMenu");
		// explicitly adding correct branch id, cause addMetaDataToMap() adds a
		// default id
		map.put("rid", brancchId);
		String params = mapToPOSTString(map, c);
		String data = openHttpPostConnection(urlString + params, "");
		if (data != null && data.trim().length() > 0) {

			MenuDetails menu = new MenuDetails(data);
			return menu;
		}
		return null;
	}

	public static String submitOder(HashMap<String, String> map, Context c) {

		String result = "Success.";
		String urlString = TOSSDOWN_URL + "api/post_order";
		String params = mapToPOSTString(map, c);
		String data = openHttpPostConnection(urlString, params);
		if (data != null && data.trim().length() > 0) {
			Log.d("Webservice", data);
			ResTableResponse res = ResTableResponse.parseResTableResponse(data);
			if (res != null) {
				result = res.getMessage();
				if(map != null && result != null && result.contains("successfully posted")){
					sendAcknowledgementTextMessage(map.get("order"), map.get("mobile_phone"), map.get("name"));
				}
			} else {
				return data;
			}
		} else {
			result = "There was an error in connecting to server.";
		}

		return result;
	}

	private static void sendAcknowledgementTextMessage(String order, String mobile_number, String name){
		try {
			String urlString = USER_SERVER_URL + "smsfile.php";
			JSONObject jsonOrder = new JSONObject(order);
			JSONArray itemsArray = (JSONArray) jsonOrder.get("items");
			String msgText = "Dear "+ name + ",\n" + "We've received your order and your order details are:\n";
			
			for (int i = 0; i < itemsArray.length(); i++) {
			    JSONObject row = itemsArray.getJSONObject(i);
			    msgText = msgText + row.get("dish_name") +"\n";
			    msgText = msgText + "Qty: " + row.get("dish_qty") + " " + "Price: " + row.get("dish_price") + "\n";
			}
			msgText = msgText + "Grand Total: Rs " + ShoppingCart.getInstance().getGrossTotal() + "\n" +
			"Someone from our team will get in contact with you shortly to confirm your order, Thank you";
			
			String params = "message=" + msgText + "&subno=" + mobile_number;
			String data = openHttpPostConnection(urlString, params);
			Log.d("TextMessage", data);
			
		} catch (Exception e) {
			e.printStackTrace();
			Log.e("TextMessage", "Error while calling smsfile.php: " + e.getMessage() );
		}
	}
	public static Locations getLocations(Context c) {

		String urlString = TOSSDOWN_URL + "api/eatout_branches/?";

		HashMap<String, String> map = new HashMap<String, String>();
		map.put("eatoutid", Constants.EATINGOUT_ID);
		map.put("city", Constants.CITY);

		String params = mapToPOSTString(map, c);

		String data = openHttpPostConnection(urlString + params, "");
		if (data != null && data.trim().length() > 0) {

			Locations locations = new Locations(data);
			return locations;
		}
		return null;
	}

	public static Locations getLocations(Context c, String restuarantId) {

		String urlString = TOSSDOWN_URL + "api/eatout_branches/?";

		HashMap<String, String> map = new HashMap<String, String>();
		map.put("eatoutid", restuarantId);
		map.put("city", Constants.CITY);

		String params = mapToPOSTString(map, c);

		String data = openHttpPostConnection(urlString + params, "");
		Constants.test = data;

		if (data != null && data.trim().length() > 0) {

			Locations locations = new Locations(data);
			return locations;
		}
		return null;
	}


	public static Deals getDeals(HashMap<String, String> map, Context c) {
		// http://tossdown.com/api/eatout_deals/?rid=7

		String urlString = TOSSDOWN_URL + "api/eatout_deals/?";

		String params = mapToPOSTString(map, c);

		String data = openHttpPostConnection(urlString + params, "");
		if (data != null && data.trim().length() > 0) {

			Deals deals = new Deals(data);
			return deals;
		}
		return null;
	}

	public static Photos getPhotos(HashMap<String, String> map, Context c) {
		// http://tossdown.com/api/image_gallery?eatout_id=7

		String urlString = TOSSDOWN_URL + "api/image_gallery?eatout_id=";

		//String params = mapToPOSTString(map, c);

		String data = openHttpPostConnection(urlString + Constants.RESTAURANT_ID, "");
		//Constants.test = data;

		if (data != null && data.trim().length() > 0) {
			Photos photos = new Photos(data);
			Constants.test = photos.getSnpImages().get(0).getImageName();
			return photos;
		}
		return null;
	}




	private static String openHttpPostConnection(String urlString, String parameters) {

		StringBuffer response = null;
		String result = null;
		InputStreamReader isr = null;
		HttpURLConnection connection = null;
		Log.i("Webservice:openHttpPostConnection", "URL: " + urlString);
		try {

			URL url = new URL(urlString);
			connection = (HttpURLConnection) url.openConnection();
			connection.setDoOutput(true);
			connection.setDoInput(true);
			connection.setInstanceFollowRedirects(false);
			connection.setRequestMethod("POST");

			connection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
			connection.setRequestProperty("charset", "utf-8");
			connection.setRequestProperty("Content-Length", "" + Integer.toString(parameters.getBytes("UTF-8").length));
			connection.setUseCaches(false);
			connection.setConnectTimeout(30000);
			connection.connect();

			DataOutputStream wr = new DataOutputStream(connection.getOutputStream());
			wr.writeBytes(parameters);
			wr.flush();
			wr.close();

			int responseCode = connection.getResponseCode();
			Log.i("Response", Integer.toString(responseCode));
			if (responseCode == HttpURLConnection.HTTP_OK) {

				response = new StringBuffer();
				isr = new InputStreamReader(connection.getInputStream());
				BufferedReader in = new BufferedReader(isr);

				String line;
				while ((line = in.readLine()) != null) {
					response.append(line);
					Log.i("Webservice", "Response: " + line);
				}
			} else {

				response = new StringBuffer();
				isr = new InputStreamReader(connection.getErrorStream());
				BufferedReader in = new BufferedReader(isr);

				String line;
				while ((line = in.readLine()) != null) {
					response.append(line);
				}
			}

			Log.i("Webservice", "Response: " + response.toString());
		} catch (Exception e) {

			e.printStackTrace();
			Log.e("Webservice", "ClientProtocolException error: " + e.getMessage());
			result = e.getMessage();
		} finally {

			if (isr != null) {
				try {
					isr.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
				isr = null;
			}
			if (connection != null) {
				connection.disconnect();
				connection = null;
			}
		}
		if (response != null)
			return response.toString();
		else
			return result;
	}

	private static String openHttpPostConnection(String urlString, Map<String, String> parmas, String filePath) {

		StringBuffer response = null;

		InputStreamReader isr = null;
		HttpURLConnection connection = null;

		DataOutputStream outputStream = null;

		String twoHyphens = "--";
		String boundary = "*****" + Long.toString(System.currentTimeMillis()) + "*****";
		String lineEnd = "\r\n";

		String result = "";

		int bytesRead, bytesAvailable, bufferSize;
		byte[] buffer;
		int maxBufferSize = 1 * 1024 * 1024;

		try {
			FileInputStream fileInputStream = null;
			String[] q = null;
			int idx = 0;
			if (filePath != null && filePath.length() > 0) {
				q = filePath.split("/");
				idx = q.length - 1;
				File file = new File(filePath);
				fileInputStream = new FileInputStream(file);
			}

			URL url = new URL(urlString);
			connection = (HttpURLConnection) url.openConnection();

			connection.setDoInput(true);
			connection.setDoOutput(true);
			connection.setUseCaches(false);

			connection.setRequestMethod("POST");
			connection.setRequestProperty("Connection", "Keep-Alive");
			connection.setRequestProperty("User-Agent", "Android Multipart HTTP Client 1.0");
			connection.setRequestProperty("Content-Type", "multipart/form-data; boundary=" + boundary);

			outputStream = new DataOutputStream(connection.getOutputStream());
			if (fileInputStream != null) {
				outputStream.writeBytes(twoHyphens + boundary + lineEnd);
				outputStream.writeBytes("Content-Disposition: form-data; name=\"" + "user_pic" + "\"; filename=\""
						+ q[idx] + "\"" + lineEnd);
				outputStream.writeBytes("Content-Type: " + "image/jpg" + lineEnd);
				outputStream.writeBytes("Content-Transfer-Encoding: binary" + lineEnd);

				outputStream.writeBytes(lineEnd);

				bytesAvailable = fileInputStream.available();
				bufferSize = Math.min(bytesAvailable, maxBufferSize);
				buffer = new byte[bufferSize];

				bytesRead = fileInputStream.read(buffer, 0, bufferSize);
				while (bytesRead > 0) {
					outputStream.write(buffer, 0, bufferSize);
					bytesAvailable = fileInputStream.available();
					bufferSize = Math.min(bytesAvailable, maxBufferSize);
					bytesRead = fileInputStream.read(buffer, 0, bufferSize);
				}
			}

			outputStream.writeBytes(lineEnd);

			// Upload POST Data
			Iterator<String> keys = parmas.keySet().iterator();
			while (keys.hasNext()) {
				String key = keys.next();
				String value = parmas.get(key);

				outputStream.writeBytes(twoHyphens + boundary + lineEnd);
				outputStream.writeBytes("Content-Disposition: form-data; name=\"" + key + "\"" + lineEnd);
				outputStream.writeBytes("Content-Type: text/plain" + lineEnd);
				outputStream.writeBytes(lineEnd);
				outputStream.writeBytes(value);
				outputStream.writeBytes(lineEnd);
			}

			outputStream.writeBytes(twoHyphens + boundary + twoHyphens + lineEnd);

			int responseCode = connection.getResponseCode();
			Log.i("Response", Integer.toString(responseCode));
			if (responseCode == HttpURLConnection.HTTP_OK) {

				response = new StringBuffer();
				isr = new InputStreamReader(connection.getInputStream());
				BufferedReader in = new BufferedReader(isr);

				String line;
				while ((line = in.readLine()) != null) {
					response.append(line);
				}
			} else {

				response = new StringBuffer();
				isr = new InputStreamReader(connection.getErrorStream());
				BufferedReader in = new BufferedReader(isr);

				String line;
				while ((line = in.readLine()) != null) {
					response.append(line);
				}
			}

			Log.i("Webservice", "Response: " + response.toString());
			if (fileInputStream != null) {
				fileInputStream.close();
			}
			outputStream.flush();
			outputStream.close();

			// return result;
		} catch (Exception e) {

			e.printStackTrace();
			Log.e("Webservice", "ClientProtocolException error: " + e.getMessage());
			result = e.getMessage();
		} finally {

			if (isr != null) {
				try {
					isr.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
				isr = null;
			}
			if (connection != null) {
				connection.disconnect();
				connection = null;
			}
		}
		if (response != null)
			return response.toString();
		else
			return result;
	}

	private static String openHttpGETConnection(String urlString, MutableString errorMessage) {

		InputStreamReader isr = null;
		StringBuffer response = null;
		HttpURLConnection connection = null;
		try {

			URL url = new URL(urlString);
			connection = (HttpURLConnection) url.openConnection();
			connection.setRequestMethod("GET");
			connection.setAllowUserInteraction(true);
			connection.setUseCaches(false);
			connection.setDoInput(true);
			connection.setConnectTimeout(30000);
			connection.connect();

			int responseCode = connection.getResponseCode();
			if (responseCode == HttpURLConnection.HTTP_OK) {

				response = new StringBuffer();
				isr = new InputStreamReader(connection.getInputStream());
				BufferedReader in = new BufferedReader(isr);

				String line;
				while ((line = in.readLine()) != null) {
					response.append(line);
				}
			} else {
				Log.e("Webservice", "Http error code: " + responseCode);
				errorMessage.setString("Http error code: " + responseCode);
			}
		} catch (Exception e) {

			e.printStackTrace();
			errorMessage.setString(e.getMessage());
			Log.e("Webservice", "Exception occured: " + e.getMessage());
		} finally {

			if (isr != null) {
				try {
					isr.close();
				} catch (IOException e) {

					e.printStackTrace();
				}
				isr = null;
			}
			if (connection != null) {

				connection.disconnect();
				connection = null;
			}
		}
		if (response != null)
			return response.toString();
		else
			return null;
	}

}
