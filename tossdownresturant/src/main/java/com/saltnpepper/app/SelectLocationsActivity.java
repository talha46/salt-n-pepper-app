/**
 * SelectLocationsActivity.java
 * BooleanBites Ltd. (c) 2015
 * @author adilsoomro
 */
package com.saltnpepper.app;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.saltnpepper.app.utils.Constants;
import com.saltnpepper.app.R;

/**
 * @author adilsoomro
 * 
 */
public class SelectLocationsActivity extends Activity {
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.setContentView(R.layout.activity_select_locations_layout);
	}

	public void showMenu(View v) {
		String id = v.getTag().toString();
		Intent intent = new Intent(this, com.saltnpepper.app.MenuActivity.class);
		intent.putExtra(Constants.RESTAURANT_ID_KEY, id); // add selected branch
		startActivity(intent);
	}
}
