package com.saltnpepper.app;


import org.json.JSONException;
import org.json.JSONObject;

import android.app.ProgressDialog;
import android.content.Context;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Looper;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.text.method.HideReturnsTransformationMethod;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Toast;

import com.saltnpepper.app.utils.Util;
import com.facebook.FacebookException;
import com.facebook.FacebookRequestError;
import com.facebook.HttpMethod;
import com.facebook.Request;
import com.facebook.RequestAsyncTask;
import com.facebook.Response;
import com.facebook.Session;
import com.facebook.SessionState;
import com.facebook.model.GraphObject;
import com.facebook.model.GraphPlace;
import com.facebook.widget.PickerFragment;
import com.facebook.widget.PlacePickerFragment;
import com.saltnpepper.app.R;

public class CheckInActivity extends FragmentActivity {
    //    public static final Uri PLACE_PICKER = Uri.parse("picker://place");
    private static final String PLACE_KEY = "place";
    private PlacePickerFragment placePickerFragment;
    private LocationListener locationListener;

    private static final int SEARCH_RADIUS_METERS = 10000;
    private static final int SEARCH_RESULT_LIMIT = 50;
    private static final String SEARCH_TEXT = "bundu khan";
    private static final int LOCATION_CHANGE_THRESHOLD = 50; // meters

    private static final Location SAN_FRANCISCO_LOCATION = new Location("") {{
	setLatitude(31.501288);
	setLongitude(74.367316);
    }};

    private GraphPlace selectedPlace;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
	super.onCreate(savedInstanceState);
	setContentView(R.layout.activity_check_in_layout);

	Bundle args = getIntent().getExtras();
	FragmentManager manager = getSupportFragmentManager();
	Fragment fragmentToShow = null;

	if (savedInstanceState == null) {
	    placePickerFragment = new PlacePickerFragment(args);
	} else {
	    placePickerFragment = 
		    (PlacePickerFragment) manager.findFragmentById(R.id.picker_fragment);
	}
	placePickerFragment.setOnSelectionChangedListener(
		new PickerFragment.OnSelectionChangedListener() {
		    @Override
		    public void onSelectionChanged(PickerFragment<?> fragment) {
			doCheckIn(); // call finish since you can only pick one place
		    }
		});
	placePickerFragment.setOnErrorListener(
		new PickerFragment.OnErrorListener() {
		    @Override
		    public void onError(PickerFragment<?> fragment,
			    FacebookException error) {
			CheckInActivity.this.onError(error);
		    }
		});
	placePickerFragment.setOnDoneButtonClickedListener(
		new PickerFragment.OnDoneButtonClickedListener() {
		    @Override
		    public void onDoneButtonClicked(PickerFragment<?> fragment) {
			Util.displayAlert(CheckInActivity.this,"Are you sure you want to cancel the check-in.", "Yes","No", new View.OnClickListener() {
			    @Override
			    public void onClick(View arg0) {
				finishActivity();
			    }
			});
		    }
		});
	fragmentToShow = placePickerFragment;
	manager.beginTransaction()
	.replace(R.id.picker_fragment, fragmentToShow)
	.commit();
    }
    private void onError(Exception error) {
	//onError(error.getLocalizedMessage(), false);
	Util.showMessage(error.getLocalizedMessage(), CheckInActivity.this);
    }
    private void finishActivity() {
	setResult(RESULT_OK, null);
	finish();
    }
    @Override
    protected void onStart() {
	super.onStart();
	try {
	    Location location = null;
	    // Instantiate the default criteria for a location provider
	    Criteria criteria = new Criteria();
	    // Get a location manager from the system services
	    LocationManager locationManager = 
		    (LocationManager) getSystemService(Context.LOCATION_SERVICE);
	    // Get the location provider that best matches the criteria
	    String bestProvider = locationManager.getBestProvider(criteria, false);
	    if (bestProvider != null) {
		// Get the user's last known location
		location = locationManager.getLastKnownLocation(bestProvider);
		if (locationManager.isProviderEnabled(bestProvider) 
			&& locationListener == null) {
		    // Set up a location listener if one is not already set up
		    // and the selected provider is enabled
		    locationListener = new LocationListener() {
			@Override
			public void onLocationChanged(Location location) {
			    // On location updates, compare the current
			    // location to the desired location set in the
			    // place picker
			    float distance = location.distanceTo(
				    placePickerFragment.getLocation());
			    if (distance >= LOCATION_CHANGE_THRESHOLD) {
				placePickerFragment.setLocation(location);
				placePickerFragment.loadData(true);
			    }
			}
			@Override
			public void onStatusChanged(String s, int i, 
				Bundle bundle) {
			}
			@Override
			public void onProviderEnabled(String s) {
			}
			@Override
			public void onProviderDisabled(String s) {
			}
		    };
		    locationManager.requestLocationUpdates(bestProvider, 
			    1, LOCATION_CHANGE_THRESHOLD,
			    locationListener, 
			    Looper.getMainLooper());
		}
	    }
	    if (location == null) {
		// Set the last saved location
		location = Util.getLocation(CheckInActivity.this);
	    }
	    if (location == null) {
		// Set the default location if there is no
		// initial location

		// This may be the emulator, use the default location
		location = SAN_FRANCISCO_LOCATION;

	    }
	    if (location != null) {
		// Configure the place picker: search center, radius,
		// query, and maximum results.
		placePickerFragment.setLocation(location);
		placePickerFragment.setRadiusInMeters(SEARCH_RADIUS_METERS);
		placePickerFragment.setSearchText(SEARCH_TEXT);
		placePickerFragment.setResultsLimit(SEARCH_RESULT_LIMIT);
		// Start the API call
		placePickerFragment.loadData(true);
	    } else {
		// If no location found, show an error
		Util.showAlert(getResources().getString(R.string.no_location_error), CheckInActivity.this, false);
	    }
	} catch (Exception ex) {
	    onError(ex);

	}
    }
    @Override
    protected void onStop() {
	super.onStop();
	if (locationListener != null) {
	    LocationManager locationManager = 
		    (LocationManager) getSystemService(Context.LOCATION_SERVICE);
	    // Remove updates for the location listener
	    locationManager.removeUpdates(locationListener);
	    locationListener = null;
	}
    }
    @Override
    protected void onSaveInstanceState(Bundle bundle) {
	if (selectedPlace != null) {
	    bundle.putString(PLACE_KEY, 
		    selectedPlace.getInnerJSONObject().toString());
	}   
    }

    public void doCheckIn(){
	hasRequestedPermission = false;
	if (placePickerFragment != null) {
	    this.selectedPlace = placePickerFragment.getSelection();

	    if(this.selectedPlace != null){
		Log.i("CheckIn", "GraphPlace "+this.selectedPlace.getInnerJSONObject().toString());	
		final String placeId = this.selectedPlace.getId();
		String message = "Are you sure you want to check-in to Bundu Khan.\nThis will post your check-in to facebook.";
		Util.displayAlert(CheckInActivity.this, message, new OnClickListener() {
		    @Override
		    public void onClick(View v) {
			final String PERMISSIONS = "publish_actions";
			Session.openActiveSession(CheckInActivity.this, true, new Session.StatusCallback() {
			    // callback when session changes state
			    @Override
			    public void call(Session session, SessionState state, Exception exception) {
				if (session != null && session.isOpened()) {
				    // Check for publish permissions
				    if (!hasPublishPermission()) {
					if (hasRequestedPermission) {
					    Util.showMessage("Check-In failed", "App doesn't have Check-In permission.",
						    CheckInActivity.this);
					    return;
					}
					hasRequestedPermission = true;
					Session.NewPermissionsRequest newPermissionsRequest = new Session.NewPermissionsRequest(
						CheckInActivity.this, PERMISSIONS);
					session.requestNewPublishPermissions(newPermissionsRequest);
					return;
				    }

				    Bundle postParams = new Bundle();
				    postParams.putString("message", "");
				    postParams.putString("tags", "359040380819328");
				    postParams.putString("place", placeId);
				    Request.Callback callback = new Request.Callback() {
					private String toastmessage;

					public void onCompleted(Response response) {
					    String postId  = null;
					    try {
						JSONObject graphResponse = response.getGraphObject().getInnerJSONObject();
						postId = graphResponse.getString("id");
					    } catch (Exception e) {
						Log.i("Test", "JSON error " + e.getMessage());
						// Toast.makeText(DashboardActivity.this,
						// error.getErrorMessage(),
						// Toast.LENGTH_SHORT).show();
					    }
					    FacebookRequestError error = response.getError();
					    if (error != null || postId == null) {
						Toast.makeText(CheckInActivity.this, error.getErrorMessage(),
							Toast.LENGTH_SHORT).show();
					    } else {
						toastmessage = "Checked-In successfully";
						Toast.makeText(CheckInActivity.this, toastmessage, Toast.LENGTH_SHORT).show();
						dismissProgressDialog();
						finishActivity();
					    }
					}
				    };
				    showProgressDialog("Check In", "Please wait...");
				    Request request = new Request(session, "me/feed", postParams, HttpMethod.POST, callback);
				    RequestAsyncTask task = new RequestAsyncTask(request);
				    task.execute();
				}

			    }
			});
		    }
		});
	    }
	}
    }
    private boolean hasRequestedPermission = false;

    private boolean hasPublishPermission() {
	Session session = Session.getActiveSession();
	Log.d("DashboardActivity", "Permissions: " + session.getPermissions().toString());
	return session != null && session.getPermissions().contains("publish_actions");
    }
    @Override
    protected void onRestoreInstanceState(Bundle savedState) {
	super.onRestoreInstanceState(savedState);
	String place = savedState.getString(PLACE_KEY);
	if (place != null) {
	    try {
		selectedPlace = GraphObject.Factory.create(
			new JSONObject(place), 
			GraphPlace.class);
		//setPlaceText();
		return ;
	    } catch (JSONException e) {
		Log.e("CheckInActivity", "Unable to deserialize place.", e); 
	    }   
	}   
	return ;
    } 
    private ProgressDialog dialog = null;
    private void showProgressDialog(String title, String message) {

	if (dialog == null) {
	    dialog = new ProgressDialog(CheckInActivity.this);
	}
	dialog.setTitle(title);
	dialog.setCanceledOnTouchOutside(false);
	dialog.setMessage(message);
	dialog.show();
    }

    private void dismissProgressDialog() {
	if (dialog != null) {
	    if (dialog.isShowing()) {
		dialog.dismiss();
		dialog = null;
	    }
	}
    }


}

